// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 11.8.2023

package de.thkoeln.cvogt.android.opengl_utilities;

import java.util.ArrayList;

/**
 * Class to define group of shapes to be handled together
 * <P>
 * Currently this class provides only a few basic methods and could be extended in the future.
 * @see GLShapeCV
 */

public class GLShapeGroupCV {

    /**
     * The shapes belonging to this group.
     */

    private ArrayList<GLShapeCV> shapes;

    /**
     * Constructor
     */

    public GLShapeGroupCV() {
        this.shapes = new ArrayList<>();
    }

    /**
     * Constructor
     * @param shapes The shapes to be added to this group.
     */

    public GLShapeGroupCV(ArrayList<GLShapeCV> shapes) {
        this.shapes = (ArrayList<GLShapeCV>) shapes.clone();
    }

    /**
     * Constructor
     * @param shapes The shapes to be added to this group.
     */

    public GLShapeGroupCV(GLShapeCV[] shapes) {
        this.shapes = new ArrayList<GLShapeCV>();
        for (GLShapeCV shape : shapes)
            this.shapes.add(shape);
    }

    /**
     * Gets the shapes of this group (as a copy of the shapes attribute)
     */

    public ArrayList<GLShapeCV> getShapes() {
        return (ArrayList<GLShapeCV>) this.shapes.clone();
    }

    /**
     * Adds a shape to the group.
     */

    public void addShape(GLShapeCV shape) {
        this.shapes.add(shape);
    }

    /**
     * Removes a shape from the group.
     */

    public void removeShape(GLShapeCV shape) {
        this.shapes.remove(shape);
    }

}

