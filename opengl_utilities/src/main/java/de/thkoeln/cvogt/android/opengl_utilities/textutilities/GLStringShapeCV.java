// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 14.8.2023

package de.thkoeln.cvogt.android.opengl_utilities.textutilities;

import android.content.Context;
import android.util.Log;

import de.thkoeln.cvogt.android.opengl_utilities.GLLineCV;
import de.thkoeln.cvogt.android.opengl_utilities.GLShapeCV;
import de.thkoeln.cvogt.android.opengl_utilities.GLTriangleCV;
import de.thkoeln.cvogt.android.opengl_utilities.GraphicsUtilsCV;

/**
 * A subclass of class GLShapeCV to define shapes that show a character string. The string shape
 * <UL>
 * <LI>may be two- or three-dimensional,
 * <LI>may be solid (i.e. with colored faces) or a wireframe and
 * <LI>may have lines bordering all its triangles, bordering its faces, or no lines.
 * </UL>
 * @see GLShapeCV
 * @see GLCharShapeCV
 * @see GLTextUtilsCV
 */

public class GLStringShapeCV extends GLShapeCV {

    /**
     * A combination of flags (see the corresponding constants defined in class GLTextUtilsCV) describing the properties of the shape.
     * @see GLTextUtilsCV#TwoD
     * @see GLTextUtilsCV#ThreeD
     * @see GLTextUtilsCV#Solid
     * @see GLTextUtilsCV#Wireframe
     * @see GLTextUtilsCV#FaceLines
     * @see GLTextUtilsCV#TriangleLines
     */

    private int propertyFlags;

    /**
     * The string displayed by the shape
     */

    private String string;

    /**
     * The face color of the shape (only valid if the shape has faces, i.e. is not a wireframe).
     */

    private float[] faceColor = null;

    /**
     * The line color of the shape (only valid if the shape has lines).
     */

    private float[] lineColor = null;

    /**
     * In the triangles[] attribute the start positions of the triangle sequences that form the individual characters
     * - value at position i = triangles[] index of the first triangle of the ith character.
     */

    private int[] trianglesStartIndices;

    /**
     * In the lines[] attribute the start positions of the line sequences that form the individual characters
     * - value at position i = lines[] index of the first line of the ith character.
     */

    private int[] linesStartIndices;

    /**
     * Constructor to create a shape for a string of characters.
     * The shape will be two-dimensional and have white faces without lines.
     * For more information, see the comments on the constructor with seven parameters.
     * @param id The ID of the new shape.
     * @param string The string to be displayed. If a character in the string is not supported, an underscore _ will be shown instead.
     * @param context The context in which this method is called (can be null if the cache entries for the characters have already been created, e.g. by createCacheEntries()).
     * @see GLStringShapeCV#GLStringShapeCV(String, String, float[], float[], float, int,Context)
     **/

    public GLStringShapeCV(String id, String string, Context context) {
        this(id,string,GraphicsUtilsCV.white,context);
    }

    /**
     * Constructor to create a shape for a string of characters.
     * The shape will be two-dimensional and have colored faces without lines.
     * For more information, see the comments on the constructor with seven parameters
     * @param id The ID of the new shape.
     * @param string The string to be displayed. If a character in the string is not supported, an underscore _ will be shown instead.
     * @param faceColor The face color of the shape. If null or not a valid color value, a white face color will be assumed.
     * @param context The context in which this method is called (can be null if the cache entries for the characters have already been created, e.g. by createCacheEntries()).
     * @see GLStringShapeCV#GLStringShapeCV(String, String, float[], float[], float, int,Context)
     **/

    public GLStringShapeCV(String id, String string, float[] faceColor, Context context) {
        this(id,string,faceColor,null,0,GLTextUtilsCV.TwoD|GLTextUtilsCV.Solid,context);
    }

    /**
     * Constructor to create a shape for a string of characters.
     * The shape will be three-dimensional, have colored faces and colored face lines.
     * For more information, see the comments on the constructor with seven parameters
     * @param id The ID of the new shape.
     * @param string The string to be displayed. If a character in the string is not supported, an underscore _ will be shown instead.
     * @param faceColor The face color of the shape. If null or not a valid color value, a white face color will be assumed.
     * @param lineColor The line color of the shape. If null or not a valid color value, a black line color will be assumed.
     * @param lineWidth The line width of the shape.
     * @param context The context in which this method is called (can be null if the cache entries for the characters have already been created, e.g. by createCacheEntries()).
     * @see GLStringShapeCV#GLStringShapeCV(String, String, float[], float[], float, int,Context)
     **/

    public GLStringShapeCV(String id, String string, float[] faceColor, float[] lineColor, float lineWidth, Context context) {
        this(id,string,faceColor,lineColor,lineWidth,GLTextUtilsCV.ThreeD|GLTextUtilsCV.Solid|GLTextUtilsCV.FaceLines,context);
    }

    /**
     * Constructor to create a shape for a string of characters.
     * <P>
     * The shape will be built form the triangles and lines of the corresponding single-character shapes created by the constructor of the class GLCharShapeCV.
     * More details are provided in the comments of this constructor.
     * @param id The ID of the new shape.
     * @param string The string to be displayed. If a character in the string is not supported, an underscore _ will be shown instead.
     * @param faceColor The face color of the shape. May be null if the shape is a wireframe. If needed but not provided, a white face color will be assumed.
     * @param lineColor The line color of the shape. May be null if the shape has only faces but no lines. If needed but not provided, a black line color will be assumed.
     * @param lineWidth The line width of the shape. Irrelevant if the shape has only faces but no lines.
     * @param flags Flags specifying the looks of the shape (as defined by static constants of the class GLTextUtilsCV). The flags form three groups:
     *              <UL>
     *              <LI>ThreeD, TwoD: three-dimensional or two-dimensional (i.e. flat) shape. If both flags are set or none, ThreeD will be assumed.
     *              <LI>Solid, Wireframe: solid (i.e. with colored faces) or wireframe shape. If both flags are set or none, Solid will be assumed.
     *              <LI>FaceLines, TriangleLines: The lines to be shown - only the border lines of the faces, the border lines of all triangles, or no lines at all.
     *                  If both flags are set, FaceLines will be assumed.
     *                  If both flags are not set, for a wireframe shape FaceLines will be assumed, and for a solid shape no lines will be drawn.
     *              </UL>
     *              Hence, the value 0 for this parameter will produce a 3D shape with solid colored faces and no lines.
     * @param context The context in which this method is called (can be null if the cache entries for the characters have already been created, e.g. by createCacheEntries()).
     * @see GLCharShapeCV#GLCharShapeCV(String, char, float[], float[], float, int, Context)
     **/

    public GLStringShapeCV(String id, String string, float[] faceColor, float[] lineColor, float lineWidth, int flags, Context context) {
        setId(id);
        long start = System.nanoTime();
        // set the 'shape' attribute
        this.string = string;
        // analyze the 'flags' parameter and set the 'propertyFlags' attribute
        boolean threeD = ((flags & GLTextUtilsCV.ThreeD) != 0) || ((flags & GLTextUtilsCV.ThreeD) == 0 && (flags & GLTextUtilsCV.TwoD) == 0);
        boolean solid = ((flags & GLTextUtilsCV.Solid) != 0) || ((flags & GLTextUtilsCV.Solid) == 0 && (flags & GLTextUtilsCV.Wireframe) == 0);
        boolean faceLines = ((flags & GLTextUtilsCV.FaceLines) != 0) || ((flags & GLTextUtilsCV.FaceLines) == 0 && (flags & GLTextUtilsCV.TriangleLines) == 0 && !solid);
        boolean triangleLines = !faceLines && ((flags & GLTextUtilsCV.TriangleLines) != 0);
        if (threeD)
            this.propertyFlags = GLTextUtilsCV.ThreeD;
        else
            this.propertyFlags = GLTextUtilsCV.TwoD;
        if (solid)
            this.propertyFlags |= GLTextUtilsCV.Solid;
        else
            this.propertyFlags |= GLTextUtilsCV.Wireframe;
        if (faceLines) this.propertyFlags |= GLTextUtilsCV.FaceLines;
        if (triangleLines) this.propertyFlags |= GLTextUtilsCV.TriangleLines;
        // set the 'lineWidth' attribute
        this.lineWidth = lineWidth;
        // set the color attributes
        if (isSolid() && !GraphicsUtilsCV.isValidColorArray(faceColor))
            this.faceColor = GraphicsUtilsCV.white;
        else
            this.faceColor = faceColor.clone();
        if (hasLines())
            if (!GraphicsUtilsCV.isValidColorArray(lineColor))
                this.lineColor = GraphicsUtilsCV.black;
            else
                this.lineColor = lineColor.clone();
        // set the triangles and lines
        GLCharShapeCV[] charShapes = new GLCharShapeCV[string.length()];
        trianglesStartIndices = new int[string.length()];
        linesStartIndices = new int[string.length()];
        int numberOfTriangles = 0;
        int numberOfLines = 0;
        float currXPos = 0;
        for (int i=0;i<string.length();i++) {
            charShapes[i] = new GLCharShapeCV(id,string.charAt(i),faceColor,lineColor,lineWidth,propertyFlags,context);
            trianglesStartIndices[i]=numberOfTriangles;
            // Log.v("GLDEMO","Triangle "+i+" start: "+numberOfTriangles);
            linesStartIndices[i]=numberOfLines;
            // Log.v("GLDEMO","Line "+i+" start: "+numberOfLines);
            numberOfTriangles += charShapes[i].getNumberOfTriangles();
            numberOfLines += charShapes[i].getNumberOfLines();
            if (i>0)
                currXPos += GLTextUtilsCV.horizDistanceCharShapes(charShapes[i - 1], charShapes[i], true);
            charShapes[i].moveZeroPointTo(-currXPos,-GLTextUtilsCV.charShapeVertAdjustment(charShapes[i]),0);
        }
        // Log.v("GLDEMO","Triangles end: "+numberOfTriangles);
        // Log.v("GLDEMO","Lines end: "+numberOfLines);
        if (numberOfTriangles>0)
            this.triangles = new GLTriangleCV[numberOfTriangles];
          else
            this.triangles = null;
        if (numberOfLines>0)
            this.lines = new GLLineCV[numberOfLines];
          else
            this.lines = null;
        for (int i=0;i<string.length();i++) {
            GLTriangleCV[] trianglesIthShape = charShapes[i].getTriangles(false);
            if (trianglesIthShape!=null&&trianglesIthShape.length>0)
                System.arraycopy(trianglesIthShape,0,triangles,trianglesStartIndices[i],trianglesIthShape.length);
            GLLineCV[] linesIthShape = charShapes[i].getLines(false);
            if (linesIthShape!=null&&linesIthShape.length>0)
                System.arraycopy(linesIthShape, 0, lines, linesStartIndices[i], linesIthShape.length);
        }
        setBuffers();
        moveZeroPointTo(getCenter());
    }

    /**
     * Sets/replaces the string.
     * @param string The new string to be set.
     * @param context The context from which this method is called (can be null if the cache entries for the character have already been created, e.g. by createCacheEntries()).
     */

    synchronized public void setString(String string, Context context) {
        this.string = string;
        GLCharShapeCV[] charShapes = new GLCharShapeCV[string.length()];
        trianglesStartIndices = new int[string.length()];
        linesStartIndices = new int[string.length()];
        int numberOfTriangles = 0;
        int numberOfLines = 0;
        float currXPos = 0;
        for (int i = 0; i < string.length(); i++) {
            charShapes[i] = new GLCharShapeCV("", string.charAt(i), faceColor, lineColor, lineWidth, propertyFlags, context);
            trianglesStartIndices[i] = numberOfTriangles;
            // Log.v("GLDEMO","Triangle "+i+" start: "+numberOfTriangles);
            linesStartIndices[i] = numberOfLines;
            // Log.v("GLDEMO","Line "+i+" start: "+numberOfLines);
            numberOfTriangles += charShapes[i].getNumberOfTriangles();
            numberOfLines += charShapes[i].getNumberOfLines();
            if (i > 0)
                currXPos += GLTextUtilsCV.horizDistanceCharShapes(charShapes[i - 1], charShapes[i], true);
            charShapes[i].moveZeroPointTo(-currXPos, -GLTextUtilsCV.charShapeVertAdjustment(charShapes[i]), 0);
        }
        // Log.v("GLDEMO","Triangles end: "+numberOfTriangles);
        // Log.v("GLDEMO","Lines end: "+numberOfLines);
        if (numberOfTriangles > 0)
            this.triangles = new GLTriangleCV[numberOfTriangles];
        else
            this.triangles = null;
        if (numberOfLines > 0)
            this.lines = new GLLineCV[numberOfLines];
        else
            this.lines = null;
        for (int i = 0; i < string.length(); i++) {
            GLTriangleCV[] trianglesIthShape = charShapes[i].getTriangles(false);
            if (trianglesIthShape != null && trianglesIthShape.length > 0)
                System.arraycopy(trianglesIthShape, 0, triangles, trianglesStartIndices[i], trianglesIthShape.length);
            GLLineCV[] linesIthShape = charShapes[i].getLines(false);
            if (linesIthShape != null && linesIthShape.length > 0)
                System.arraycopy(linesIthShape, 0, lines, linesStartIndices[i], linesIthShape.length);
        }
        // colorArrayOfTriangles = null;
        moveZeroPointTo(getCenter());
        // Log.v("GLDEMO",getId()+": setString() ends - Thread ID: "+Thread.currentThread().getId());
    }


    /**
     * Makes a deep copy of this shape.
     * @param id The id of the new shape.
     * @param context The context from which this method is called (can be null if the cache entries for the characters have already been created, e.g. by createCacheEntries()).
     * @return A reference to the new shape.
     */

    synchronized public GLStringShapeCV copy(String id, Context context) {
        GLStringShapeCV newShape = new GLStringShapeCV(id,string,faceColor,lineColor,lineWidth,propertyFlags,context);
        newShape.setInfoBundle(info);
        return newShape;
    }

    /**
     * @return true if the shape is three-dimensional, false if it is two-dimensional
     */

    public boolean isThreeD() {
        return (propertyFlags&GLTextUtilsCV.ThreeD)!=0;
    }

    /**
     * @return true if the shape is solid (i.e. has colored faces, false if it is a wireframe
     */

    public boolean isSolid() {
        return (propertyFlags&GLTextUtilsCV.Solid)!=0;
    }

    /**
     * @return true if the shape has face lines, false otherwise
     */

    public boolean hasFaceLines() {
        return (propertyFlags&GLTextUtilsCV.FaceLines)!=0;
    }

    /**
     * @return true if the shape has triangle lines, false otherwise
     */

    public boolean hasTriangleLines() {
        return (propertyFlags&GLTextUtilsCV.TriangleLines)!=0;
    }

    /**
     * @return true if the shape has lines, false otherwise
     */

    public boolean hasLines() {
        return hasFaceLines()||hasTriangleLines();
    }

    /**
     * @return the value of the string attribute
     */

    public String getString() {
        return this.string;
    }

    /**
     * @return the value of the propertyFlags attribute
     */

    public int getPropertyFlags() {
        return this.propertyFlags;
    }

    /**
     * @return a copy of the face color attribute (or null)
     */

    public float[] getFaceColor() {
        if (faceColor!=null)
            return faceColor.clone();
        else return null;
    }

    /**
     * @return a copy of the line color attribute (or null)
     */

    public float[] getLineColor() {
        if (lineColor!=null)
            return lineColor.clone();
        else return null;
    }

    /**
     * Deletes a character from this shape.
     * @param index The index/position of the character in the displayed string (if not a valid position nothing happens).
     */

    synchronized public void deleteCharAt(int index) {
        deleteCharAt(index,0);
    }

    /**
     * Deletes a character from this shape.
     * @param index The index/position of the character in the displayed string (if not a valid position nothing happens).
     * @param animDuration The duration of an animation that shows the deletion (in ms). If 0 or negative the deletion will not be animated.
     */

    synchronized public void deleteCharAt(int index, long animDuration) {
        if (index<0||index>string.length()-1) return;
        // calculate the width of the gap that will occur when removing the character
        float gapWidth;
        if (index==0||index==string.length()-1)
            gapWidth=0;
        else
            gapWidth = leftBorder(index+1)-rightBorder(index-1);
        // delete triangles for the i-th character
        if (isSolid()) {
            int deleteFrom = trianglesStartIndices[index];
            int deleteLength;
            if (index < string.length() - 1)
                deleteLength = trianglesStartIndices[index + 1] - deleteFrom;
            else
                deleteLength = triangles.length - deleteFrom;
            deleteTriangles(deleteFrom, deleteLength);
            int[] newStartIndices = new int[trianglesStartIndices.length-1];
            System.arraycopy(trianglesStartIndices,0,newStartIndices,0,index);
            for (int i=index;i<newStartIndices.length;i++)
                newStartIndices[i] = trianglesStartIndices[i+1]-(trianglesStartIndices[index+1]-trianglesStartIndices[index]);
            trianglesStartIndices = newStartIndices;
        }
        // delete lines for the i-th character
        if (hasLines()) {
            int deleteFrom = linesStartIndices[index];
            int deleteLength;
            if (index < string.length() - 1)
                deleteLength = linesStartIndices[index + 1] - deleteFrom;
            else
                deleteLength = lines.length - deleteFrom;
            deleteLines(deleteFrom, deleteLength);
            int[] newStartIndices = new int[linesStartIndices.length-1];
            System.arraycopy(linesStartIndices,0,newStartIndices,0,index);
            for (int i=index;i<newStartIndices.length;i++)
                newStartIndices[i] = linesStartIndices[i+1]-(linesStartIndices[index+1]-linesStartIndices[index]);
            linesStartIndices = newStartIndices;
        }
        // center the coordinates and update the string attribute
        // (if the first or the last character of the string have been deleted not gap needs to be closed)
        if (index==0) {
            moveZeroPointTo(getCenter());
            string = string.substring(1);
            return;
        }
        if (index==string.length()-1) {
            moveZeroPointTo(getCenter());
            string = string.substring(0,string.length()-1);
            return;
        }
        // close the gap by moving the triangles and lines of the characters right of the gap to the left
        float translateBy = -gapWidth+GLTextUtilsCV.defaultSpacing;
        String charPair = string.charAt(index-1)+""+string.charAt(index+1);
        if (GLTextUtilsCV.kerningAdjustmentTable == null)
            GLTextUtilsCV.initKerningAdjustmentTable();
        if (GLTextUtilsCV.kerningAdjustmentTable.containsKey(charPair))
            translateBy -= GLTextUtilsCV.kerningAdjustmentTable.get(charPair);
        if (animDuration<=0) {
            if (isSolid()) {
                for (int i=trianglesStartIndices[index];i<triangles.length;i++)
                    triangles[i].translate(translateBy,0,0);
            }
            if (hasLines()) {
                for (int i=linesStartIndices[index];i<lines.length;i++)
                    lines[i].translate(translateBy,0,0);
            }
        }
        else {
            final long msPerRound = 50;
            final int rounds = (int) (animDuration / msPerRound);
            final float transByPerRound = translateBy / rounds;
            (new Thread() {
                @Override
                public void run() {
                    super.run();
                    for (int r = 0; r < rounds; r++) {
                        if (isSolid()) {
                            for (int i = trianglesStartIndices[index]; i < triangles.length; i++)
                                triangles[i].translate(transByPerRound, 0, 0);
                            setTriangleBuffers();
                        }
                        if (hasLines()) {
                            for (int i = linesStartIndices[index]; i < lines.length; i++)
                                lines[i].translate(transByPerRound, 0, 0);
                            setLineBuffers();
                        }
                        try {
                            sleep(msPerRound);
                        } catch (InterruptedException exc) {
                        }
                    }
                }
            }).start();
        }
        string = string.substring(0,index)+string.substring(index+1);
        moveZeroPointTo(getCenter());
    }

    // TODO Insert character / character sequence
    // TODO Replace character / character sequence
    // TODO Refine animation

    /**
     * Left border of some character in the string shape, i.e. the smallest x coordinate value (in the model coordinate space) of all its triangles and lines.
     * <P>
     * Currently used only within the class GLStringShapeCV for calculating character positions.
     * @param charIndex The index of the character.
     * @return The left border (or Float.MAX_VALUE if the index is not valid)
     */

    synchronized private float leftBorder(int charIndex) {
        float leftBorder = Float.MAX_VALUE;
        if (charIndex<0||charIndex>=string.length()) return leftBorder;
        int numberOfLines=0;  // number of lines of the character
        if (hasLines())
            if (charIndex<string.length()-1)
                numberOfLines=linesStartIndices[charIndex+1]-linesStartIndices[charIndex];
            else
                numberOfLines=lines.length-linesStartIndices[charIndex];
        int numberOfTriangles=0;  // number of triangles of the character
        if (isSolid())
            if (charIndex<string.length()-1)
                numberOfTriangles=trianglesStartIndices[charIndex+1]-trianglesStartIndices[charIndex];
            else
                numberOfTriangles=triangles.length-trianglesStartIndices[charIndex];
        if (hasLines()&&(!isSolid()||(2*numberOfLines<3*numberOfTriangles))) {
            // if there are lines and no triangles or if there are more triangle vertices than line vertices, go through the line vertices
            int loopLimit;
            if (charIndex<string.length()-1)
                loopLimit=linesStartIndices[charIndex+1];
            else
                loopLimit=lines.length;
            for (int i=linesStartIndices[charIndex];i<loopLimit;i++) {
                float xCoord = lines[i].getPoint1()[0];
                if (xCoord<leftBorder)
                    leftBorder=xCoord;
                xCoord = lines[i].getPoint2()[0];
                if (xCoord<leftBorder)
                    leftBorder=xCoord;
            }
            return leftBorder;
        }
        // otherwise go through the triangle vertices
        int loopLimit;
        if (charIndex<string.length()-1)
            loopLimit=trianglesStartIndices[charIndex+1];
        else
            loopLimit=triangles.length;
        for (int i=trianglesStartIndices[charIndex];i<loopLimit;i++) {
            float coords[] = triangles[i].getVertexCoordinates();
            for (int j=0;j<9;j+=3)
                if (coords[j]<leftBorder)
                    leftBorder=coords[j];
        }
        return leftBorder;
    }

    /**
     * Right border of some character in the string shape, i.e. the largest x coordinate value (in the model coordinate space) of all its triangles and lines.
     * <P>
     * Currently used only within the class GLStringShapeCV for calculating character positions.
     * @param charIndex The index of the character.
     * @return The right border (or -Float.MAX_VALUE if the index is not valid)
     */

    synchronized private float rightBorder(int charIndex) {
        float rightBorder = -Float.MAX_VALUE;
        if (charIndex<0||charIndex>=string.length()) return rightBorder;
        int numberOfLines=0;  // number of lines of the character
        if (hasLines())
            if (charIndex<string.length()-1)
                numberOfLines=linesStartIndices[charIndex+1]-linesStartIndices[charIndex];
            else
                numberOfLines=lines.length-linesStartIndices[charIndex];
        int numberOfTriangles=0;  // number of triangles of the character
        if (isSolid())
            if (charIndex<string.length()-1)
                numberOfTriangles=trianglesStartIndices[charIndex+1]-trianglesStartIndices[charIndex];
            else
                numberOfTriangles=triangles.length-trianglesStartIndices[charIndex];
        if (hasLines()&&(!isSolid()||(2*numberOfLines<3*numberOfTriangles))) {
            // if there are lines and no triangles or if there are more triangle vertices than line vertices, go through the line vertices
            int loopLimit;
            if (charIndex<string.length()-1)
                loopLimit=linesStartIndices[charIndex+1];
            else
                loopLimit=lines.length;
            for (int i=linesStartIndices[charIndex];i<loopLimit;i++) {
                float xCoord = lines[i].getPoint1()[0];
                if (xCoord>rightBorder)
                    rightBorder=xCoord;
                xCoord = lines[i].getPoint2()[0];
                if (xCoord>rightBorder)
                    rightBorder=xCoord;
            }
            return rightBorder;
        }
        // otherwise go through the triangle vertices
        int loopLimit;
        if (charIndex<string.length()-1)
            loopLimit=trianglesStartIndices[charIndex+1];
        else
            loopLimit=triangles.length;
        for (int i=trianglesStartIndices[charIndex];i<loopLimit;i++) {
            float coords[] = triangles[i].getVertexCoordinates();
            for (int j=0;j<9;j+=3)
                if (coords[j]>rightBorder)
                    rightBorder=coords[j];
        }
        return rightBorder;
    }

}