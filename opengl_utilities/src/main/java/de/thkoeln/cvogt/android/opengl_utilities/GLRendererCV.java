// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 18.7.2023

package de.thkoeln.cvogt.android.opengl_utilities;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Class to define renderers. A renderer is attached to a surface view, i.e. an object of class <I>GLSurfaceViewCV</I>,
 * and calls the <I>draw()</I> methods of the shapes of this view.
 * <P>
 * A renderer also specifies and updates the View Projection Matrix to be applied to all shapes,
 *  i.e. the matrix that specifies the properties of the eye/camera looking at the collection of shapes
 *  and the projection of the shapes onto the 2D display.
 * <BR>
 * @see de.thkoeln.cvogt.android.opengl_utilities.GLSurfaceViewCV
 * @see GLShapeCV
 */

public class GLRendererCV implements GLSurfaceView.Renderer {

    /**
     * The associated surface view on which the renderer will draw the shapes.
     * The renderer will obtain the shapes from this view to call their draw() methods.
     */
    private GLSurfaceViewCV surfaceView;

    /**
     * Background color (as RGBA). Default value: black
     */

    public float[] clearColor;

    /** Initial value for the eye/camera position (x/y/z coordinates). */
    public static final float[] eyePosInit = { 0f, 0f, 5f };

    /** Initial value for the eye/camera focus (x/y/z coordinates). */
    public static final float[] eyeFocusInit = { 0f, 0f, 0f };

    /** Current eye/camera position (x/y/z coordinates). */
    private float[] eyePos = eyePosInit.clone();

    /** Current eye/camera focus (x/y/z coordinates). */
    private float[] eyeFocus = eyeFocusInit.clone();

    /** Frustum: near value. */
    public static final float frustumNear = 1f;

    /** Frustum: far value. */
    public static final float frustumFar = 1000f;

    /**
     * View matrix calculated from the eye/camera position (eyePos) and focus (eyeFocus).
     * It initialized in onSurfaceChanged() and updated each time the eye/camera position or focus changes.
     */

    private final float[] viewMatrix = new float[16];

    /**
     * Projection matrix to project the 3D coordinates to the 2D display. Is set in onSurfaceChanged() based on the display geometry.
     */

    private final float[] projectionMatrix = new float[16];

    /**
     * View projection matrix to be passed to the draw methods of the shapes.
     * <BR>
     * The matrix is the product of the projection matrix and the view matrix,
     * the view matrix being calculated from the eye/camera position (eyePos) and eye/camera focus (eyeFocus).
     * It initialized in onSurfaceChanged() and updated each time the eye/camera position or focus changes.
     */

    private final float[] viewProjectionMatrix = new float[16];

    /**
     * A shape whose current position shall be used as the origin of the point light.
     * This shape itself will not be lighted by the point light but only by the directional and ambient light if these are defined.
     * If no directional and ambient lighting is defined, no lighting at all will be applied to the shape, i.e. it will be displayed as it is.
     */

    private GLShapeCV pointLightSource = null;

    /**
     * Position of the point light source (x,y,z coordinates).
     * Will only be used if the pointLightSource attribute is null.
     * If this attribute and the pointLightSource and the directionalLightVector attribute are null no lighting is applied.
     */

    private float[] pointLightPos = null;

    /**
     * Vector of the directional light (x,y,z components).
     * If this attribute and the pointLightSource and the pointLightPos attributes are null no lighting is applied.
     */

    private float[] directionalLightVector = null;

    /**
     * The contribution of the point light to the total of point light and directional light,
     * as a percentage given by a value between 0 and 1.
     * Only valid if lighting is applied, i.e. pointLightSource, pointLightPos and directionalLightVector are not all null.
     */

    private float relativePointLightShare = 0;

    /**
     * The additional contribution of the ambient light,
     * as a small non-negative value.
     * Only valid if lighting is applied, i.e. pointLightSource, pointLightPos and directionalLightVector are not all null.
     */

    private float ambientLight = 0;

    /**
     * Constant to be used as a parameter for methods that modify the eye/camera position (eyePos attrib).
     * It specifies if and how the eye/camera focus point (eyeFocus attribute) shall be modified as well.
     * CONSTANT_FOCUS_POINT = The focus point remains unchanged,
     * i.e. when the eye/camera is moved it is simultaneously turned to remain focused at the same point.
     */

    public static final int CONSTANT_FOCUS_POINT = 0;

    /**
     * Constant to be used as a parameter for methods that modify the eye/camera position (eyePos attrib).
     * It specifies if and how the eye/camera focus point (eyeFocus attribute) shall be modified as well.
     * CONSTANT_FOCUS_VECTOR = The focus point is modified such that the vector between the eye/camera position and the focus point remains the same,
     * i.e. when the eye/camera is moved it keeps looking into the same direction.
     */

    public static final int CONSTANT_FOCUS_VECTOR = 1;

    /**
     * Constant to be used as a parameter for methods that modify the eye/camera position (eyePos attrib).
     * It specifies if and how the eye/camera focus point (eyeFocus attribute) shall be modified as well.
     * FOCUS_INTO_MOVEMENT_DIRECTION = The focus point is modified such that
     * the vector between the eye/camera position and the focus points into the direction of the current movement of the camera,
     * i.e. when the eye/camera is moved it keeps looking ahead.
     */

    public static final int FOCUS_INTO_MOVEMENT_DIRECTION = 2;

    /**
     * Constructor. Sets the clearColor attribute (= background color) to black.
     */

    public GLRendererCV() {
        this(GraphicsUtilsCV.black);

    }

    /**
     * Constructor.
     * @param clearColor the background color
     */

    public GLRendererCV(float[] clearColor) {
        this.clearColor = clearColor.clone();
    }

    /**
     * @param surfaceView The surface view to which this renderer shall be attached.
     */

    public void setSurfaceView(GLSurfaceViewCV surfaceView) {
        this.surfaceView = surfaceView;
    }

    /**
     * Get the surface view to which the renderer is currently attached.
     */

    synchronized public GLSurfaceViewCV getSurfaceView() {
        return surfaceView;
    }

    /**
     * Method called by the runtime system when the associated surface view has been initialized.
     * Colors the background black and compiles the OpenGL programs of the shapes to be displayed (i.e. the shapes attached to the associated surface view).
     * Prepares the textures for textured shapes.
     */

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        ArrayList<GLShapeCV> shapesToRender = surfaceView.getShapesToRender();
        for (GLShapeCV shape : shapesToRender) {
            shape.initOpenGLPrograms();
            // shape.prepareTextures();
        }
    }

    /**
     * Method called by the runtime system when the geometry of the display changes.
     * Sets the projection matrix.
     */

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        float ratio = (float) width / height;
        Matrix.frustumM(projectionMatrix, 0, -ratio, +ratio, -1, 1, frustumNear, frustumFar);
        updateViewProjectionMatrix();
    }

    /**
     * Method called by the runtime system when the surface view shall been drawn, i.e. its shapes shall be rendered.
     */

    @Override
    synchronized public void onDrawFrame(GL10 gl10) {
        // Log.v("GLDEMO","Renderer: onDrawFrame() - Thread ID: "+Thread.currentThread().getId());
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT|GLES20.GL_DEPTH_BUFFER_BIT);  // clear the buffers before drawing the shapes
        GLES20.glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);  // such that fragments in the front ...
        GLES20.glDepthFunc(GLES20.GL_LESS);     // ... hide fragments in the back
        GLES20.glDepthMask( true );
        // draw the shapes based on the current view projection matrix
        // start = (new Date()).getTime();
        // long start = System.nanoTime();
        try {
            for (GLShapeCV shape : surfaceView.getShapesToRender()) {
                // if (!shape.isCompiled())
                //    shape.initOpenGLPrograms();
                if (shape == null) continue;
                if (pointLightSource == null)
                    shape.draw(viewProjectionMatrix, viewMatrix, pointLightPos, directionalLightVector, relativePointLightShare, ambientLight);
                else if (shape == pointLightSource)
                    if (directionalLightVector != null || ambientLight > 0)
                        shape.draw(viewProjectionMatrix, viewMatrix, null, directionalLightVector, 0, ambientLight);
                    else
                        shape.draw(viewProjectionMatrix, viewMatrix, null, null, 0, 0);
                else
                    shape.draw(viewProjectionMatrix, viewMatrix, pointLightSource.getTrans(), directionalLightVector, relativePointLightShare, ambientLight);
            }
            // duration = (new Date()).getTime() - start;
            // long duration = System.nanoTime() - start;
            // Log.v("GLDEMO",">>> Draw "+duration+" ns");
        } catch (ConcurrentModificationException exc) {
            // In some cases, ConcurrentModificationExceptions have been observed when executing this loop.
            // In case of such an exception, this call of onDrawFrame() just skips the drawing.
        }
    }

    /**
     * Get a copy of the current projection matrix.
     */

    synchronized public float[] getProjectionMatrix() {
        return projectionMatrix.clone();
    }

    /**
     * Get a copy of the current projection matrix.
     */

    synchronized public float[] getViewMatrix() {
        return viewMatrix.clone();
    }

    /**
     * Get a copy of the current view projection matrix.
     */

    synchronized public float[] getViewProjectionMatrix() {
        return viewProjectionMatrix.clone();
    }

    /**
     * Calculates the current view matrix from the eye/camera position (eyePos) and eye/camera focus (eyeFocus).
     * Updates the view projection matrix as the product of the projection matrix and the view matrix.
     * Is called initially from onSurfaceChanged() and updated each time the eye/camera position or focus changes.
     */

    private void updateViewProjectionMatrix() {
        Matrix.setLookAtM(viewMatrix, 0,
                eyePos[0], eyePos[1], eyePos[2],
                eyeFocus[0], eyeFocus[1], eyeFocus[2],
                // vector specifying the "up direction" as seen from the eye/camera
                0f, 1f, 0f);
        Matrix.multiplyMM(viewProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
        surfaceView.requestRender();
    }

    /**
     * Gets the current eye/camera position value in the x dimension.
     * @return The current value of eyePos[0].
     */

    synchronized public float getEyePosX() {
        return eyePos[0];
    }

    /**
     * Gets the current eye/camera position value in the y dimension.
     * @return The current value of eyePos[1].
     */

    synchronized public float getEyePosY() {
        return eyePos[1];
    }

    /**
     * Gets the current eye/camera position value in the z dimension.
     * @return The current value of eyePos[2].
     */

    synchronized public float getEyePosZ() {
        return eyePos[2];
    }

    /**
     * Gets a copy of the current eye/camera position (eyePos attribute).
     */

    synchronized public float[] getEyePos() {
        return eyePos.clone();
    }

    /**
     * Gets the current eye/camera focus value in the x dimension.
     * @return The current value of eyeFocus[0].
     */

    synchronized public float getEyeFocusX() {
        return eyeFocus[0];
    }

    /**
     * Gets the current eye/camera focus value in the y dimension.
     * @return The current value of eyeFocus[1].
     */

    synchronized public float getEyeFocusY() {
        return eyeFocus[1];
    }

    /**
     * Gets the current eye/camera focus value in the z dimension.
     * @return The current value of eyeFocus[2].
     */

    synchronized public float getEyeFocusZ() {
        return eyeFocus[2];
    }

    /**
     * Gets a copy of the current eye/camera focus (eyeFocus attribute).
     */

    synchronized public float[] getEyeFocus() {
        return eyeFocus.clone();
    }

    /**
     * Sets the eye/camera position value in the x dimension (i.e. eyePos[0]). Updates the view projection matrix accordingly.
     * @param eyePosX The new value for eyePos[0].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePosX(float eyePosX) {
        this.eyePos[0] = eyePosX;
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the eye/camera position value in the y dimension (i.e. eyePos[1]). Updates the view projection matrix accordingly.
     * @param eyePosY The new value for eyePos[1].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePosY(float eyePosY) {
        this.eyePos[1] = eyePosY;
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the eye/camera position value in the z dimension (i.e. eyePos[2]). Updates the view projection matrix accordingly.
     * @param eyePosZ The new value for eyePos[2].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePosZ(float eyePosZ) {
        this.eyePos[2] = eyePosZ;
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the eye/camera position value (eyePos attribute). Updates the view projection matrix accordingly.
     * @param eyePosX The new value for eyePos[0].
     * @param eyePosY The new value for eyePos[1].
     * @param eyePosZ The new value for eyePos[2].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePos(float eyePosX, float eyePosY, float eyePosZ) {
        return setEyePos(new float[]{eyePosX,eyePosY,eyePosZ});
    }

    /**
     * Sets the eye/camera position value (eyePos attribute). Updates the view projection matrix accordingly.
     * If the parameter is not valid the attribute will remain unchanged.
     * @param eyePos The values to be set.
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePos(float[] eyePos) {
        return setEyePos(eyePos,CONSTANT_FOCUS_POINT);
        /*
        if (eyePos==null||eyePos.length!=3) return this;
        this.eyePos = eyePos.clone();
        updateViewProjectionMatrix();
        return this;

         */
    }

    /**
     * Sets the eye/camera position value (eyePos attribute)
     * and potentially adapts the eye/camera focus value (eyeFocus attribute).
     * Updates the view projection matrix accordingly.
     * If the eyePos parameter is not valid nothing will be done.
     * @param eyePos The new values for the eyePos attribute.
     * @param eyeFocusControl Specification it and how the eye/camera focus shall be adapted
     *                        (CONSTANT_FOCUS_POINT, CONSTANT_FOCUS_VECTOR, or FOCUS_INTO_MOVEMENT_DIRECTION;
     *                         all other parameter values will be treated like CONSTANT_FOCUS_POINT).
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePos(float[] eyePos, int eyeFocusControl) {
        if (eyePos==null||eyePos.length!=3) return this;
        if (eyeFocusControl!=CONSTANT_FOCUS_VECTOR&&eyeFocusControl!=FOCUS_INTO_MOVEMENT_DIRECTION) {
            this.eyePos = eyePos.clone();
            updateViewProjectionMatrix();
            return this;
        }
        if (eyeFocusControl==CONSTANT_FOCUS_VECTOR)
            this.eyeFocus = new float[] { this.eyeFocus[0]+(eyePos[0]-this.eyePos[0]), this.eyeFocus[1]+(eyePos[1]-this.eyePos[1]), this.eyeFocus[2]+(eyePos[2]-this.eyePos[2]) };
        if (eyeFocusControl==FOCUS_INTO_MOVEMENT_DIRECTION)
            this.eyeFocus = new float[] { 2*eyePos[0]-this.eyePos[0], 2*eyePos[1]-this.eyePos[1], 2*eyePos[2]-this.eyePos[2] };
                                                  // new focus point = new position (eyePos) plus current movement direction (eyePos-this.eyePos)
        this.eyePos = eyePos.clone();
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the eye/camera position value in the x dimension (i.e. eyeFocus[0]). Updates the view projection matrix accordingly.
     * @param eyeFocusX The new value for eyeFocus[0].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyeFocusX(float eyeFocusX) {
        this.eyeFocus[0] = eyeFocusX;
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the eye/camera position value in the y dimension (i.e. eyeFocus[1]). Updates the view projection matrix accordingly.
     * @param eyeFocusY The new value for eyeFocus[1].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyeFocusY(float eyeFocusY) {
        this.eyeFocus[1] = eyeFocusY;
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the eye/camera position value in the z dimension (i.e. eyeFocus[2]). Updates the view projection matrix accordingly.
     * @param eyeFocusZ The new value for eyeFocus[2].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyeFocusZ(float eyeFocusZ) {
        this.eyeFocus[2] = eyeFocusZ;
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the eye/camera focus value (eyeFocus attribute). Updates the view projection matrix accordingly.
     * @param eyeFocusX The new value for eyeFocus[0].
     * @param eyeFocusY The new value for eyeFocus[1].
     * @param eyeFocusZ The new value for eyeFocus[2].
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyeFocus(float eyeFocusX, float eyeFocusY, float eyeFocusZ) {
        return setEyeFocus(new float[]{eyeFocusX,eyeFocusY,eyeFocusZ});
    }

    /**
     * Sets the eye/camera focus value (eyeFocus attribute). Updates the view projection matrix accordingly.
     * If the parameter is not valid the attribute will remain unchanged.
     * @param eyeFocus The values to be set.
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyeFocus(float[] eyeFocus) {
        if (eyeFocus==null||eyeFocus.length!=3) return this;
        this.eyeFocus = eyeFocus.clone();
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the values of eye/camera position and focus (attributes eyePos and eyeFocus). Updates the projection view matrix accordingly.
     * @param eyePosX The new value for eyePos[0]
     * @param eyePosY The new value for eyePos[1]
     * @param eyePosZ The new value for eyePos[2]
     * @param eyeFocusX The new value for eyeFocus[0]
     * @param eyeFocusY The new value for eyeFocus[1]
     * @param eyeFocusZ The new value for eyeFocus[2]
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePosAndFocus(float eyePosX, float eyePosY, float eyePosZ, float eyeFocusX, float eyeFocusY, float eyeFocusZ) {
        return setEyePosAndFocus(new float[] { eyePosX, eyePosY, eyePosZ }, new float[] { eyeFocusX, eyeFocusY, eyeFocusZ });
    }

    /**
     * Sets the values of eye/camera position and focus (attributes eyePos and eyeFocus). Updates the view projection matrix accordingly.
     * If one of the parameters is not valid the attributes will remain unchanged.
     * @param eyePos An array of length 3 with the new values for the eyePos[] attribute.
     * @param eyeFocus An array of length 3 with the new values for the eyeFocus[] attribute.
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePosAndFocus(float[] eyePos, float[] eyeFocus) {
        if (eyePos==null||eyePos.length!=3||eyeFocus==null||eyeFocus.length!=3) return this;
        this.eyePos = eyePos.clone();
        this.eyeFocus = eyeFocus.clone();
        updateViewProjectionMatrix();
        return this;
    }

    /**
     * Sets the values of eye/camera position and focus (attributes eyePos and eyeFocus). Updates the view projection matrix accordingly.
     * If the parameter is not valid the attributes will remain unchanged.
     * @param eyePosAndFocus The new values for the eyePos[] attribute (in positions 0-2) and the eyeFocus[] attribute (in positions 3-5)
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV setEyePosAndFocus(float[] eyePosAndFocus) {
        return setEyePosAndFocus(eyePosAndFocus[0],eyePosAndFocus[1],eyePosAndFocus[2],eyePosAndFocus[3],eyePosAndFocus[4],eyePosAndFocus[5]);
    }

    /**
     * Resets the eye/camera position and focus (attributes eyePos and eyeFocus) to their initial values. Updates the view projection matrix accordingly.
     * @return The renderer such that method calls of this type can be daisy-chained.
     */

    synchronized public GLRendererCV resetEyePosAndFocus() {
        return setEyePosAndFocus(eyePosInit, eyeFocusInit);
    }

    /**
     * Sets the pointLightSource attribute
     * @param pointLightSource
     */

    synchronized public void setPointLightSource(GLShapeCV pointLightSource) {
        this.pointLightSource = pointLightSource;
    }

    /**
     * Sets the lighting specification (NB: does not work for shapes with lines and/or textures yet)
     * @param pointLightPos see pointLightPos attribute
     * @param directionalLightVector see directionalLightVector attribute
     * @param relativePointLightShare see relativePointLightShare attribute
     * @param ambientLight see ambientLight attribute
     */

    synchronized public void setLighting(float[] pointLightPos, float[] directionalLightVector, float relativePointLightShare, float ambientLight) {
        if (pointLightPos!=null)
            this.pointLightPos = pointLightPos.clone();
        else
            this.pointLightPos = null;
        if (directionalLightVector!=null)
            this.directionalLightVector = directionalLightVector.clone();
        else
            this.directionalLightVector = null;
        this.relativePointLightShare = relativePointLightShare;
        this.ambientLight = ambientLight;
    }

    /**
     * Switches off lighting by setting pointLightPos and directionalLightVector to null
     * and relativePointLightShare and ambientLight to zero.
     */

    synchronized public void switchOffLighting() {
        pointLightSource = null;
        pointLightPos = null;
        directionalLightVector = null;
        relativePointLightShare = 0;
        ambientLight = 0;
    }

}
