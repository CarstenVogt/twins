// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 26.11.2023

package de.thkoeln.cvogt.android.opengl_utilities;

import android.content.res.Resources;

import java.util.ArrayList;

/**
 * Class with some utility methods for the OpenGL-related classes and objects defined in this package.
 * The class GraphicsUtilsCV contains utility methods for 2D and 3D graphics programming in general.
 * @see GraphicsUtilsCV
 */

public class GLUtilsCV {

    private GLUtilsCV() {
        throw new IllegalStateException("Class not instantiable");
    }

    /**
     * Method to calculate the maximum visible x coordinate for a given z coordinate,
     * i.e. for this z, the x coordinate of the point on the right border of the frustum.
     * It assumes that the eye/camera is placed on the z axis and looks into the negative z direction,
     * i.e. the x and y coordinates of the eye position and the eye focus are 0.
     * The method therefore depends on the z position of the eye/camera and the position and the size of the near plane of the frustum.
     * <P>
     * N.B.: The value of the viewportWidth parameter (see parameter list) can be obtained from the underlying surface view by calling its getWidth() and getHeight() methods.
     * These methods, however, will return meaningful values only after the surface view has been rendered.
     * Its is therefore recommend to place a call of this method into a Runnable object
     * and let this Runnable be executed through the surface view's post() method:
     * surfaceView.post(new Runnable() { public void run() { ... method call ... } });
     *
     * @param targetZ The given z coordinate
     * @param eyeZ The z coordinate of the eye/camera position
     * @param frustumNear The distance of the near plane of the frustum to the camera (usually GLRendererCV.frustumNear)
     * @param maxVisibleXOnViewPort The maximum visible x coordinate on the near plane of the frustum (usually the quotient width/height of the displaying surface view)
     * @return The maximum visible x coordinate
     */
    public static float maxVisibleX(float targetZ, float eyeZ, float frustumNear, float maxVisibleXOnViewPort) {
        return (eyeZ-targetZ)*maxVisibleXOnViewPort/frustumNear;  // calculation based on the intercept theorem ("Strahlensatz")
    }

    /**
     * Method to calculate the minimum visible x coordinate for a given z coordinate,
     * i.e. for this z, the x coordinate of the point on the left border of the frustum.
     * It assumes that the eye/camera is placed on the z axis and looks into the negative z direction,
     * i.e. the x and y coordinates of the eye position and the eye focus are 0.
     * The method therefore depends on the z position of the eye/camera and the position and the size of the near plane of the frustum.
     * <P>
     * N.B.: The value of the viewportWidth parameter (see parameter list) can be obtained from the underlying surface view by calling its getWidth() and getHeight() methods.
     * These methods, however, will return meaningful values only after the surface view has been rendered.
     * It is therefore recommended to place a call of this method into a Runnable object
     * and let this Runnable be executed through the surface view's post() method:
     * surfaceView.post(new Runnable() { public void run() { ... method call ... } });
     *
     * @param targetZ The given z coordinate
     * @param eyeZ The z coordinate of the eye/camera position
     * @param frustumNear The distance of the near plane of the frustum to the camera (usually GLRendererCV.frustumNear)
     * @param minVisibleXOnViewPort The minimum visible x coordinate on the near plane of the frustum (usually the quotient -width/height of the displaying surface view)
     * @return The minimum visible x coordinate
     */
    public static float minVisibleX(float targetZ, float eyeZ, float frustumNear, float minVisibleXOnViewPort) {
        return (eyeZ-targetZ)*minVisibleXOnViewPort/frustumNear;  // calculation based on the intercept theorem ("Strahlensatz")
    }

    /**
     * Method to calculate the maximum visible y coordinate for a given z coordinate,
     * i.e. for this z, the y coordinate of the point on the upper border of the frustum.
     * It assumes that the eye/camera is placed on the z axis and looks into the negative z direction,
     * i.e. the x and y coordinates of the eye position and the eye focus are 0.
     * The method therefore depends on the z position of the eye/camera and the position and the size of the near plane of the frustum.
     *
     * @param targetZ The given z coordinate
     * @param eyeZ The z coordinate of the eye/camera position
     * @param frustumNear The distance of the near plane of the frustum to the camera (usually GLRendererCV.frustumNear)
     * @param maxVisibleYOnViewPort The maximum visible y coordinate on the near plane of the frustum (usually 1)
     * @return The maximum visible y coordinate
     */

    public static float maxVisibleY(float targetZ, float eyeZ, float frustumNear, float maxVisibleYOnViewPort) {
        return (eyeZ-targetZ)*maxVisibleYOnViewPort/frustumNear;  // calculation based on the intercept theorem ("Strahlensatz")
    }

    /**
     * Method to calculate the minimum visible y coordinate for a given z coordinate,
     * i.e. for this z, the y coordinate of the point on the lower border of the frustum.
     * It assumes that the eye/camera is placed on the z axis and looks into the negative z direction,
     * i.e. the x and y coordinates of the eye position and the eye focus are 0.
     * The method therefore depends on the z position of the eye/camera and the position and the size of the near plane of the frustum.
     *
     * @param targetZ The given z coordinate
     * @param eyeZ The z coordinate of the eye/camera position
     * @param frustumNear The distance of the near plane of the frustum to the camera (usually GLRendererCV.frustumNear)
     * @param minVisibleYOnViewPort The minimum visible y coordinate on the near plane of the frustum (usually -1)
     * @return The minimum visible y coordinate
     */

    public static float minVisibleY(float targetZ, float eyeZ, float frustumNear, float minVisibleYOnViewPort) {
        return (eyeZ-targetZ)*minVisibleYOnViewPort/frustumNear;  // calculation based on the intercept theorem ("Strahlensatz")
    }

    /** Method to test if a ray goes through a shape.
     * <P>
     * N.B. The current implementation of the test is rather basic.
     * It just checks if the distance between the ray and the center of the shape is smaller than the arithmetic mean of the current sizes of the shape in the x, y,, and z dimension.
     * <P>
     * A precise implementation should check for each triangle of the shape if the ray goes through the triangle,
     * e.g. by the Moeller-Trumbore algorithm (https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm)
     * or the Badouel algorithm (https://en.wikipedia.org/wiki/Badouel_intersection_algorithm).
     *
     * @param rayStart The start point of the ray (x/y/z coordinates).
     * @param rayVector The direction vector of the ray (x/y/z coordinates).
     * @param shape The shape.
     * @return true iff the distance between the center of the shape and the ray is smaller than the above-mentioned value.
     * (false if one of the parameters is not valid).
     */

    public static boolean rayGoesThroughShape(float[] rayStart, float[] rayVector, GLShapeCV shape) {
        if (rayStart==null||rayStart.length!=3||rayVector==null||rayVector.length!=3||shape==null) return false;
        float sizeX = shape.getIntrinsicSize(0)*shape.getScaleX();
        float sizeY = shape.getIntrinsicSize(1)*shape.getScaleY();
        float sizeZ = shape.getIntrinsicSize(2)*shape.getScaleZ();
        float threshold = (sizeX+sizeY+sizeZ)/3;
        return (GraphicsUtilsCV.distancePointToLine3D(shape.getTrans(),rayStart,rayVector)<threshold);
    }


    /** Method to find, in a set of shapes, those shapes through which a specific ray goes.
     * <P>
     * N.B. The current implementation of the method is rather basic - see method rayGoesThroughShape().
     *
     * @param rayStart The start point of the ray (x/y/z coordinates).
     * @param rayVector The direction vector of the ray (x/y/z coordinates).
     * @param shapes The set of shapes.
     * @return An ArrayList containing those shapes through which the ray goes (or null if one of the parameters is not vald).
     */

    public static ArrayList<GLShapeCV> shapesOnRay(float[] rayStart, float[] rayVector, ArrayList<GLShapeCV> shapes) {
        if (rayStart==null||rayStart.length!=3||rayVector==null||rayVector.length!=3||shapes==null||shapes.size()==0) return null;
        ArrayList<GLShapeCV> result = new ArrayList<>();
        for (GLShapeCV shape : shapes)
            if (rayGoesThroughShape(rayStart,rayVector,shape))
                result.add(shape);
        return result;
    }

    /** Method to find, in a set of shapes, all shapes through which a specific ray goes
     * and then, among these shapes, the shape closest to the start point of the ray.
     * <P>
     * N.B. The current implementation of the method is rather basic - see method rayGoesThroughShape().
     *
     * @param rayStart The start point of the ray (x/y/z coordinates).
     * @param rayVector The direction vector of the ray (x/y/z coordinates).
     * @param shapes The set of shapes.
     * @return The shape on the ray closest to the start point (or null if one of the parameters is not valid).
     */

    public static GLShapeCV closestShapeOnRay(float[] rayStart, float[] rayVector, ArrayList<GLShapeCV> shapes) {
        if (rayStart==null||rayStart.length!=3||rayVector==null||rayVector.length!=3||shapes==null||shapes.size()==0) return null;
        return shapeClosestToPoint(rayStart,shapesOnRay(rayStart,rayVector,shapes));
    }

    /** Method to find, in a set of shapes, the shape whose center lies closest to a given point.
     *
     * @param point The point (x/y/z coordinates).
     * @param shapes The set of shapes.
     * @return The shape closest to the point (or null if one of the parameters is not valid).
     */

    public static GLShapeCV shapeClosestToPoint(float[] point, ArrayList<GLShapeCV> shapes) {
        if (point==null||point.length!=3||shapes==null||shapes.size()==0) return null;
        GLShapeCV result = shapes.get(0);
        float minDist = GraphicsUtilsCV.distance3D(point,result.getTrans());
        for (GLShapeCV shape : shapes) {
            float newDist = GraphicsUtilsCV.distance3D(point,shape.getTrans());
            if (newDist<minDist) {
                minDist = newDist;
                result = shape;
            }
        }
        return result;
    }

}