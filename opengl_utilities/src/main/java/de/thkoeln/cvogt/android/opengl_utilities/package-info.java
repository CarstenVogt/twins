/**
 Package with utility classes for programming OpenGL-ES-based applications under Android.
*/

package de.thkoeln.cvogt.android.opengl_utilities;

