// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 5.6.2024

// Some code in this class is based on the bachelor thesis of Ahmed Ez Aldeen Bobaki, May 2023.
// Especially most of the OBJ files for the characters have been created by A. Bobaki.
// The code for importing these OBJ files is taken from the bachelor thesis of Cem Cetin Kalkan,April 2023.

package de.thkoeln.cvogt.android.opengl_utilities.textutilities;

import android.content.Context;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;

import de.thkoeln.cvogt.android.opengl_utilities.GLLineCV;
import de.thkoeln.cvogt.android.opengl_utilities.GLShapeCV;
import de.thkoeln.cvogt.android.opengl_utilities.GLTriangleCV;
import de.thkoeln.cvogt.android.opengl_utilities.GraphicsUtilsCV;

/**
 * Class with some utility methods for creating and displaying 2D and 3D shapes for single characters and character strings.
 * @see GraphicsUtilsCV
 * @see GLCharShapeCV
 * @see GLStringShapeCV
 */

public class GLTextUtilsCV {

    private GLTextUtilsCV() {
        throw new IllegalStateException("Class not instantiable");
    }

    /**
     * The path of the directory with the OBJ files for the character shapes
     */

    public static final String directoryPathToObjFiles = "assets://charShapesObjFiles";

    /**
     * The path of the directory with the files containing the triangle and line coordinates of the character shapes
     */

    public static final String directoryPathToCharCoordinatesFiles = "assets://charShapesCoordinates";

    /**
     * Scale factor to multiply the coordinates in the OBJ files with,
     * will generate character shapes of unit sizes
     * (see comment on makeCharacter3D()).
     */

    public static final float scaleFactorForObjCoordinates = 22.3713661547f;

    /**
     * The default spacing value to be applied when placing character shapes to form a word (see method horizDistanceCharShapes()).
     */

    public static final float defaultSpacing = 0.18f;

    /**
     * Flag for a property of a character shape: 3D, i.e. spatial
     */

    public static final int ThreeD = 0x00000001;

    /**
     * Flag for a property of a character shape: 2D shape, i.e. flat
     */

    public static final int TwoD = 0x00000002;

    /**
     * Flag for a property of a character shape: solid, i.e. with colored faces
     */

    public static final int Solid = 0x00000004;

    /**
     * Flag for a property of a character shape: wireframe, i.e. with lines and transparent faces
     */

    public static final int Wireframe = 0x00000008;

    /**
     * Flag for a property of a character shape: with face lines, i.e. only those lines that mark the borders of the faces but no interior lines within the faces
     */

    public static final int FaceLines = 0x00000010;

    /**
     * Flag for a property of a character shape: with triangle lines, i.e. all lines that mark the borders of the triangles of the shape
     */

    public static final int TriangleLines = 0x00000020;

    /**
     * String with all supported characters
     */

    public static final String supportedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜabcdefghijklmnopqrstuvwxyzäöüß0123456789!?#$€@[](){}<> +-*/=_:.,;\"";

    // The HashMaps declared below define the triangles and lines of the shapes for the supported characters. They can be used for a speedy creation of such shapes.

    private static boolean hashMapsInitialized = false;

    /**
     * Triangle coordinates of 3D character shapes (key: character, value: array with the coordinates of the triangle vertices
     * - linearized as returned by the GLShapeCV method getTriangleVertexCoordinates())
     * @see GLShapeCV#getTriangleVertexCoordinates()
     */

    public static HashMap<Character, float[]> chars3D_TriangleCoords;

    /**
     * Triangle coordinates of 2D character shapes (key: character, value: array with the coordinates of the triangle vertices
     * - linearized as returned by the GLShapeCV method getTriangleVertexCoordinates())
     * @see GLShapeCV#getTriangleVertexCoordinates()
     */

    public static HashMap<Character, float[]> chars2D_TriangleCoords;

    /**
     * Triangle lines coordinates of 3D character shapes (key: character, value: array with the coordinates of the lines bordering the triangles
     * - linearized as returned by the GLLineCV method getPointCoordinates())
     * @see GLLineCV#getPointCoordinates()
     */

    public static HashMap<Character, float[]> chars3D_TriangleLinesCoords;

    /**
     * Triangle lines coordinates of 2D character shapes (key: character, value: array with the coordinates of the lines bordering the triangles
     * - linearized as returned by the GLLineCV method getPointCoordinates())
     * @see GLLineCV#getPointCoordinates()
     */

    public static HashMap<Character, float[]> chars2D_TriangleLinesCoords;

    /**
     * Face lines coordinates of 3D character shapes (key: character, value: array with the coordinates of the lines bordering the faces
     * - linearized as returned by the GLLineCV method getPointCoordinates())
     * @see GLLineCV#getPointCoordinates()
     */

    public static HashMap<Character, float[]> chars3D_FaceLinesCoords;

    /**
     * Face lines coordinates of 2D character shapes (key: character, value: array with the coordinates of the lines bordering the face
     * - linearized as returned by the GLLineCV method getPointCoordinates())
     * @see GLLineCV#getPointCoordinates()
     */

    public static HashMap<Character, float[]> chars2D_FaceLinesCoords;

    /**
     * Triangles of 3D character shapes (key: character, value: array with the triangles)
     */

    public static HashMap<Character, GLTriangleCV[]> chars3D_Triangles;

    /**
     * Triangles of 3D character shapes (key: character, value: array with the triangles)
     */

    public static HashMap<Character, GLTriangleCV[]> chars2D_Triangles;

    /**
     * Face lines of 3D character shapes (key: character, value: array with the lines)
     */

    public static HashMap<Character, GLLineCV[]> chars3D_FaceLines;

    /**
     * Face lines of 2D character shapes (key: character, value: array with the lines)
     */

    public static HashMap<Character, GLLineCV[]> chars2D_FaceLines;

    /**
     * Triangle lines of 3D character shapes (key: character, value: array with the lines)
     */

    public static HashMap<Character, GLLineCV[]> chars3D_TriangleLines;

    /**
     * Triangle lines of 2D character shapes (key: character, value: array with the lines)
     */

    public static HashMap<Character, GLLineCV[]> chars2D_TriangleLines;

    /**
     * Static method to initialize the HashMaps.
     * It reads the coordinates (i.e. values for the six xxxCoords HashMaps) from files
     * and then generates the entries for the triangle and lines HashMaps from these coordinates.
     * <BR>
     * The method should finish quickly (about 270 ms execution time on a Samsung Galaxy S21).
     * If not, it should be considered running it by a dedicated background thread.
     * <BR>
     * If a character shape is specified by an OBJ file
     * the method createHashmapEntriesFromObjFile() should be used instead.
     * @param context The context from which this method is called.
     * @return Successful execution.
     */

    public static boolean initHashMaps(Context context) {
        if (hashMapsInitialized) return true;
        // read triangle and line coordinates from the asset files
        // Date start = new Date();
        try {
            String filePath = GLTextUtilsCV.directoryPathToCharCoordinatesFiles + "/Chars3D_TriangleCoords";
            ObjectInputStream objIn = new ObjectInputStream(context.getAssets().open(filePath.substring(9)));
            chars3D_TriangleCoords = (HashMap<Character, float[]>) objIn.readObject();
            objIn.close();
            filePath = GLTextUtilsCV.directoryPathToCharCoordinatesFiles + "/Chars2D_TriangleCoords";
            objIn = new ObjectInputStream(context.getAssets().open(filePath.substring(9)));
            chars2D_TriangleCoords = (HashMap<Character, float[]>) objIn.readObject();
            objIn.close();
            filePath = GLTextUtilsCV.directoryPathToCharCoordinatesFiles + "/Chars3D_TriangleLinesCoords";
            objIn = new ObjectInputStream(context.getAssets().open(filePath.substring(9)));
            chars3D_TriangleLinesCoords = (HashMap<Character, float[]>) objIn.readObject();
            objIn.close();
            filePath = GLTextUtilsCV.directoryPathToCharCoordinatesFiles + "/Chars2D_TriangleLinesCoords";
            objIn = new ObjectInputStream(context.getAssets().open(filePath.substring(9)));
            chars2D_TriangleLinesCoords = (HashMap<Character, float[]>) objIn.readObject();
            objIn.close();
            filePath = GLTextUtilsCV.directoryPathToCharCoordinatesFiles + "/Chars3D_FaceLinesCoords";
            objIn = new ObjectInputStream(context.getAssets().open(filePath.substring(9)));
            chars3D_FaceLinesCoords = (HashMap<Character, float[]>) objIn.readObject();
            objIn.close();
            filePath = GLTextUtilsCV.directoryPathToCharCoordinatesFiles + "/Chars2D_FaceLinesCoords";
            objIn = new ObjectInputStream(context.getAssets().open(filePath.substring(9)));
            chars2D_FaceLinesCoords = (HashMap<Character, float[]>) objIn.readObject();
            objIn.close();
        } catch(Exception exc) {
            Log.v("GLDEMO","Exception: "+exc.toString());
            return false;
        }
        // Date end = new Date();
        // Log.v("GLDEMO","Duration initHashMaps() - read HashMap entries for triangle and line coordinates from files: "+(end.getTime()-start.getTime()));
        // create the corresponding triangles and lines of the character shapes and store them in the HashMaps
        // start = new Date();
        chars3D_Triangles = new HashMap<>();
        for (char key : chars3D_TriangleCoords.keySet()) {
            float[] coordinates = chars3D_TriangleCoords.get(key);
            int index = 0;
            GLTriangleCV[] triangles = new GLTriangleCV[coordinates.length/9];
            do {
                triangles[index/9] = new GLTriangleCV("", Arrays.copyOfRange(coordinates, index, index + 3), Arrays.copyOfRange(coordinates, index + 3, index + 6), Arrays.copyOfRange(coordinates, index + 6, index + 9));
                index += 9;
            } while (index<coordinates.length);
            chars3D_Triangles.put(key,triangles);
        }
        chars2D_Triangles = new HashMap<>();
        for (char key : chars2D_TriangleCoords.keySet()) {
            float[] coordinates = chars2D_TriangleCoords.get(key);
            int index = 0;
            GLTriangleCV[] triangles = new GLTriangleCV[coordinates.length/9];
            do {
                triangles[index/9] = new GLTriangleCV("", Arrays.copyOfRange(coordinates, index, index + 3), Arrays.copyOfRange(coordinates, index + 3, index + 6), Arrays.copyOfRange(coordinates, index + 6, index + 9));
                index += 9;
            } while (index<coordinates.length);
            chars2D_Triangles.put(key,triangles);
        }
        chars3D_TriangleLines = new HashMap<>();
        for (char key : chars3D_TriangleLinesCoords.keySet()) {
            float[] coordinates = chars3D_TriangleLinesCoords.get(key);
            int index = 0;
            GLLineCV[] lines = new GLLineCV[coordinates.length/6];
            do {
                lines[index/6] = new GLLineCV("", Arrays.copyOfRange(coordinates, index, index + 3), Arrays.copyOfRange(coordinates, index + 3, index + 6));
                index += 6;
            } while (index<coordinates.length);
            chars3D_TriangleLines.put(key,lines);
        }
        chars2D_TriangleLines = new HashMap<>();
        for (char key : chars2D_TriangleLinesCoords.keySet()) {
            float[] coordinates = chars2D_TriangleLinesCoords.get(key);
            int index = 0;
            GLLineCV[] lines = new GLLineCV[coordinates.length/6];
            do {
                lines[index/6] = new GLLineCV("", Arrays.copyOfRange(coordinates, index, index + 3), Arrays.copyOfRange(coordinates, index + 3, index + 6));
                index += 6;
            } while (index<coordinates.length);
            chars2D_TriangleLines.put(key,lines);
        }
        chars3D_FaceLines = new HashMap<>();
        for (char key : chars3D_FaceLinesCoords.keySet()) {
            float[] coordinates = chars3D_FaceLinesCoords.get(key);
            int index = 0;
            GLLineCV[] lines = new GLLineCV[coordinates.length/6];
            do {
                lines[index/6] = new GLLineCV("", Arrays.copyOfRange(coordinates, index, index + 3), Arrays.copyOfRange(coordinates, index + 3, index + 6));
                index += 6;
            } while (index<coordinates.length);
            chars3D_FaceLines.put(key,lines);
        }
        chars2D_FaceLines = new HashMap<>();
        for (char key : chars2D_FaceLinesCoords.keySet()) {
            float[] coordinates = chars2D_FaceLinesCoords.get(key);
            int index = 0;
            GLLineCV[] lines = new GLLineCV[coordinates.length/6];
            do {
                lines[index/6] = new GLLineCV("", Arrays.copyOfRange(coordinates, index, index + 3), Arrays.copyOfRange(coordinates, index + 3, index + 6));
                index += 6;
            } while (index<coordinates.length);
            chars2D_FaceLines.put(key,lines);
        }
        // end = new Date();
        // Log.v("GLDEMO","Duration initHashMaps() - create HashMap entries for triangles and lines: "+(end.getTime()-start.getTime()));
        hashMapsInitialized = true;
        return true;
    }

    /**
     * Creates a shape that represents the string consisting of multiple characters.
     * The origin of the shape (i.e. point (0,0,0) in its model space) will be set in the center of all its triangles and lines.
     * N.B. For 3D character shapes, this method is rather time-intensive and should therefore be used with care. The method makeStringShape() might be an alternative.
     * @param shapes The shapes with the characters to be combined
     * @return The shape for the string
     */

    /*
    public static GLShapeCV joinCharShapes(GLCharShapeCV[] shapes) {
        float currXPos = 0;
        GLShapeCV stringShape = shapes[0];
        float firstAdjust = GLTextUtilsCV.charShapeVertAdjustment(shapes[0]);
        for (int i=1;i<shapes.length;i++) {
            // long startTime = System.nanoTime();
            GLShapeCV shape = shapes[i].copy("");
            // Log.v("GLDEMO","  copy shape: "+(System.nanoTime()-startTime)/1000000.0+" ms");
            currXPos+=GLTextUtilsCV.horizSpacingCharShapes(shapes[i-1],shape,true);
            shape.moveZeroPointTo(-currXPos,-GLTextUtilsCV.charShapeVertAdjustment(shape)+firstAdjust,0);    // TIME-INTENSIVE (because of the setBuffers() call within this method which is redundant here)
            // Log.v("GLDEMO","    moveZeroPoint: "+(System.nanoTime()-startTime)/1000000.0+" ms");
            GLTriangleCV[] triangles = shape.getTriangles(false);
            // Log.v("GLDEMO","      getTriangles: "+(System.nanoTime()-startTime)/1000000.0+" ms");
            stringShape.addTriangles(triangles,false);   // TIME-INTENSIVE (SEE ABOVE)
            // Log.v("GLDEMO","        addTriangles: "+(System.nanoTime()-startTime)/1000000.0+" ms");
            GLLineCV[] lines = shape.getLines(false);
            // Log.v("GLDEMO","          getLines: "+(System.nanoTime()-startTime)/1000000.0+" ms");
            stringShape.addLines(lines,false);    // TIME-INTENSIVE (SEE ABOVE)
            // Log.v("GLDEMO","            addLines: "+(System.nanoTime()-startTime)/1000000.0+" ms");
        }
        stringShape.moveZeroPointTo(stringShape.getCenter());
        return stringShape;
    }
     */

    /**
     * Creates a shape that represents a string for a number.
     * For a detailed explanation, see the comments on the constructors of the class GLStringShapeCV.
     */

    public static GLStringShapeCV makeStringShapeForNumber(String id, double number, float[] faceColor, float[] lineColor, float lineWidth, int flags, Context context) {
        if ((long)number==number)
            return new GLStringShapeCV(id,new Long((long)(number)).toString(),faceColor,lineColor,lineWidth,flags,context);
        else
            return new GLStringShapeCV(id,new Double(number).toString(),faceColor,lineColor,lineWidth,flags,context);
    }

    /**
     * Creates a shape that represents a string for a number. The number is rounded such that a given maximum of decimal places is not exceeded
     * For a detailed explanation, see the comments on the constructors of the class GLStringShapeCV.
     */

    public static GLStringShapeCV makeStringShapeForNumber(String id, double number, int maxDecPlaces, float[] faceColor, float[] lineColor, float lineWidth, int flags, Context context) {
        double roundedNumber;
        if (maxDecPlaces>=0) {
            double factor = Math.pow(10,maxDecPlaces);
            roundedNumber = Math.round(number*factor)/factor;
        }
        else roundedNumber = number;
        return makeStringShapeForNumber(id,roundedNumber,faceColor,lineColor,lineWidth,flags,context);
    }

    /**
     * Sets the x positions of the shapes based on the values returned by the method horizDistanceCharShapes()
     * and the y positions based on the values returned by charShapeVertAdjustment().
     * @param shapes The character shapes to place.
     * @param xStart The x position of the first character shape.
     * @param yPos The basic y position of all character shapes, will be adjusted by the values returned by charShapeVertAdjustment().
     * @param zPos The z positions of the shapes.
     */

    public static void placeCharShapes(GLCharShapeCV[] shapes, float xStart, float yPos, float zPos) {
        if (shapes==null||shapes.length==0) return;
        float[][] positions = positionsCharShapes(shapes,xStart,yPos,zPos);
        for (int i=0;i<shapes.length;i++)
            shapes[i].setTrans(positions[i]);
    }

    /**
     * Calculates the positions of character shapes to form a string.
     * The calculation is based on the x values returned by the method horizDistanceCharShapes()
     * and the y values returned by charShapeVertAdjustment().
     * @param shapes The character shapes.
     * @param xStart The x position of the first character shape.
     * @param yPos The basic y position of all character shapes, will be adjusted by the values returned by charShapeVertAdjustment().
     * @param zPos The z positions of the shapes.
     */

    public static float[][] positionsCharShapes(GLCharShapeCV[] shapes, float xStart, float yPos, float zPos) {
        if (shapes==null||shapes.length==0) return null;
        float[][] result = new float[shapes.length][3];
        float currentXPos = xStart;
        result[0][0]=xStart;
        result[0][1]=yPos+GLTextUtilsCV.charShapeVertAdjustment(shapes[0]);
        result[0][2]=zPos;
        for (int i=1;i<shapes.length;i++) {
            currentXPos += GLTextUtilsCV.horizDistanceCharShapes(shapes[i-1],shapes[i], true);
            result[i][0]=currentXPos;
            result[i][1]=yPos+GLTextUtilsCV.charShapeVertAdjustment(shapes[i]);
            result[i][2]=zPos;
        }
        return result;
    }

    /**
     * Calculates the horizontal distance between two character shapes (i.e. the distance between their centers in the x dimension) when placing them to form a string.
     * The distance is calculated by adding half of the widths of both shapes,
     * adding a spacing value as defined by the constant 'defaultSpacing',
     * subtracting a kerning adjustment value if kerning is applied,
     * and multiplying the result by the scaling factor of the shapes (assuming that both shapes have the same scaling factor).
     * @param charShape1 The first character shape
     * @param charShape2 The second character shape
     * @param kerning Kerning to be applied
     * @return The calculated distance.
     */

    public static float horizDistanceCharShapes(GLCharShapeCV charShape1, GLCharShapeCV charShape2, boolean kerning) {
        return horizDistanceCharShapes(charShape1,charShape2,defaultSpacing,kerning);
    }

    /**
     * Calculates the horizontal distance between two character shapes (i.e. the distance between their centers in the x dimension) when placing them to form a string.
     * The distance is calculated by adding half of the widths of both shapes,
     * adding a spacing value passed as parameter,
     * subtracting a kerning adjustment value if kerning is applied,
     * and multiplying the result by the scaling factor of the shapes (assuming that both shapes have the same scaling factor).
     * @param charShape1 The first character shape
     * @param charShape2 The second character shape
     * @param spacing The spacing value (i.e. the width of the blank space between the shapes; for normal spacing, a value of 0.18 is recommended)
     * @param kerning Kerning to be applied
     * @return The calculated distance.
     */

    public static float horizDistanceCharShapes(GLCharShapeCV charShape1, GLCharShapeCV charShape2, float spacing, boolean kerning) {
        float scaleFactor = charShape1.getScaleX();
        char char1 = charShape1.getCharacter();
        char char2 = charShape2.getCharacter();
        // Log.v("GLDEMO","scale: "+scaleFactor);
        // Log.v("GLDEMO",char1+": width="+charShape1.getIntrinsicSize(0));
        String pair = char1 + "" + char2;
        if (kerning && kerningAdjustmentTable == null)
            initKerningAdjustmentTable();
        if (!kerning || !kerningAdjustmentTable.containsKey(pair))
            return (spacing + (charShape1.getIntrinsicSize(0) + charShape2.getIntrinsicSize(0)) / 2) * scaleFactor;
          else
            return (spacing - kerningAdjustmentTable.get(pair) + (charShape1.getIntrinsicSize(0) + charShape2.getIntrinsicSize(0)) / 2) * scaleFactor;
    }

    /**
     * The table supports kerning, i.e. placing specific pairs of character closer together than with normal spacing - see https://en.wikipedia.org/wiki/Kerning.
     * The values are applied when calculating the horizontal positions of characters in string
     * and have to be subtracted from the spacing value used in this calculation - see e.g. the method horizDistanceCharShapes().
     */

    protected static HashMap<String, Float> kerningAdjustmentTable = null;

    /**
     * Internal method for initializing the kerning adjustment table.
     */

    public static void initKerningAdjustmentTable() {
        kerningAdjustmentTable = new HashMap<>();
        kerningAdjustmentTable.put("AV", 0.1f);
        kerningAdjustmentTable.put("Av", 0.1f);
        kerningAdjustmentTable.put("VA", 0.1f);
        kerningAdjustmentTable.put("vA", 0.1f);
        kerningAdjustmentTable.put("AW", 0.15f);
        kerningAdjustmentTable.put("Aw", 0.05f);
        kerningAdjustmentTable.put("WA", 0.15f);
        kerningAdjustmentTable.put("wA", 0.05f);
        kerningAdjustmentTable.put("Ay", 0.05f);
        kerningAdjustmentTable.put("yA", 0.05f);
        kerningAdjustmentTable.put("LT", 0.2f);
        kerningAdjustmentTable.put("LV", 0.1f);
        kerningAdjustmentTable.put("Ly", 0.1f);
        kerningAdjustmentTable.put("Pa", 0.1f);
        kerningAdjustmentTable.put("Pä", 0.1f);
        kerningAdjustmentTable.put("Pe", 0.1f);
        kerningAdjustmentTable.put("Po", 0.1f);
        kerningAdjustmentTable.put("Pö", 0.1f);
        kerningAdjustmentTable.put("Ta", 0.1f);
        kerningAdjustmentTable.put("Te", 0.1f);
        kerningAdjustmentTable.put("To", 0.1f);
        kerningAdjustmentTable.put("Tr", 0.1f);
        kerningAdjustmentTable.put("Tu", 0.1f);
        kerningAdjustmentTable.put("Tw", 0.1f);
        kerningAdjustmentTable.put("aj", 0.15f);
        kerningAdjustmentTable.put("ew", 0.1f);
        kerningAdjustmentTable.put("ej", 0.2f);
        kerningAdjustmentTable.put("ev", 0.1f);
        kerningAdjustmentTable.put("ey", 0.1f);
        kerningAdjustmentTable.put("ye", 0.1f);
        kerningAdjustmentTable.put("oj", 0.1f);
        kerningAdjustmentTable.put("ov", 0.05f);
        kerningAdjustmentTable.put("ow", 0.05f);
        kerningAdjustmentTable.put("oy", 0.05f);
        kerningAdjustmentTable.put("re", 0.1f);
        kerningAdjustmentTable.put("ro", 0.1f);
        kerningAdjustmentTable.put("va", 0.1f);
        kerningAdjustmentTable.put("ve", 0.1f);
        kerningAdjustmentTable.put("vo", 0.1f);
        kerningAdjustmentTable.put("wa", 0.1f);
        kerningAdjustmentTable.put("we", 0.1f);
        kerningAdjustmentTable.put("wo", 0.1f);
        // more kerning pairs:
        // kt oz ru rz ub ye
        // Va V. Ya Yo Y. fa fe f, f.
        // potentially also: Fa Fe Fi Fo Fr Fu v, v. w, w.
        // TODO a somewhat larger spacing for pairs of characters with long perpendicular lines at their sides - e.g. IL, lh etc.
    }

    /**
     * Calculates the vertical adjustment (i.e. the value to be added in the y dimension)
     * to align a character shape when placing it to form a string with other character shapes.
     * @param charShape The character shape for which the adjustment shall be calculated
     * @return The calculated adjustment
     */

    public static float charShapeVertAdjustment(GLCharShapeCV charShape) {
        float scaleFactor = charShape.getScaleY();
        char zeichen = charShape.getCharacter();
        if (zeichen == 'g' || zeichen == 'j' || zeichen == 'p' || zeichen == 'q' || zeichen == 'y')
            return (charShape.getIntrinsicSize(1) / 4 - .06f) * scaleFactor;
        else if (zeichen == 'Q' || zeichen == '$')
            return (charShape.getIntrinsicSize(1) / 4 + .14f) * scaleFactor;
        else if (zeichen == '\"')
            return (charShape.getIntrinsicSize(1) / 4 + .78f) * scaleFactor;
        if (zeichen == '*')
            return (charShape.getIntrinsicSize(1) / 4 + .2f) * scaleFactor;
        if (zeichen == '-')
            return (charShape.getIntrinsicSize(1) / 4 + .305f) * scaleFactor;
        if (zeichen == '=')
            return (charShape.getIntrinsicSize(1) / 4 + .25f) * scaleFactor;
        if (zeichen == ',')
            return (charShape.getIntrinsicSize(1) / 4 - 0.2f) * scaleFactor;
        if (zeichen == ';')
            return (charShape.getIntrinsicSize(1) / 4 - .06f) * scaleFactor;
        else
            return (charShape.getIntrinsicSize(1) / 2) * scaleFactor;
    }

    /**
     * Method to save the HashMaps xxxCoords.
     * To be used when the collection of OBJ files has been modified and hence new coordinates must be saved.
     * Note that the new coordinate files will be stored in the internal file directory of the application
     * and must be transferred by hand to the assets directory
     * @param context
     */

    public static void saveCoordinatesHashMaps(Context context) {
        try {
            FileOutputStream fileOut = context.openFileOutput("Chars3D_TriangleCoords", 0);
            ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
            objOut.writeObject(chars3D_TriangleCoords);
            fileOut.close();
            objOut.close();
            fileOut = context.openFileOutput("Chars2D_TriangleCoords", 0);
            objOut = new ObjectOutputStream(fileOut);
            objOut.writeObject(chars2D_TriangleCoords);
            fileOut.close();
            objOut.close();
            fileOut = context.openFileOutput("Chars3D_TriangleLinesCoords", 0);
            objOut = new ObjectOutputStream(fileOut);
            objOut.writeObject(chars3D_TriangleLinesCoords);
            fileOut.close();
            objOut.close();
            fileOut = context.openFileOutput("Chars2D_TriangleLinesCoords", 0);
            objOut = new ObjectOutputStream(fileOut);
            objOut.writeObject(chars2D_TriangleLinesCoords);
            fileOut.close();
            objOut.close();
            fileOut = context.openFileOutput("Chars3D_FaceLinesCoords", 0);
            objOut = new ObjectOutputStream(fileOut);
            objOut.writeObject(chars3D_FaceLinesCoords);
            fileOut.close();
            objOut.close();
            fileOut = context.openFileOutput("Chars2D_FaceLinesCoords", 0);
            objOut = new ObjectOutputStream(fileOut);
            objOut.writeObject(chars2D_FaceLinesCoords);
            fileOut.close();
            objOut.close();
        } catch(Exception exc) {
            Log.v("GLDEMO","Exception: "+exc.toString());
        }
    }

    /**
     * Makes character shapes for the characters of a string. If a character is not supported a shape for an underscore is created.
     * For details and especially a detailed explanation of the parameters, see the constructor of class GLCharShapeCV().
     * @return The created shapes
     */

    public static GLCharShapeCV[] makeCharShapes(String string, float[] faceColor, float[] lineColor, float lineWidth, int flags, Context context) {
        if (string==null) return null;
        GLCharShapeCV[] result = new GLCharShapeCV[string.length()];
        for (int i=0; i<string.length();i++)
            result[i] = new GLCharShapeCV("",string.charAt(i),faceColor,lineColor,lineWidth,flags,context);
        return result;
    }

    /**
     * Makes 3D character shapes for the digits of a value with and without decimal places
     * For details and especially a detailed explanation of the parameters, see the constructor of class GLCharShapeCV().
     * @return The created shapes
     */

    public static GLCharShapeCV[] makeCharShapesForNumber(double value, float[] faceColor, float[] lineColor, float lineWidth, int flags, Context context) {
        String numberString;
        if ((int)value==value)
            numberString = new Integer((int)value).toString();
        else
            numberString = new Double(value).toString();
        return makeCharShapes(numberString,faceColor,lineColor,lineWidth,flags,context);

    }


    /*
     The following methods have been written by Ahmed Ez Aldeen Bobaki for his bachelor thesis
     but are currently not used here.
     */

    /**
     * Diese Methode konvertiert den eingegebenen Text in ein Array von GLShapeCV-Objekten.
     * @param text Der eingegebene Text.
     *
    private static GLShapeCV[] erzeugeSchriftzeichen(String text, Activity context, boolean threeD) {
        String eingabeOhneLeerzeichen = leerzeichenEntfernen(text);
        int len = eingabeOhneLeerzeichen.length();
        GLShapeCV[] schriftzeichen = new GLShapeCV[len];
        for (int i = 0; i < len; i++) {
            char zeichen = eingabeOhneLeerzeichen.charAt(i);
            if (threeD)
                schriftzeichen[i] = GLShapeFactoryCV.makeCharacter3D("",zeichen,GraphicsUtilsCV.white,context);
              else
                schriftzeichen[i] = GLShapeFactoryCV.makeCharacter2D("",zeichen,GraphicsUtilsCV.white,context);
        }
        return schriftzeichen;
    }

    /**
     * Diese Methode ist für die Darstellung und Anpassung als TextEditor von schriftzeichen zuständig.
     * @param text eingegebene Text
     * @return Die 3D-Schriftzeichen-Modellen als Array von GLShapeCV-Objekte
     *
    public static GLShapeCV[] textDarstellen(String text, Activity context, boolean threeD) {
        return textDarstellen(text,context,threeD,false);
    }

    public static GLShapeCV[] textDarstellen(String text, Activity context, boolean threeD, boolean kerning) {
        GLShapeCV[] glShapeCVS = erzeugeSchriftzeichen(text,context, threeD);
        if (kerningTable==null)
            initKerningTable();
//        textDarstellen(glShapeCVS, text, 6.5f, 0,context,kerning);
        textDarstellen(glShapeCVS, text, 0.290545f, 0,context,kerning);
        return glShapeCVS;
    }

    /**
     * Diese Methode ist für die Darstellung und Anpassung als TextEditor von schriftzeichen zuständig.
     * @param text eingegebene Text
     * @param skalierungsfaktor  Die Ziel-Skalierungsfaktor der Animation.
     * @param abstandsanpassung  Abstand zwischen Wörtern und Zeichen
     * @return Die 3D-Schriftzeichen-Modellen als Array von GLShapeCV-Objekte
     *
    public static GLShapeCV[] textDarstellen(String text, float skalierungsfaktor, float abstandsanpassung, Activity context, boolean threeD, boolean kerning) {
        GLShapeCV[] glShapeCVS = erzeugeSchriftzeichen(text,context,threeD);
        textDarstellen(glShapeCVS, text, skalierungsfaktor, abstandsanpassung, context,kerning);
        return glShapeCVS;
    }

    /**
     * Diese Methode setzt die entsprechene Skalierung, Animation usw. für jedes Zeichen und passt diesen Text als TextEditor von schriftzeichen an.
     * @param glshapeCVs        Die 3D-Schriftzeichen-Modellen als Array von GLShapeCV-Objekte
     * @param text              eingegebene Text
     * @param skalierungsfaktor Die Ziel-Skalierungsfaktor der Animation.
     * @param abstandsanpassung Abstand zwischen Wörtern und Zeichen
     *
    public static void textDarstellen(GLShapeCV[] glshapeCVs, String text, float skalierungsfaktor, float abstandsanpassung, Activity context, boolean kerning) {

        //Grenze der Zeile von Rechts
        float zeileBegrenzungVonRechts;
        //Position eines Zeichens
        float zeichenposition = -2.6f;
        //wortabstand für den Abstand zwischen Wörtern
        float wortabstand = (skalierungsfaktor * 0.02f) + abstandsanpassung;
        //zeichenabstand für den Abstand zwischen Zeichen
        float zeichenabstand = (skalierungsfaktor * 0.2f) + abstandsanpassung;
        //zeilenabstand
        float zeilenabstand = (skalierungsfaktor * 0.059f);

        int anzahlZeilen = 0;
        int anzahlLeerzeichen = 0;
        boolean neueZeileErforderlich = false;
        List<Integer> leerzeichenIndex = letzteZeichenIndices(text);

        for (int i = 0; i < glshapeCVs.length; i++) {
            int j = i;
            //Hier wird überprüft, ob das Zeichen nach der Leerzeichen befindet.
            if (leerzeichenIndex.get(anzahlLeerzeichen) < i) {
                zeichenposition += wortabstand;
                zeileBegrenzungVonRechts = zeichenposition;
                anzahlLeerzeichen++;

                //Die Länge des nächsten Worts wird komplett berechnet und dann wird zu diesen variable
                // zeileBegrenzungVonRechts hinzufügt.
                while (leerzeichenIndex.get(anzahlLeerzeichen) >= j && j != 0) {
                    zeileBegrenzungVonRechts += zeichenabstand + ((glshapeCVs[j-1].getIntrinsicSize(0) * skalierungsfaktor + glshapeCVs[j].getIntrinsicSize(0) * skalierungsfaktor) / 2);
                    j++;
                }

                //Hier wird überprüft, ob das Wort die rechte Grenze überschritten hat
                //wenn ja, wird dann ein neue Zeile hinzufügt und die zeichenPosition zurückgesetzt
                if (zeileBegrenzungVonRechts > 2.6) {
                    zeichenposition = -2.6f;
                    anzahlZeilen++;
                    neueZeileErforderlich = true;
                }
            }

            if (i != 0 && !neueZeileErforderlich) {
                String pair = text.charAt(i-1)+""+text.charAt(i);
                if (kerning&&kerningTable.containsKey(pair)) {
                    Log.v("GLDEMO","Kerning alt: "+pair+"  "+(zeichenabstand + kerningTable.get(pair)));
                    zeichenposition += zeichenabstand + kerningTable.get(pair);
                }
                  else
                    zeichenposition += zeichenabstand + (glshapeCVs[i-1].getIntrinsicSize(0) * skalierungsfaktor + glshapeCVs[i].getIntrinsicSize(0) * skalierungsfaktor) / 2;


               // zeichenposition += charShapesHorizDistance(glshapeCVs[i-1],glshapeCVs[i],kerning);
            } else
                neueZeileErforderlich = false;

            glshapeCVs[i].setTransX(zeichenposition);
            glshapeCVs[i].setTransY(charShapeRelativeYPos(glshapeCVs[i]) * skalierungsfaktor - anzahlZeilen * zeilenabstand);
            glshapeCVs[i].setScale(skalierungsfaktor);

        }
    }

    /**
     * Diese Methode ist dafür verantwortlich, dass Zeichen vertauschen, wenn der Benutzer die Berührung in eine bestimmte Richtung verwendet
     * @param glShapeCVs              Die 3D-Schriftzeichen-Modellen als Array von GLShapeCV-Objekte
     * @param text                    eingegebene Text
     * @param beruehrteSchriftzeichen Schriftzeichen, die vom Benutzer berührt wurde
     * @param richtung                Richtung, die vom Benutzer augewählt wurde
     * @return der Text wird als String zurückgegeben, nachdem die Zeichen vertauscht werden.
     *
    public static String tauscheSchriftzeichen(GLShapeCV[] glShapeCVs, String text, GLShapeCV beruehrteSchriftzeichen, String richtung) {
        int indexBeruhrteZeichen = GLTextUtilsCV.indexSchriftzeichen(glShapeCVs, beruehrteSchriftzeichen);
        int indexGezielteZeichen;

        if (Objects.equals(richtung, "left") && (indexBeruhrteZeichen == 0)) {
            indexGezielteZeichen = glShapeCVs.length - 1;
        } else if (Objects.equals(richtung, "right") && (glShapeCVs.length - 1) == indexBeruhrteZeichen) {
            indexGezielteZeichen = 0;
        } else if (Objects.equals(richtung, "right")) {
            indexGezielteZeichen = indexBeruhrteZeichen + 1;
        } else if (Objects.equals(richtung, "left")) {
            indexGezielteZeichen = indexBeruhrteZeichen - 1;
        } else return null;

        text = text.trim();
        List<Integer> whitespaceIndices = GLTextUtilsCV.indizesLeerzeichen(text);
        text = GLTextUtilsCV.leerzeichenEntfernen(text);
        char[] chars = text.toCharArray();

        char tmp1 = chars[indexBeruhrteZeichen];
        chars[indexBeruhrteZeichen] = chars[indexGezielteZeichen];
        chars[indexGezielteZeichen] = tmp1;

        float tmp2 = hoehe[indexBeruhrteZeichen];
        hoehe[indexBeruhrteZeichen] = hoehe[indexGezielteZeichen];
        hoehe[indexGezielteZeichen] = tmp2;

        float tmp3 = glShapeCVs[indexBeruhrteZeichen].getIntrinsicSize(0);

        GLShapeCV tmp4 = glShapeCVs[indexBeruhrteZeichen];
        glShapeCVs[indexBeruhrteZeichen] = glShapeCVs[indexGezielteZeichen];
        glShapeCVs[indexGezielteZeichen] = tmp4;

        text = new String(chars);
        StringBuilder sb = new StringBuilder(text);
        for (int i = 0; i < whitespaceIndices.size(); i++) {
            int index = whitespaceIndices.get(i);
            sb.insert(index, " ");
        }
        text = sb.toString();
        return text;
    }

    /**
     * Die Methode getRichtung erhält zwei Punkte als Eingabe: den ersten Punkt, der berührt wird,
     * und den letzten Punkt. Anschließend wird der Winkel zwischen diesen beiden Punkten ermittelt.
     * Basierend auf diesem Winkel wird die entsprechende Richtung bestimmt.
     *
    public static String getRichtung(float x1, float y1, float x2, float y2) {
        //Weitere Informationen über (https://stackoverflow.com/a/26387629/11434585)
        double rad = Math.atan2(y1 - y2, x2 - x1) + Math.PI;
        double winkel = (rad * 180 / Math.PI + 180) % 360;
        if (winkel >= 45 && winkel < 135) {
            return "up";
        } else if (winkel >= 0 && winkel < 45 || winkel >= 315 && winkel < 360) {
            return "right";
        } else if (winkel >= 225 && winkel < 315) {
            return "down";
        } else {
            return "left";
        }
    }

    /**
     * ändert die Farbe von Schriftzeichen mithilfe von setTrianglesUniformColor-Methode in GLShapeCV-Klasse
     * @param glShapeCVS  Die 3D-Schriftzeichen-Modellen als Array von GLShapeCV-Objekte
     * @param colorString die ausgewählte Farbe
     *
    public static void farbeAendern(GLShapeCV[] glShapeCVS, String colorString) {
        for (GLShapeCV glShapeCV : glShapeCVS) {
            glShapeCV.setTrianglesUniformColor(getFarbe(colorString));
        }
    }

    /**
     * entfernt ein beruehrteSchriftzeichen aus einem Array von GLShapeCV-Objekten und gibt das aktualisierte Array zurück.
     * @param glShapeCVS              glShapeCVs Die 3D-Schriftzeichen-Modellen als Array von GLShapeCV-Objekte
     * @param beruehrteSchriftzeichen Schriftzeichen, die vom Benutzer berührt wurde
     * @return aktualisierte Array
     *
    public static GLShapeCV[] schriftzeichenEntfernen(GLShapeCV[] glShapeCVS, GLShapeCV beruehrteSchriftzeichen) {
        int index = indexSchriftzeichen(glShapeCVS, beruehrteSchriftzeichen);
        if (index == -1) {
            return glShapeCVS;
        } else {
            GLShapeCV[] newShapes = new GLShapeCV[glShapeCVS.length - 1];
            int destIndex = 0;
            for (int srcIndex = 0; srcIndex < glShapeCVS.length; srcIndex++) {
                if (srcIndex != index) {
                    newShapes[destIndex++] = glShapeCVS[srcIndex];
                }
            }
            return newShapes;
        }
    }

    /**
     * gibt den Index des einzigeShape im Array zurück
     * @param glShapeCVS   Array von GLShapeCV
     * @param einzigeShape ausgewählte GLShapeCV
     * @return Index des ausgewählte GLShapeCV
     *
    public static int indexSchriftzeichen(GLShapeCV[] glShapeCVS, GLShapeCV einzigeShape) {
        for (int i = 0; i < glShapeCVS.length; i++) {
            if (glShapeCVS[i].equals(einzigeShape)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Diese Methode gibt die Indizes der Leerzeichen im Eingabestring zurück
     * Z.b ("ABC DEF GHI J"): -> = [3, 7, 11]
     *
    private static ArrayList<Integer> indizesLeerzeichen(String text) {
        // https://stackoverflow.com/a/4731081
        ArrayList<Integer> indizes = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\S\\s"); // match any non-whitespace character followed by whitespace
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            indizes.add(matcher.end() - 1); // add the index of the whitespace character
        }
        return indizes;
    }

    /**
     * Diese Methode gibt die Indices des letzten Zeichens für jedes zusammenhängende Zeichenfolge zurück, ohne die Indices von Leerzeichen zu berücksichtigen.
     * Z.b ("ABC DEF % GHI"): -> = [2, 5, 6, 8]
     *
    public static List<Integer> letzteZeichenIndices(String text) {
        List<Integer> charCounts = new ArrayList<>();
        int charCount = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                charCounts.add(i - charCount);
                charCount = i + 1;
            }
        }
        charCounts.add(text.length() - charCount);
        for (int i = 1; i < charCounts.size(); i++) {
            charCounts.set(i, charCounts.get(i) + charCounts.get(i - 1));
        }
        //make every Value -1
        for (int i = 0; i < charCounts.size(); i++) {
            int newValue = charCounts.get(i) - 1;
            charCounts.set(i, newValue);
        }
        return charCounts;
    }

    public static String getLinkesWort(String eingabe) {
        int kommaIndex = eingabe.indexOf(',');
        return eingabe.substring(0, kommaIndex);
    }

    public static String charAt(String input, int index) {
        if (index >= 0 && index < input.length()) {
            return input.substring(index, index + 1);
        }
        return "Index out of bounds";
    }

    private static float[] getFarbe(String farbeAlsString) {
        switch (farbeAlsString) {
            case "Weiß":
                return GraphicsUtilsCV.white;
            case "Schwarz":
                return GraphicsUtilsCV.black;
            case "Gelb":
                return GraphicsUtilsCV.yellow;
            case "Hellgelb":
                return GraphicsUtilsCV.lightyellow;
            case "Orange":
                return GraphicsUtilsCV.orange;
            case "Rot":
                return GraphicsUtilsCV.red;
            case "Hellrot":
                return GraphicsUtilsCV.lightred;
            case "Blau":
                return GraphicsUtilsCV.blue;
            case "Hellblau":
                return GraphicsUtilsCV.lightblue;
            case "Grün":
                return GraphicsUtilsCV.green;
            case "Hellgrün":
                return GraphicsUtilsCV.lightgreen;
            case "Dunkelgrün":
                return GraphicsUtilsCV.darkgreen;
            case "Türkis":
                return GraphicsUtilsCV.cyan;
            case "Magenta":
                return GraphicsUtilsCV.magenta;
            case "Lila":
                return GraphicsUtilsCV.purple;
            default:
                return new float[0];
        }
    }

    private static String leerzeichenEntfernen(String str) {
        return str.replaceAll("\\s+", "");
    }

    public static String zeichenEntfernenAnIndexOhneLeerzeichen(String str, int index) {
        int nonWhitespaceCharCount = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!Character.isWhitespace(c)) {
                nonWhitespaceCharCount++;
            }
            if (nonWhitespaceCharCount == index) {
                if (Character.isWhitespace(c)) {
                    sb.append(c);
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString().replaceAll("\\s{2}", " ");
    }

     */

}
