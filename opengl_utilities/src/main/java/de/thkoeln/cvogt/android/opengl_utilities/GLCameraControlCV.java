// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 30.11.2023

package de.thkoeln.cvogt.android.opengl_utilities;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.app.Activity;
import android.content.Context;
import android.opengl.Matrix;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import java.util.ArrayList;

/**
 * Class with
 * <UL>
 * <LI>static convenience methods to control the eye/camera, i.e. the eye/camera position and focus of a renderer and
 * <LI>a touch listener class that allows to control the eye/camera by touches and gestures.
 * </UL>
 * @see GLRendererCV
 */

public class GLCameraControlCV {

    private GLCameraControlCV() {
        throw new IllegalStateException("Class not instantiable");
    }

    /**
     * Method to calculate the z coordinate of a recommended eye/camera position to fully see a given shape.
     * It assumes that the eye/camera will be placed on the z axis and look into the negative z direction,
     * i.e. the x and y coordinates of the eye position and of the eye focus will be set to 0.
     * The method depends on
     * <UL>
     * <LI>the position and the scaling factor of the shape (but not on its rotation) and
     * <LI>the position and the size of the near plane of the frustum.
     * </UL>
     * The returned value will be not smaller than 1.
     * <P>
     * N.B.: The value of the viewportWidth parameter (see parameter list) can be obtained from the underlying surface view by calling its getWidth() and getHeight() methods.
     * These methods, however, will return meaningful values only after the surface view has been rendered.
     * Its is therefore recommend to place a call of this method into a Runnable object
     * and let this Runnable be executed through the surface view's post() method:
     * surfaceView.post(new Runnable() { public void run() { ... method call ... } });
     *
     * @param shapeToSee The shape to be seen
     * @param frustumNear The distance of the near plane of the frustum to the camera (usually GLRendererCV.frustumNear)
     * @param viewportWidth The width of the view port / the near plane of the frustum (usually the quotient width/height of the displaying surface view)
     * @return The recommended z coordinate of the eye/camera position
     */

    public static float eyeZPosToSeeShape(GLShapeCV shapeToSee, float frustumNear, float viewportWidth) {
        float shapeLimitX = Math.abs(shapeToSee.getTransX())+shapeToSee.getIntrinsicSize(0)*shapeToSee.getScaleX();
        float shapeLimitY = Math.abs(shapeToSee.getTransY())+shapeToSee.getIntrinsicSize(1)*shapeToSee.getScaleY();
        float shapeMinZ = shapeToSee.getTransZ()+shapeToSee.getIntrinsicSize(2)*shapeToSee.getScaleZ();
        // float posToSeeX = shapeMinZ+Math.abs(shapeToSee.getTransX())*frustumNear/viewportWidth;
        // float posToSeeY = shapeMinZ+Math.abs(shapeToSee.getTransY())*frustumNear;
        float posToSeeX = shapeMinZ+shapeLimitX*frustumNear/viewportWidth;  // calculation based on the intercept theorem ("Strahlensatz")
        float posToSeeY = shapeMinZ+shapeLimitY*frustumNear;  // calculation based on the intercept theorem ("Strahlensatz")
        float result;
        if (posToSeeX>posToSeeY) result=posToSeeX;
        else result=posToSeeY;
        if (result-GLRendererCV.frustumNear<shapeMinZ)
            result = shapeMinZ+frustumNear;
        if (result<1) result = 1;
        return result;
    }

    /**
     * Method to calculate the z coordinate of a recommended eye/camera position to fully see a given collection of shapes.
     * It assumes that the eye/camera will be placed on the z axis and look into the negative z direction,
     * i.e. the x and y coordinates of the eye position and of the eye focus will be set to 0.
     * The method depends on
     * <UL>
     * <LI>the positions and the scaling factors of the shapes (but not on their rotation) and
     * <LI>the position and the size of the near plane of the frustum.
     * </UL>
     * The returned value will be not smaller than 1.
     * <P>
     * N.B.: The value of the viewportWidth parameter (see parameter list) can be obtained from the underlying surface view by calling its getWidth() and getHeight() methods.
     * These methods, however, will return meaningful values only after the surface view has been rendered.
     * Its is therefore recommend to place a call of this method into a Runnable object
     * and let this Runnable be executed through the surface view's post() method:
     * surfaceView.post(new Runnable() { public void run() { ... method call ... } });
     *
     * @param shapesToSee The shapes to be seen
     * @param frustumNear The distance of the near plane of the frustum to the camera (usually GLRendererCV.frustumNear)
     * @param viewPortWidth The width of the view port / the near plane of the frustum (usually width/height of the displaying surface view)
     * @return The recommended z coordinate of the eye/camera position
     */

    public static float eyeZPosToSeeShapes(ArrayList<GLShapeCV> shapesToSee, float frustumNear, float viewPortWidth) {
        float eyeZPos = 1;
        for (GLShapeCV shape : shapesToSee) {
            float tmp = GLCameraControlCV.eyeZPosToSeeShape(shape,frustumNear,viewPortWidth);
            if (tmp>eyeZPos) eyeZPos=tmp;
        }
        return eyeZPos;
    }

    /**
     * Method to calculate the coordinates of a recommended eye/camera position to fully see a given rectangle.
     * Hence, all shapes that lie within this rectangle will be visible from this position.
     * <P>
     * The method assumes that
     * <UL>
     * <LI>the rectangle lies in a plane parallel to the x-y plane, i.e. that its normal (= perpendicular vector) is parallel to the z axis and
     * <LI>the eye/camera will look straightly into the negative z direction, i.e. the focus will be set such that the (x,y) coordinates of the eye position and of the eye focus will be equal.
     * </UL>
     * The method depends on
     * <UL>
     * <LI>the borders of the rectangle,
     * <LI>the z coordinate of the rectangle, and
     * <LI>the position and the size of the near plane of the frustum.
     * </UL>
     * N.B.: The value of the viewportWidth parameter (see parameter list) can be obtained from the underlying surface view by calling its getWidth() and getHeight() methods.
     * These methods, however, will return meaningful values only after the surface view has been rendered.
     * Its is therefore recommend to place a call of this method into a Runnable object
     * and let this Runnable be executed through the surface view's post() method:
     * surfaceView.post(new Runnable() { public void run() { ... method call ... } });
     *
     * @param left The left border (= left x coordinate) of the rectangle
     * @param right The right border (= right x coordinate) of the rectangle
     * @param top The left border (= left x coordinate) of the rectangle
     * @param bottom The left border (= left x coordinate) of the rectangle
     * @param frustumNear The distance of the near plane of the frustum to the camera (usually GLRendererCV.frustumNear)
     * @param viewportWidth The width of the view port / the near plane of the frustum (usually width/height of the displaying surface view)
     * @return The recommended eye/camera position (or null if left>=right or top<=bottom or viewPortWidth<=0)
     */

    public static float[] eyePosToSeeRectangle(float left, float right, float top, float bottom, float zPos, float frustumNear, float viewportWidth) {
        if (left>=right||top<=bottom||viewportWidth<=0) return null;
        float[] result = new float[] { (left+right)/2, (top+bottom)/2, 0 };
        float posToSeeX = zPos+(right-result[0])*frustumNear/viewportWidth;  // calculation based on the intercept theorem ("Strahlensatz")
        float posToSeeY = zPos+(top-result[1])*frustumNear;  // calculation based on the intercept theorem ("Strahlensatz")
        if (posToSeeX>posToSeeY) result[2]=posToSeeX;
           else result[2]=posToSeeY;
        Log.v("GLDEMO",result[0]+" "+result[1]+" "+result[2]);
        return result;
    }

    /**
     * Makes an animator to let the eye/camera move on a direct line to a specific position.
     * The focus point of the eye/camera (i.e. the eyeFocus attribute of the renderer) will remain unchanged.
     * @param renderer The renderer whose eye/camera shall be moved.
     * @param targetX The target x position of the animation.
     * @param targetY The target y position of the animation.
     * @param targetZ The target z position of the animation.
     * @param duration The duration of the animation (ms).
     * @return The newly generated animator.
     */

    public static ObjectAnimator makeAnimTransLinearTo(GLRendererCV renderer, float targetX, float targetY, float targetZ, int duration) {
        return makeAnimTransLinearTo(renderer,targetX,targetY,targetZ, GLRendererCV.CONSTANT_FOCUS_POINT,duration);
    }

    /**
     * Makes an animator to let the eye/camera move on a direct line to a specific position.
     * The focus point of the eye/camera (i.e. the eyeFocus attribute of the renderer) may
     * <UL>
     * <LI>remain unchanged,
     * <LI>be adapted such that the direction of the eye/camera focus vector will always be the same or
     * <LI>be adapted such that the eye/camera focus vector always points into the current movement direction.
     * </UL>
     * @param renderer The renderer whose eye/camera shall be moved.
     * @param targetX The target x position of the animation.
     * @param targetY The target y position of the animation.
     * @param targetZ The target z position of the animation.
     * @param focusControl Specifies if and how the focus point shall be adapted, according to the constants defined in class GLRendererCV.
     * @param duration The duration of the animation (ms).
     * @return The newly generated animator.
     * @see GLRendererCV
     */

    public static ObjectAnimator makeAnimTransLinearTo(GLRendererCV renderer, float targetX, float targetY, float targetZ, int focusControl, int duration) {
        EvaluatorTransLinearTo eval = new EvaluatorTransLinearTo(renderer,new float[]{targetX,targetY,targetZ},focusControl);
        float[] dummy = new float[1];
        ObjectAnimator animator = ObjectAnimator.ofObject(renderer,"eyePosAndFocus",eval,dummy,dummy);
        animator.setDuration(duration);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    /**
     * Evaluator for a linear eye/camera movement with potential adaptation of the eye/camera focus.
     * The evaluator returns a float array of length 6 with the new eye/camera position in its components 0-2
     * and the (potentially) new eye/camera focus point in its components 3-5.
     * The array can be used as a parameter for the setEyePosAndFocus() method of a GLRendererCV object.
     */

    private static class EvaluatorTransLinearTo implements TypeEvaluator<float[]> {

        private GLRendererCV renderer;   // the renderer to which this evaluator belongs

        private float[] start;  // start position of the eye/camera

        private float[] target;  // target position of the eye/camera

        private float[] movementVector;  // vector from the start to the target position

        private float[] focusVector;  // vector specifying the constant viewing direction of the eye/camera (only valid if focusControl==CONSTANT_FOCUS_VECTOR)

        private int focusControl;  // treatment of the eye/camera focus (according to the constants defined in GLRendererCV)

        private float retval[];   // array containing the return values of the evaluator (eyePosX/Y/Z in positions 0-2; eyeFocusX/Y/Z in positions 3-5)

        private boolean attributesAreValid;  // Some attribute values are stored when the animation starts. 'attributesAreValid' specifies if this has been done already.

        EvaluatorTransLinearTo(GLRendererCV renderer, float[] target, int focusControl) {
            this.renderer = renderer;
            this.target = target.clone();
            this.focusControl = focusControl;
            retval = new float[6];
            this.attributesAreValid = false;
        }

        public float[] evaluate(float f, float dummy1[], float[] dummy2) {
            if (!attributesAreValid) {
                start = renderer.getEyePos();
                movementVector = new float[3];
                for (int i=0; i<3; i++)
                    movementVector[i] = target[i] - start[i];
                if (focusControl==GLRendererCV.CONSTANT_FOCUS_POINT) {
                    retval[3] = renderer.getEyeFocusX();
                    retval[4] = renderer.getEyeFocusY();
                    retval[5] = renderer.getEyeFocusZ();
                }
                if (focusControl==GLRendererCV.CONSTANT_FOCUS_VECTOR) {
                    focusVector = new float[3];
                    focusVector[0] = renderer.getEyeFocusX()-renderer.getEyePosX();
                    focusVector[1] = renderer.getEyeFocusY()-renderer.getEyePosY();
                    focusVector[2] = renderer.getEyeFocusZ()-renderer.getEyePosZ();
                }
                attributesAreValid = true;
            }
            for (int i=0;i<3;i++)
                retval[i] = start[i]+f*movementVector[i];
            switch (focusControl) {
                case GLRendererCV.CONSTANT_FOCUS_POINT: break;
                case GLRendererCV.CONSTANT_FOCUS_VECTOR:
                    retval[3] = renderer.getEyePosX()+focusVector[0];
                    retval[4] = renderer.getEyePosY()+focusVector[1];
                    retval[5] = renderer.getEyePosZ()+focusVector[2];
                    break;
                case GLRendererCV.FOCUS_INTO_MOVEMENT_DIRECTION:
                    retval[3] = 2*retval[0]-renderer.getEyePosX(); // new focus point = retval[0] + (retval[0]-eyePosX) = new position plus current movement direction
                    retval[4] = 2*retval[1]-renderer.getEyePosY();
                    retval[5] = 2*retval[2]-renderer.getEyePosZ();
                    break;
            }
            return retval;
        }

    }

    /**
     * Makes an animator to let the eye/camera move on an arc around an axis. The axis is specified by two points in 3D space.
     * The focus point of the eye/camera (i.e. the eyeFocus attribute of the renderer) may
     * <UL>
     * <LI>remain unchanged,
     * <LI>be adapted such that the direction of the eye/camera focus vector will always be the same or
     * <LI>be adapted such that the eye/camera focus vector always points into the current movement direction.
     * </UL>
     * N.B. Movements around the x axis that imply an upside-down turn of the eye/camera do not work properly.
     * @param renderer The renderer whose eye/camera shall be moved.
     * @param axisPoint1 The first point defining the axis.
     * @param axisPoint2 The second point defining the axis.
     * @param angle The angle by which the eye/camera shall be moved.
     * @param focusControl Specifies if and how the focus point shall be adapted, according to the constants defined in class GLRendererCV.
     * @param duration The duration of the animation (in ms).
     * @return The new animator
     * @see GLRendererCV
     */

    public static ObjectAnimator makeAnimTransArc(GLRendererCV renderer, float[] axisPoint1, float[] axisPoint2, float angle, int focusControl, int duration) {
        return makeAnimTransSpiral(renderer,axisPoint1,axisPoint2,angle/360,0,focusControl,duration);
    }

    /**
     * Makes an animator to let the eye/camera move on a spiral around an axis. The axis is specified by two points in 3D space.
     * The focus point of the eye/camera (i.e. the eyeFocus attribute of the renderer) may
     * <UL>
     * <LI>remain unchanged,
     * <LI>be adapted such that the direction of the eye/camera focus vector will always be the same or
     * <LI>be adapted such that the eye/camera focus vector always points into the current movement direction.
     * </UL>
     * N.B. Movements around the x axis that imply an upside-down turn of the eye/camera do not work properly.
     * @param renderer The renderer whose eye/camera shall be moved.
     * @param axisPoint1 The first point defining the axis.
     * @param axisPoint2 The second point defining the axis.
     * @param turns The number of turns to be made around the axis. If negative, the rotation is clockwise.
     * @param height The height of the spiral.
     * @param focusControl Specifies if and how the focus point shall be adapted, according to the constants defined in class GLRendererCV.
     * @param duration The duration of the animation (in ms).
     * @return The new animator
     * @see GLRendererCV
     */

    public static ObjectAnimator makeAnimTransSpiral(GLRendererCV renderer, float[] axisPoint1, float[] axisPoint2, float turns, float height, int focusControl, int duration) {
        EvaluatorTransSpiral eval = new EvaluatorTransSpiral(renderer,axisPoint1,axisPoint2,turns,height,focusControl);
        float[] dummy = new float[1];
        ObjectAnimator animator = ObjectAnimator.ofObject(renderer,"eyePosAndFocus",eval,dummy,dummy);
        animator.setDuration(duration);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    /**
     * Evaluator for a circular or spiral eye/camera movement with potential adaptation of the eye/camera focus.
     * The evaluator returns a float array of length 6 with the new eye/camera position in its components 0-2
     * and the (potentially) new eye/camera focus point in its components 3-5.
     * The array can be used as a parameter for the setEyePosAndFocus() method of a GLRendererCV object.
     */

    private static class EvaluatorTransSpiral implements TypeEvaluator<float[]> {

        private GLRendererCV renderer;   // the renderer to which this evaluator belongs

        private float[] start;  // start position of the shape

        private float[] axisPoint1;  // first point on the axis

        private float[] axisPoint2;  // second point on the axis

        private float[] normedAxisVector; // a vector defining the axis with length 'height'

        private float angle;  // angle of the arc to traverse

        private float[] focusVector;  // vector specifying the constant viewing direction of the eye/camera (only valid if focusControl==CONSTANT_FOCUS_VECTOR)

        private int focusControl;  // treatment of the eye/camera focus (according to the constants defined in GLRendererCV)

        private  float retval[];   // array containing the return values of the evaluator (eyePosX/Y/Z in positions 0-2; eyeFocusX/Y/Z in positions 3-5)

        private  boolean attributesAreValid;  // Some attribute values are stored when the animation starts. 'attributesAreValid' specifies if this has been done already.

        EvaluatorTransSpiral(GLRendererCV renderer, float[] axisPoint1, float[] axisPoint2, float turns, float height, int focusControl) {
            this.renderer = renderer;
            this.axisPoint1 = axisPoint1.clone();
            this.axisPoint2 = axisPoint2.clone();
            this.angle = turns*360;
            normedAxisVector = new float[3];
            float dist = GraphicsUtilsCV.distance3D(axisPoint1,axisPoint2);
            if (dist!=0)
                for (int i=0; i<3; i++)
                    normedAxisVector[i] = (axisPoint2[i]-axisPoint1[i])*height/dist;
            this.focusControl = focusControl;
            retval = new float[6];
            this.attributesAreValid = false;
        }

        public float[] evaluate(float f, float dummy1[], float[] dummy2) {
            if (!attributesAreValid) {
                start = renderer.getEyePos();
                if (focusControl==GLRendererCV.CONSTANT_FOCUS_POINT) {
                    retval[3] = renderer.getEyeFocusX();
                    retval[4] = renderer.getEyeFocusY();
                    retval[5] = renderer.getEyeFocusZ();
                }
                if (focusControl==GLRendererCV.CONSTANT_FOCUS_VECTOR) {
                    focusVector = new float[3];
                    focusVector[0] = renderer.getEyeFocusX()-renderer.getEyePosX();
                    focusVector[1] = renderer.getEyeFocusY()-renderer.getEyePosY();
                    focusVector[2] = renderer.getEyeFocusZ()-renderer.getEyePosZ();
                }
                attributesAreValid = true;
            }
            float[] newPos = GraphicsUtilsCV.rotateAroundAxis(start,axisPoint1,axisPoint2,f*angle);
            for (int i=0; i<3; i++)
                retval[i] = newPos[i] + f*normedAxisVector[i];
            switch (focusControl) {
                case GLRendererCV.CONSTANT_FOCUS_POINT: break;
                case GLRendererCV.CONSTANT_FOCUS_VECTOR:
                    retval[3] = renderer.getEyePosX()+100*focusVector[0];  // factor 100 because small focusVector value lead to a jerky animation
                    retval[4] = renderer.getEyePosY()+100*focusVector[1];
                    retval[5] = renderer.getEyePosZ()+100*focusVector[2];
                    break;
                case GLRendererCV.FOCUS_INTO_MOVEMENT_DIRECTION:
                    retval[3] = 2*retval[0]-renderer.getEyePosX(); // new focus point = retval[0] + (retval[0]-eyePosX) = new position plus current movement direction
                    retval[4] = 2*retval[1]-renderer.getEyePosY();
                    retval[5] = 2*retval[2]-renderer.getEyePosZ();
                    break;
            }
            return retval;
        }

    }

    /**
     * Makes an animator to let the eye/camera rotate in place,
     * i.e. move the focus point of the eye/camera (i.e. the eyeFocus attribute of the renderer) on a circle around an axis that goes through the eye/camera.
     * The position of the eye/camera, i.e. the eyePos attribute of the renderer, will remain unchanged.
     * <P>
     * @param renderer The renderer whose eye/camera shall be rotated.
     * @param angleToTraverse The rotation angle to traverse.
     * @param axis The rotation axis.
     * @param duration The duration of the animation (ms).
     * @return The newly generated animator.
     */

    public static ObjectAnimator makeAnimRot(GLRendererCV renderer, float angleToTraverse, float[] axis, int duration) {
        EvaluatorRot eval = new EvaluatorRot(renderer, angleToTraverse,axis);
        float[] dummy = new float[1];
        ObjectAnimator animator = ObjectAnimator.ofObject(renderer,"eyeFocus",eval,dummy,dummy);
        animator.setDuration(duration);
        animator.setInterpolator(new LinearInterpolator());
        return animator;
    }

    /**
     * Class that defines a TypeEvaluator for a movement of the eyeFocus attribute on a circle around an axis - see method makeAnimRot().
     */

    private static class EvaluatorRot implements TypeEvaluator<float[]> {

        private GLRendererCV renderer;   // the renderer to which the animator that uses this evaluator belongs

        float[] axisVector;  // the direction vector of the axis (the axis going through the center of the eye/camera)

        float angleToTraverse;  // angle of the arc to traverse

        float[] focusStart;  // start position of the eye/camera focus point

        boolean attributeIsValid;

        // The start position of the animated shape is stored when the animation starts.
        // 'attributeIsValid' specifies if this has been done already.
        EvaluatorRot(GLRendererCV renderer, float angleToTraverse, float[] axisVector) {
            this.renderer = renderer;
            this.angleToTraverse = angleToTraverse;
            this.axisVector = axisVector.clone();
            this.attributeIsValid = false;
        }

        public float[] evaluate(float f, float dummy1[], float[] dummy2) {
            if (!attributeIsValid) {
                focusStart = renderer.getEyeFocus();
                attributeIsValid = true;
            }
            float[] eyePos = renderer.getEyePos();
            float[] axis = new float[3];
            for (int i=0;i<3;i++)
                axis[i] = eyePos[i]+axisVector[i];
            return GraphicsUtilsCV.rotateAroundAxis(focusStart,eyePos,axis,f*angleToTraverse);
        }

    }

    /**
     * Makes and returns a popup window
     * through which the view matrix of this renderer, i.e. the eye/camera position (eyePos) and eye/camera focus (eyeFocus), can be controlled.
     */

    static public PopupWindow makeCameraControlPopup(Context context, GLSurfaceViewCV surfaceView) {
        return new CameraControlPopup(context,surfaceView);
    }

    /**
     * Class for popoup windows
     * through which the view matrix of a renderer, i.e. the eye/camera position (eyePos) and eye/camera focus (eyeFocus), can be controlled.
     */

    static private class CameraControlPopup extends PopupWindow {

        private Context context;

        // the surface view whose renderer shall be controlled
        private GLSurfaceViewCV surfaceView;

        // the renderer whose eye/camera position and focus shall be controlled
        private GLRendererCV renderer;

        // the layout for the popup window
        private LinearLayout layout;

        // seekbars for eye/camera position and focus
        private SeekBar seekBarPosX, seekBarPosY, seekBarPosZ, seekBarFocusX, seekBarFocusY, seekBarFocusZ;

        // seekbars for eye/camera rotation
        private SeekBar seekBarRotInPlaceX, seekBarRotInPlaceY, seekBarRotInPlaceZ, seekBarRotAroundX, seekBarRotAroundY, seekBarRotAroundZ;

        // checkbox to specify if the focus direction shall remain the same when the eye/camera position changes
        private CheckBox checkboxKeepFocusDirection;

        // initial values for eye/camera position and focus
        private float initPosX, initPosY, initPosZ, initFocusX, initFocusY, initFocusZ;

        // a little shape to mark the current eye/camera focus point

        GLShapeCV focusMarker;

        CameraControlPopup(Context context, GLSurfaceViewCV surfaceView) {
                super(context);
                this.context = context;
                this.surfaceView = surfaceView;
                this.renderer = surfaceView.getRenderer();
                layout = makeLayoutPosFocus();
                setContentView(layout);
                setFocusable(true);
                int width = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth() - 40;
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, ((Activity) context).getResources().getDisplayMetrics());
                update(0, 0, width, height);
                focusMarker = GLShapeFactoryCV.makeSphere("CamCtrlFocusMarker", 3, GraphicsUtilsCV.white);
                focusMarker.setScale(0.2f).setTrans(renderer.getEyeFocus());
                // focusMarker.setTrans(renderer.getEyeFocus());
                surfaceView.addShape(focusMarker);
                setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        surfaceView.removeShape(focusMarker);
                    }
                });
        }

        // layout for the popup window with seekbars to control eye/camera position and focus
        private LinearLayout makeLayoutPosFocus() {
            // layout for the popup window with seekbars to control eye/camera position and focus
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.layout_popup_camctrl_posfocus, null, false);
            RadioGroup radioGroupLayoutSelection = layout.findViewById(R.id.radioGroupInputSelection);
            radioGroupLayoutSelection.setOnCheckedChangeListener(new RadioGroupListener());
            SeekbarListenerPosFocus seekbarListenerPosFocus = new SeekbarListenerPosFocus();
            seekBarPosX = (SeekBar) layout.findViewById(R.id.seekBarPosX);
            seekBarPosX.setOnSeekBarChangeListener(seekbarListenerPosFocus);
            seekBarPosY = (SeekBar) layout.findViewById(R.id.seekBarPosY);
            seekBarPosY.setOnSeekBarChangeListener(seekbarListenerPosFocus);
            seekBarPosZ = (SeekBar) layout.findViewById(R.id.seekBarPosZ);
            seekBarPosZ.setOnSeekBarChangeListener(seekbarListenerPosFocus);
            seekBarFocusX = (SeekBar) layout.findViewById(R.id.seekBarFocusX);
            seekBarFocusX.setOnSeekBarChangeListener(seekbarListenerPosFocus);
            seekBarFocusY = (SeekBar) layout.findViewById(R.id.seekBarFocusY);
            seekBarFocusY.setOnSeekBarChangeListener(seekbarListenerPosFocus);
            seekBarFocusZ = (SeekBar) layout.findViewById(R.id.seekBarFocusZ);
            seekBarFocusZ.setOnSeekBarChangeListener(seekbarListenerPosFocus);
            ButtonListenerPosFocus buttonListenerPosFocus = new ButtonListenerPosFocus();
            layout.findViewById(R.id.buttonOk).setOnClickListener(buttonListenerPosFocus);
            layout.findViewById(R.id.buttonReset).setOnClickListener(buttonListenerPosFocus);
            layout.findViewById(R.id.buttonFront).setOnClickListener(buttonListenerPosFocus);
            checkboxKeepFocusDirection = (CheckBox) layout.findViewById(R.id.checkboxKeepFocusDirection);
            initPosX = renderer.eyePosInit[0]; initPosY = renderer.eyePosInit[1]; initPosZ = renderer.eyePosInit[2];
            initFocusX = renderer.eyeFocusInit[0]; initFocusY = renderer.eyeFocusInit[1]; initFocusZ = renderer.eyeFocusInit[2];
            initSeekBarsPosFocus();
            return layout;
        }

        private void initSeekBarsPosFocus() {
                seekBarPosX.setProgress((int) ((renderer.getEyePosX() - initPosX) / SeekbarListenerPosFocus.scalingFactor + 50));
                seekBarPosY.setProgress((int) ((renderer.getEyePosY() - initPosY) / SeekbarListenerPosFocus.scalingFactor + 50));
                seekBarPosZ.setProgress((int) ((renderer.getEyePosZ() - initPosZ) / SeekbarListenerPosFocus.scalingFactor + 50));
                seekBarFocusX.setProgress((int) ((renderer.getEyeFocusX() - initFocusX) / SeekbarListenerPosFocus.scalingFactor + 50));
                seekBarFocusY.setProgress((int) ((renderer.getEyeFocusY() - initFocusY) / SeekbarListenerPosFocus.scalingFactor + 50));
                seekBarFocusZ.setProgress((int) ((renderer.getEyeFocusZ() - initFocusZ) / SeekbarListenerPosFocus.scalingFactor + 50));
        }

        private class SeekbarListenerPosFocus implements SeekBar.OnSeekBarChangeListener {
            static final float scalingFactor = 0.5f;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (seekBar == seekBarPosX) {
                    float oldEyePosX = renderer.getEyePosX();
                    renderer.setEyePosX(initPosX + ((i - 50) * scalingFactor));
                    if (checkboxKeepFocusDirection.isChecked()) {
                        renderer.setEyeFocusX(renderer.getEyeFocusX() + renderer.getEyePosX() - oldEyePosX);
                        seekBarFocusX.setProgress((int) ((renderer.getEyeFocusX() - initFocusX) / scalingFactor + 50));
                    }
                }
                if (seekBar == seekBarPosY) {
                    float oldEyePosY = renderer.getEyePosY();
                    renderer.setEyePosY(initPosY + ((i - 50) * scalingFactor));
                    if (checkboxKeepFocusDirection.isChecked()) {
                        renderer.setEyeFocusY(renderer.getEyeFocusY() + renderer.getEyePosY() - oldEyePosY);
                        seekBarFocusY.setProgress((int) ((renderer.getEyeFocusY() - initFocusY) / scalingFactor + 50));
                    }
                }
                if (seekBar == seekBarPosZ) {
                    float oldEyePosZ = renderer.getEyePosZ();
                    renderer.setEyePosZ(initPosZ + ((i - 50) * scalingFactor));
                    if (checkboxKeepFocusDirection.isChecked()) {
                        renderer.setEyeFocusZ(renderer.getEyeFocusZ() + renderer.getEyePosZ() - oldEyePosZ);
                        seekBarFocusZ.setProgress((int) ((renderer.getEyeFocusZ() - initFocusZ) / scalingFactor + 50));
                    }
                }
                if (seekBar == seekBarFocusX)
                    renderer.setEyeFocusX(initFocusX + ((i - 50) * scalingFactor));
                if (seekBar == seekBarFocusY)
                    renderer.setEyeFocusY(initFocusY + ((i - 50) * scalingFactor));
                if (seekBar == seekBarFocusZ)
                    renderer.setEyeFocusZ(initFocusZ + ((i - 50) * scalingFactor));
                if (focusMarker!=null)
                    focusMarker.setTrans(renderer.getEyeFocus());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        }

        private class ButtonListenerPosFocus implements View.OnClickListener {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.buttonOk)
                    dismiss();
                if (v.getId() == R.id.buttonReset) {
                    renderer.resetEyePosAndFocus();
                    initSeekBarsPosFocus();
                }
                if (v.getId() == R.id.buttonFront) {
                    ArrayList<GLShapeCV> shapes = surfaceView.getShapesToRender();
                    if (shapes.size()==0||(shapes.size()==1&&shapes.get(0).getId().equals("CamCtrlFocusMarker"))) return;
                    float minX = Float.MAX_VALUE, maxX = Float.MIN_VALUE, minY = Float.MAX_VALUE, maxY = Float.MIN_VALUE, minZ = Float.MAX_VALUE, maxZ = Float.MIN_VALUE;
                    for (GLShapeCV shape : shapes) {
                        if (shape.getId().equals("CamCtrlFocusMarker")) continue;
                        float[] pos = shape.getTrans();
                        if (pos[0]<minX) minX = pos[0];
                        if (pos[0]>maxX) maxX = pos[0];
                        if (pos[1]<minY) minY = pos[1];
                        if (pos[1]>maxY) maxY = pos[1];
                        if (pos[2]<minZ) minZ = pos[2];
                        if (pos[2]>maxZ) maxZ = pos[2];
                    }
                    GLRendererCV renderer = surfaceView.getRenderer();
                    float x = (maxX+minX)/2, y = (maxY+minY)/2, z = (maxZ+minZ)/2;
                    renderer.setEyeFocus(x,y,z);
                    renderer.setEyePos(x,y,maxZ+Math.max(maxX-minX,maxY-minY)+3);
                    initSeekBarsPosFocus();
                }
           }
        }

        // layout for the popup window with seekbars to let the eye/camera rotate around its own axes and around the focus point
        private LinearLayout makeLayoutRot() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.layout_popup_camctrl_rot, null, false);
            RadioGroup radioGroupLayoutSelection = layout.findViewById(R.id.radioGroupInputSelection);
            radioGroupLayoutSelection.setOnCheckedChangeListener(new RadioGroupListener());
            SeekbarListenerRotInPlace seekbarListenerRotInPlace = new SeekbarListenerRotInPlace();
            seekBarRotInPlaceX = (SeekBar) layout.findViewById(R.id.seekBarRotInPlaceX);
            seekBarRotInPlaceX.setOnSeekBarChangeListener(seekbarListenerRotInPlace);
            seekBarRotInPlaceY = (SeekBar) layout.findViewById(R.id.seekBarRotInPlaceY);
            seekBarRotInPlaceY.setOnSeekBarChangeListener(seekbarListenerRotInPlace);
            seekBarRotInPlaceZ = (SeekBar) layout.findViewById(R.id.seekBarRotInPlaceZ);
            seekBarRotInPlaceZ.setOnSeekBarChangeListener(seekbarListenerRotInPlace);
            SeekbarListenerRotAround seekbarListenerRotAround = new SeekbarListenerRotAround();
            seekBarRotAroundX = (SeekBar) layout.findViewById(R.id.seekBarRotAroundX);
            seekBarRotAroundX.setOnSeekBarChangeListener(seekbarListenerRotAround);
            seekBarRotAroundY = (SeekBar) layout.findViewById(R.id.seekBarRotAroundY);
            seekBarRotAroundY.setOnSeekBarChangeListener(seekbarListenerRotAround);
            seekBarRotAroundZ = (SeekBar) layout.findViewById(R.id.seekBarRotAroundZ);
            seekBarRotAroundZ.setOnSeekBarChangeListener(seekbarListenerRotAround);
            initPosX = renderer.getEyePosX(); initPosY = renderer.getEyePosY(); initPosZ = renderer.getEyePosZ();
            initFocusX = renderer.getEyeFocusX(); initFocusY = renderer.getEyeFocusY(); initFocusZ = renderer.getEyeFocusZ();
            return layout;
        }

        private class SeekbarListenerRotInPlace implements SeekBar.OnSeekBarChangeListener {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float angleX = seekBarRotInPlaceX.getProgress() *3.6f-180;
                float angleY = seekBarRotInPlaceY.getProgress() *3.6f-180;
                float angleZ = seekBarRotInPlaceZ.getProgress() *3.6f-180;
                float[] rotMatrixX = new float[16];
                float[] rotMatrixY = new float[16];
                float[] rotMatrixZ = new float[16];
                float[] rotMatrix = new float[16];
                Matrix.setIdentityM(rotMatrixX,0);
                Matrix.rotateM(rotMatrixX,0,angleX,1,0,0);
                Matrix.setIdentityM(rotMatrixY,0);
                Matrix.rotateM(rotMatrixY,0,angleY,0,1,0);
                Matrix.setIdentityM(rotMatrixZ,0);
                Matrix.rotateM(rotMatrixZ,0,angleZ,0,0,1);
                Matrix.multiplyMM(rotMatrix,0,rotMatrixY,0,rotMatrixX,0);
                Matrix.multiplyMM(rotMatrix,0,rotMatrixZ,0,rotMatrix,0);
                float[] vectorPosToInitFocus = GraphicsUtilsCV.vectorFromTo3D(renderer.getEyePos(),new float[]{initFocusX,initFocusY,initFocusZ});
                float[] vectorPosToNewFocus = new float[4];
                Matrix.multiplyMV(vectorPosToNewFocus,0,rotMatrix,0,GraphicsUtilsCV.homogeneousCoordsForVector3D(vectorPosToInitFocus),0);
                renderer.setEyeFocus(renderer.getEyePosX()+vectorPosToNewFocus[0],renderer.getEyePosY()+vectorPosToNewFocus[1],renderer.getEyePosZ()+vectorPosToNewFocus[2]);
                focusMarker.setScale(0.2f).setTrans(renderer.getEyeFocus());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        }

        private class SeekbarListenerRotAround implements SeekBar.OnSeekBarChangeListener {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float angleX = seekBarRotAroundX.getProgress()*3.6f-180;
                float angleY = seekBarRotAroundY.getProgress()*3.6f-180;
                float angleZ = seekBarRotAroundZ.getProgress()*3.6f-180;
                float[] rotMatrixX = new float[16];
                float[] rotMatrixY = new float[16];
                float[] rotMatrixZ = new float[16];
                float[] rotMatrix = new float[16];
                Matrix.setIdentityM(rotMatrixX,0);
                Matrix.rotateM(rotMatrixX,0,angleX,1,0,0);
                Matrix.setIdentityM(rotMatrixY,0);
                Matrix.rotateM(rotMatrixY,0,angleY,0,1,0);
                Matrix.setIdentityM(rotMatrixZ,0);
                Matrix.rotateM(rotMatrixZ,0,angleZ,0,0,1);
                Matrix.multiplyMM(rotMatrix,0,rotMatrixY,0,rotMatrixX,0);
                Matrix.multiplyMM(rotMatrix,0,rotMatrixZ,0,rotMatrix,0);
                float[] vectorFocusToInitPos = GraphicsUtilsCV.vectorFromTo3D(renderer.getEyeFocus(),new float[]{initPosX,initPosY,initPosZ});
                float[] vectorFocusToNewPos = new float[4];
                Matrix.multiplyMV(vectorFocusToNewPos,0,rotMatrix,0,GraphicsUtilsCV.homogeneousCoordsForVector3D(vectorFocusToInitPos),0);
                renderer.setEyePos(renderer.getEyeFocusX()+vectorFocusToNewPos[0],renderer.getEyeFocusY()+vectorFocusToNewPos[1],renderer.getEyeFocusZ()+vectorFocusToNewPos[2]);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

        }

        // radio group to select which control widgets shall be displayed
        private class RadioGroupListener implements RadioGroup.OnCheckedChangeListener {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                layout.removeAllViewsInLayout();
                int newHeightValue = 0;
                int buttonId = group.getCheckedRadioButtonId();
                if (buttonId==R.id.radioPosFocus) {
                    layout.addView(makeLayoutPosFocus());
                    newHeightValue = 300;
                }
                if (buttonId==R.id.radioRot) {
                    layout.addView(makeLayoutRot());
                    newHeightValue = 240;
                }
                /*
                if (buttonId==R.id.radioPath) {
                    // TO BE FILLED
                    // layout.addView(makeLayoutRot());
                    newHeightValue = 50;
                }
                */
                int width = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth() - 40;
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newHeightValue, ((Activity) context).getResources().getDisplayMetrics());
                CameraControlPopup.this.update(0, 0, width, height);
            }

        }

    }

    /**
     * Class for listeners that can be attached to a surface view of class GLSurfaceViewCV
     * and handle gestures to set the position and the focus of the eye/camera.
     * <P>
     * In particular, the following gestures are supported:
     * <UL>
     * <LI>Dragging with one finger: Translate the camera to a new position on the "eye plane",
     *     i.e. on the plane that is perpendicular to the vector between eye/camera position and focus point.
     *     The focus point will be moved accordingly such that the vector between eye/camera position and focus point will remain constant.
     * <LI>Scaling with two fingers: Translate the eye/camera towards the current focus point or away from it.
     *     The focus point will be moved accordingly, as above.
     * <LI>Double tap: Set the eye/camera focus point.
     * </UL>
     */

    public static class OnTouchListener implements GLSurfaceViewCV.GLOnTouchListenerCV  {

        private GLRendererCV renderer;    // the renderer with the camera to be controlled

        private float[] rayVector;    // the ray vector of the current touch

        private GLShapeCV axes = null;    // the axes showing the world coordinate system

        private GestureDetector simpleGestureDetector;   // the detector for gestures to translate and rotate the shape

        private ScaleGestureDetector scaleGestureDetector;   // the detector for gestures to scale the shape

        private float prevX,prevY;   // the previous positions of the first and (if present) the second finger

        private boolean gestureDetected = false;   // set to true by the scale gesture detector such that the touch event will not be analyzed any further

        private boolean endingTwoFingerGesture = false;

        public OnTouchListener(Context context) {
            simpleGestureDetector = new GestureDetector(context, new CameraControl_SimpleGestureListener());
            simpleGestureDetector.setIsLongpressEnabled(true);
            simpleGestureDetector.setOnDoubleTapListener(new CameraControl_SimpleGestureListener());
            scaleGestureDetector = new ScaleGestureDetector(context, new CameraControl_ScaleListener());
        }

        @Override
        public boolean onTouch(GLSurfaceViewCV surfaceView, GLTouchEventCV event) {
            MotionEvent mEvent = event.getMotionEvent();
            renderer = surfaceView.getRenderer();
            rayVector = event.getRayVector();
            if (axes==null) {
                axes = GLShapeFactoryCV.makeAxes(GraphicsUtilsCV.red,GraphicsUtilsCV.green,GraphicsUtilsCV.blue,0.05f);
                surfaceView.addShape(axes);
            }
            simpleGestureDetector.onTouchEvent(mEvent);   // check for long press, double tap and fling gestures (fling gestures currently have no effect)
            scaleGestureDetector.onTouchEvent(mEvent);
            if (gestureDetected) {   // to avoid the further processing of the event
                // Log.v("GLDEMO","Scale - Pos X: "+renderer.getEyePosX()+" "+" Y: "+renderer.getEyePosY()+" "+" Z: "+renderer.getEyePosZ());
                gestureDetected = false;
                return true;
            }
            if (mEvent.getAction()==MotionEvent.ACTION_DOWN) {
                // Log.v("GLDEMO","Down");
                prevX = mEvent.getX(0);  // store the position where the first finger has gone down
                prevY = mEvent.getY(0);
            }
            if (mEvent.getAction()==MotionEvent.ACTION_MOVE) {
                if (endingTwoFingerGesture) return true;
                // Log.v("GLDEMO","Move");
                // translation into the x or y direction
                float factor = 0.0015f;
                int sign;
                if (renderer.getEyePosZ()>0)
                    sign = 1;
                else sign = -1;
                renderer.setEyePosX(renderer.getEyePosX()+sign*factor*(mEvent.getX()-prevX)*(3-renderer.getEyePosZ()));
                renderer.setEyePosY(renderer.getEyePosY()-sign*factor*(mEvent.getY()-prevY)*(3-renderer.getEyePosZ()));
                renderer.setEyeFocusX(renderer.getEyeFocusX()+sign*factor*(mEvent.getX()-prevX)*(3-renderer.getEyePosZ()));
                renderer.setEyeFocusY(renderer.getEyeFocusY()-sign*factor*(mEvent.getY()-prevY)*(3-renderer.getEyePosZ()));
                // Log.v("GLDEMO","Move - Pos X: "+renderer.getEyePosX()+" "+" Y: "+renderer.getEyePosY()+" "+" Z: "+renderer.getEyePosZ());
                prevX = mEvent.getX();
                prevY = mEvent.getY();
            }
            if (mEvent.getAction()==MotionEvent.ACTION_POINTER_UP) {
                // Log.v("GLDEMO","Pointer Up");
                endingTwoFingerGesture = true;  // ending phase of a two-finger gesture: the first finger has gone up but the second remains
            }
            if (mEvent.getAction()==MotionEvent.ACTION_UP) {
                // Log.v("GLDEMO","Up");
                // all fingers have gone up > nothing is going on anymore
                surfaceView.removeShape(axes);
                endingTwoFingerGesture = false;
                axes=null;
            }
            return true;
        }

        /**
         * Listener to handle double tap and scale gestures
         * and to handle them as explained above.
         */

        class CameraControl_SimpleGestureListener extends GestureDetector.SimpleOnGestureListener {

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                renderer.setEyeFocusX(renderer.getEyePosX()+rayVector[0]);
                renderer.setEyeFocusY(renderer.getEyePosY()+rayVector[1]);
                renderer.setEyeFocusZ(renderer.getEyePosZ()+rayVector[2]);
                Log.v("GLDEMO","Focus X: "+renderer.getEyeFocusX()+" "+" Y: "+renderer.getEyeFocusY()+" "+" Z: "+renderer.getEyeFocusZ());
                gestureDetected = true;
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                // Currently no effect
                return true;
            }

        }

        /**
         * Listener to detect scale gestures
         * and to handle them as explained above.
         */

        class CameraControl_ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

            public boolean onScale(ScaleGestureDetector detector) {
                float stepSize = .025f;
                if (detector.getScaleFactor()<1)
                    stepSize = -stepSize;
                float[] vectorPosFocus = GraphicsUtilsCV.vectorFromTo3D(renderer.getEyePos(),renderer.getEyeFocus());
                renderer.setEyePosX(renderer.getEyePosX()+stepSize*vectorPosFocus[0]);
                renderer.setEyePosY(renderer.getEyePosY()+stepSize*vectorPosFocus[1]);
                renderer.setEyePosZ(renderer.getEyePosZ()+stepSize*vectorPosFocus[2]);
                renderer.setEyeFocusX(renderer.getEyeFocusX()+stepSize*vectorPosFocus[0]);
                renderer.setEyeFocusY(renderer.getEyeFocusY()+stepSize*vectorPosFocus[1]);
                renderer.setEyeFocusZ(renderer.getEyeFocusZ()+stepSize*vectorPosFocus[2]);
                Log.v("GLDEMO",renderer.getEyePosX()+" "+renderer.getEyePosY()+" "+renderer.getEyePosZ()+"   "+renderer.getEyeFocusX()+" "+renderer.getEyeFocusY()+" "+renderer.getEyeFocusZ());
                gestureDetected = true;
                return true;
            }

            public boolean onScaleOld(ScaleGestureDetector detector) {
                float adjustFactor = 1f;
                float scaleFactor = detector.getScaleFactor()*adjustFactor;
                float stepSize = .1f;
                if (scaleFactor>1) {
                    renderer.setEyePosZ(renderer.getEyePosZ() - stepSize);
                    renderer.setEyeFocusZ(renderer.getEyeFocusZ() - stepSize);
                }
                else {
                    renderer.setEyePosZ(renderer.getEyePosZ() + stepSize);
                    renderer.setEyeFocusZ(renderer.getEyeFocusZ() - stepSize);
                }
                gestureDetected = true;
                return true;
            }

        }

    }

}

/*

    GLShapeCV focus =null;

    class CameraControlListener implements GLSurfaceViewCV.GLOnTouchListenerCV {

        private GestureDetector flingAndDoubletapDetector;

        private ScaleGestureDetector scaleDetector;

        final int screenWidth, screenHeight;

        CameraControlListener() {
            flingAndDoubletapDetector = new GestureDetector(TouchGesturesActivity.this, new CameraControl_FlingListener());
            flingAndDoubletapDetector.setOnDoubleTapListener(new CameraControl_DoubleTapListener());
            scaleDetector = new ScaleGestureDetector(TouchGesturesActivity.this, new CameraControl_ScaleListener());
            screenWidth = getWindowManager().getDefaultDisplay().getWidth();
            screenHeight = getWindowManager().getDefaultDisplay().getHeight();
        }

        @Override
        public boolean onTouch(GLSurfaceViewCV surfaceView, GLTouchEventCV event) {
            MotionEvent mEvent = event.getMotionEvent();
            flingAndDoubletapDetector.onTouchEvent(mEvent);
            scaleDetector.onTouchEvent(mEvent);
            return true;
        }

        class CameraControl_DoubleTapListener extends GestureDetector.SimpleOnGestureListener {

            public boolean onDoubleTap(MotionEvent e) {
                float factor = 0.01f;
                renderer.setEyeFocusX(renderer.getEyeFocusX()+(e.getX()-screenWidth/2)*factor);
                renderer.setEyeFocusY(renderer.getEyeFocusY()-(e.getY()-screenHeight/2)*factor);
                Log.v("GLDEMO",renderer.getEyeFocusX()+" "+renderer.getEyeFocusY()+" "+renderer.getEyeFocusZ());
                return false;
            }

        }

        class CameraControl_FlingListener extends GestureDetector.SimpleOnGestureListener {

            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                if (focus !=null) surfaceView.removeShape(focus);
                float[] focusDir = new float[] {
                        renderer.getEyeFocusX() - renderer.getEyePosX(),
                        renderer.getEyeFocusY() - renderer.getEyePosY(),
                        renderer.getEyeFocusZ() - renderer.getEyePosZ(),
                };
                float vectorX = e2.getX()-e1.getX(), vectorY = -(e2.getY()-e1.getY());
                float factor = 0.01f;
                renderer.setEyePosX(renderer.getEyePosX()-factor*vectorX);
                renderer.setEyePosY(renderer.getEyePosY()-factor*vectorY);
                renderer.setEyeFocusX(renderer.getEyeFocusX()-factor*vectorX);
                renderer.setEyeFocusY(renderer.getEyeFocusY()-factor*vectorY);
                Log.v("GLDEMO","Pos: "+renderer.getEyePosX()+" "+renderer.getEyePosY()+" "+renderer.getEyePosZ());
                Log.v("GLDEMO","Foc: "+renderer.getEyeFocusX()+" "+renderer.getEyeFocusY()+" "+renderer.getEyeFocusZ());
                focus = GLShapeFactoryCV.makeSphere("Focus",GraphicsUtilsCV.red);
                focus.setTrans(renderer.getEyeFocusX(),renderer.getEyeFocusY(),renderer.getEyeFocusZ()).setScale(0.25f);
                // surfaceView.addShape(focus);
                return false;
            }

        }

        class CameraControl_ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

            public boolean onScale(ScaleGestureDetector detector) {

                if (focus !=null) surfaceView.removeShape(focus);

                float stepSize = 0.05f;
                float[] focusDir = new float[] {
                        renderer.getEyeFocusX() - renderer.getEyePosX(),
                        renderer.getEyeFocusY() - renderer.getEyePosY(),
                        renderer.getEyeFocusZ() - renderer.getEyePosZ(),
                };
                if (detector.getScaleFactor()>1) {
                    // renderer.setEyePosZ(renderer.getEyePosZ() - stepSize);
                    renderer.setEyePosX((1-stepSize)*renderer.getEyePosX());
                    renderer.setEyePosY((1-stepSize)*renderer.getEyePosY());
                    renderer.setEyePosZ((1-stepSize)*renderer.getEyePosZ());
                    renderer.setEyeFocusX(renderer.getEyePosX()+focusDir[0]);
                    renderer.setEyeFocusY(renderer.getEyePosY()+focusDir[1]);
                    renderer.setEyeFocusZ(renderer.getEyePosZ()+focusDir[2]);
                    /*
                    renderer.setEyeFocusX((1+stepSize)*renderer.getEyeFocusX());
                    renderer.setEyeFocusY((1+stepSize)*renderer.getEyeFocusY());
                    renderer.setEyeFocusZ((1+stepSize)*renderer.getEyeFocusZ());
                     */ /*
                }
                        else {
                        // renderer.setEyePosZ(renderer.getEyePosZ() + stepSize);
                        renderer.setEyePosX((1+stepSize)*renderer.getEyePosX());
                        renderer.setEyePosY((1+stepSize)*renderer.getEyePosY());
                        renderer.setEyePosZ((1+stepSize)*renderer.getEyePosZ());
                        renderer.setEyeFocusX(renderer.getEyePosX()-focusDir[0]);
                        renderer.setEyeFocusY(renderer.getEyePosY()-focusDir[1]);
                        renderer.setEyeFocusZ(renderer.getEyePosZ()-focusDir[2]);
                    /*
                    renderer.setEyeFocusX((1-stepSize)*renderer.getEyeFocusX());
                    renderer.setEyeFocusY((1-stepSize)*renderer.getEyeFocusY());
                    renderer.setEyeFocusZ((1-stepSize)*renderer.getEyeFocusZ());

                     */ /*
                        }
                        Log.v("GLDEMO","Pos: "+renderer.getEyePosX()+" "+renderer.getEyePosY()+" "+renderer.getEyePosZ());
                        Log.v("GLDEMO","Foc: "+renderer.getEyeFocusX()+" "+renderer.getEyeFocusY()+" "+renderer.getEyeFocusZ());
                        focus = GLShapeFactoryCV.makeSphere("Focus",GraphicsUtilsCV.red);
                        focus.setTrans(renderer.getEyeFocusX(),renderer.getEyeFocusY(),renderer.getEyeFocusZ()).setScale(0.25f);
                        // surfaceView.addShape(focus);
                        return true;
                        }

                        }

                        }

*/