// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 6.11.2023

package de.thkoeln.cvogt.android.opengl_utilities;

import android.content.Context;
import android.opengl.Matrix;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

/**
 * Class for listeners that can be attached to a surface view of class GLSurfaceViewCV
 * and handle gestures to translate, rotate and scale a shape of class GLShapeCV.
 * The user selects the shape to be controlled by a double tap on the shape.
 * <P>
 * In particular, the following gestures are supported:
 * <UL>
 * <LI>Double tap with one finger: Select/deselect the shape to be controlled
 * <LI>Dragging with one finger: Translate the shape to a new (x,y) position
 * <LI>Long press with one finger: Translate the shape to a new z position
 * (either into the positive or into the negative direction, depending on previous double tap gestures)
 * <LI>Single tap with one finger: Change the direction of the z translation
 * <LI>Dragging with two fingers (horizontally or vertically): Rotation around the y axis or the x axis, respectively
 * <LI>Rotation with two fingers: Rotation around the z axis
 * <LI>Scaling with two fingers and a third finger in the center: Scaling
 * <LI>Fling: Currently no effect
 * </UL>
 * @see GLSurfaceViewCV
 * @see GLShapeCV
 */

public class GLShapeControlListenerCV implements GLSurfaceViewCV.GLOnTouchListenerCV  {

    private GLTouchEventCV touchEvent;    // the touch event currently handled

    private GLSurfaceViewCV surfaceView;    // the surface view on which the touch event occured

    private GLShapeCV shapeUnderControl;   // the shape currently controlled by this listener

    private GLShapeCV marker;   // a wireframe cube that marks the currently controlled shape

    private GestureDetector simpleGestureDetector;   // the detector for gestures to translate and rotate the shape

    private ScaleGestureDetector scaleGestureDetector;   // the detector for gestures to scale the shape

    private float prevX,prevY,prevX_2ndPointer,prevY_2ndPointer;   // the previous positions of the first and (if present) the second finger

    private boolean ongoingTransZ = false;   // Is a z translation ongoing?
                                             // Is set to true by a long press such that subsequent finger moves will effect a z translation.
                                             // Is reset to false when the finger goes up.

    private int transZDirection = 1;   // the current direction of z translations (1 or -1)

    private boolean gestureDetected = false;   // set to true by the scale gesture detector such that the touch event will not be analyzed any further

    private boolean endingTwoFingerGesture = false;   // is true in the end phase of two-finger gesture,
                                                      // i.e. between the instants when the first and the second finger leave the display,
                                                      // such that the remaining finger will not cause a translation of the shape

    public GLShapeControlListenerCV(Context context) {
        simpleGestureDetector = new GestureDetector(context, new ShapeControl_SimpleGestureListener());
        simpleGestureDetector.setIsLongpressEnabled(true);
        simpleGestureDetector.setOnDoubleTapListener(new ShapeControl_SimpleGestureListener());
        scaleGestureDetector = new ScaleGestureDetector(context, new ShapeControl_ScaleListener(this));
    }

    @Override
    public boolean onTouch(GLSurfaceViewCV surfaceView, GLTouchEventCV event) {

        this.surfaceView = surfaceView;
        this.touchEvent = event;

        MotionEvent mEvent = event.getMotionEvent();
        // shapeUnderControl = surfaceView.touchedShape(event);  // the touched shape shall be controlled
        simpleGestureDetector.onTouchEvent(mEvent);   // check for long press, double tap and fling gestures (fling gestures currently have no effect)
        if (mEvent.getPointerCount()==3)   // check for a scale gesture only if three fingers are on the display
            scaleGestureDetector.onTouchEvent(mEvent);
        if (gestureDetected) {   // to avoid the further processing of the event
            gestureDetected = false;
            return true;
        }
        if (mEvent.getAction()==MotionEvent.ACTION_DOWN) {
            Log.v("GLDEMO","Down");
            prevX = mEvent.getX(0);  // store the position where the first finger has gone down
            prevY = mEvent.getY(0);
        }
        if (mEvent.getActionMasked()==MotionEvent.ACTION_POINTER_DOWN) {
            Log.v("GLDEMO","Pointer Down");
            prevX_2ndPointer = mEvent.getX(1);  // store the position where the second finger has gone down
            prevY_2ndPointer = mEvent.getY(1);
        }
        if (mEvent.getAction()==MotionEvent.ACTION_MOVE) {
            if (shapeUnderControl==null|| endingTwoFingerGesture) return true;  // if after a rotation only one finger has remained on the display and will go up soon, do nothing, i.e. do not move the shape
            if (mEvent.getPointerCount()>1) {
                // if two fingers are on the display, rotate the shape if there is a rotation gesture
                float[] rotMatrix1 = shapeUnderControl.getRotationMatrix();  // current rotation matrix of the shape
                float[] rotMatrix2 = new float[16];  // additional rotation to be applied to the shape
                // differences between the previous positions and the current positions of the two finger
                float diffX = prevX-mEvent.getX(0);
                float diffY = prevY-mEvent.getY(0);
                float diffX_2ndPointer = prevX_2ndPointer-mEvent.getX(1);
                float diffY_2ndPointer = prevY_2ndPointer-mEvent.getY(1);
                if((diffX*diffX_2ndPointer<-1.5f)||(diffY*diffY_2ndPointer<-1.5f)) {
                    // the two pointers move into different directions > rotation gesture > z rotation
                    Log.v("GLDEMO","          Rot Z ");
                    // determine the direction of the rotation
                    float diffX_UpperPointer;  //
                    if (mEvent.getY(0)>mEvent.getY(1))
                        diffX_UpperPointer = diffX;
                    else
                        diffX_UpperPointer = diffX_2ndPointer;
                    if (diffX_UpperPointer<0)
                        Matrix.setRotateM(rotMatrix2, 0, 1, 0, 0, 1);
                    else
                        Matrix.setRotateM(rotMatrix2, 0, -1, 0, 0, 1);
                }
                else
                if (Math.abs(mEvent.getY(1)-mEvent.getY(0))>2*Math.abs(mEvent.getX(1)-mEvent.getX(0))
                        // the fingers are placed vertically, i.e. one above the other: cannot be a rotation around the x axis
                        &&diffX*diffX_2ndPointer>1) {   // 1 (and not 0): both fingers move into the same direction by more than little
                    // both fingers move horizontally into the same direction > rotation around the y axis
                    Log.v("GLDEMO", "     Rot Y");
                    if (diffX < 0)   // rotate into the same direction as the fingers move
                        Matrix.setRotateM(rotMatrix2, 0, 1, 0, 1, 0);
                    else
                        Matrix.setRotateM(rotMatrix2, 0, -1, 0, 1, 0);
                }
                else
                if (Math.abs(mEvent.getX(1)-mEvent.getX(0))>2*Math.abs(mEvent.getY(1)-mEvent.getY(0))
                        // the fingers are placed horizontally, i.e. side by side: cannot be a rotation around the y axis
                        &&diffY*diffY_2ndPointer>1) {   // 1 (and not 0): both fingers move into the same direction by more than little
                    // both fingers move vertically into the same direction > rotation around the x axis
                    Log.v("GLDEMO", "Rot X");
                    if (diffY < 0)
                        Matrix.setRotateM(rotMatrix2, 0, 1, 1, 0, 0);
                    else
                        Matrix.setRotateM(rotMatrix2, 0, -1, 1, 0, 0);
                }
                else Matrix.setIdentityM(rotMatrix2,0);    // no additional rotation if no corresponding gesture
                // apply the additional rotation
                Matrix.multiplyMM(rotMatrix1,0,rotMatrix2,0,rotMatrix1,0);
                shapeUnderControl.setRotationMatrix(rotMatrix1,false);
                // store the current positions
                prevX = mEvent.getX(0);
                prevX_2ndPointer = mEvent.getX(1);
                prevY = mEvent.getY(0);
                prevY_2ndPointer = mEvent.getY(1);
                return true;
            }
            // from here on: only one finger on the display > translation of the shape
            Log.v("GLDEMO","Move");
            if (ongoingTransZ) {  // if (initialized by a previous long press) a z translation is going on, translate the shape along the z axis
                shapeUnderControl.setTransZ(shapeUnderControl.getTransZ()+transZDirection*.5f);
                marker.setTransZ(shapeUnderControl.getTransZ()+1);
                Log.v("GLDEMO","TransZ "+transZDirection*.5f);
                return true;
            }
            // translation into the x or y direction
            float factor = 0.0015f;
            shapeUnderControl.setTransX(shapeUnderControl.getTransX()+factor*(mEvent.getX()-prevX)*(3- shapeUnderControl.getTransZ()));
            marker.setTransX(shapeUnderControl.getTransX());
            shapeUnderControl.setTransY(shapeUnderControl.getTransY()-factor*(mEvent.getY()-prevY)*(3- shapeUnderControl.getTransZ()));
            marker.setTransY(shapeUnderControl.getTransY());
            prevX = mEvent.getX();
            prevY = mEvent.getY();
        }
        if (mEvent.getAction()==MotionEvent.ACTION_POINTER_UP) {
            Log.v("GLDEMO","Pointer Up");
            endingTwoFingerGesture = true;  // ending phase of a two-finger gesture: the first finger has gone up but the second remains
        }
        if (mEvent.getAction()==MotionEvent.ACTION_UP) {
            Log.v("GLDEMO","Up");
            // all fingers have gone up > nothing is going on anymore
            endingTwoFingerGesture = false;
            ongoingTransZ = false;
        }
        return true;
    }

    /**
     * Listener to detect long press, double tap and fling gestures
     * and to handle them as explained above.
     */

    class ShapeControl_SimpleGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public void onLongPress(MotionEvent e) {
            // initiation of a translation in the z dimension
            Log.v("GLDEMO","Long Press");
            if (shapeUnderControl == null) return;
            ongoingTransZ = true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            // reverse the direction of translation in the z dimension
            Log.v("GLDEMO","Single Tap Confirmed");
            if (shapeUnderControl == null) return true;
            transZDirection = -transZDirection;
            return true;
        }

        public boolean onDoubleTap(MotionEvent e) {
            // reverse the direction of translation in the z dimension
            Log.v("GLDEMO","Double Tap");
            GLShapeCV tappedShape = surfaceView.touchedShape(touchEvent);
            if (tappedShape==marker)
                return true;
            if (tappedShape==shapeUnderControl||tappedShape==null) {
                surfaceView.removeShape(marker);
                marker=null;
                shapeUnderControl=null;
                return true;
            }
            shapeUnderControl = tappedShape;
            Log.v("GLDEMO","Shape under control: "+shapeUnderControl.getId());
            if (marker!=null)
                surfaceView.removeShape(marker);
            marker = GLShapeFactoryCV.makeCubeWireframe("Marker",GraphicsUtilsCV.white,3);
            float sizeX = shapeUnderControl.getIntrinsicSize(0)*shapeUnderControl.getScaleX();
            float sizeY = shapeUnderControl.getIntrinsicSize(1)*shapeUnderControl.getScaleY();
            float sizeZ = shapeUnderControl.getIntrinsicSize(2)*shapeUnderControl.getScaleZ();
            float size = sizeX;
            if (sizeY>size) size = sizeY;
            if (sizeZ>size) size = sizeZ;
            marker.setTrans(shapeUnderControl.getTrans()).setScale(size*1.2f);
            surfaceView.addShape(marker);
            return true;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            // currently no effect
            Log.v("GLDEMO","Fling");
                /*
                float vectorX = e2.getX()-e1.getX(), vectorY = -(e2.getY()-e1.getY());
                float factor = 0.01f;
                if (vectorX>vectorY)
                   touchedShape.setRotation(touchedShape.getRotAngle()+factor*(vectorX+vectorY),touchedShape.getRotAxis());
                gestureDetected = true;

                 */
            return false;
        }

    }

    /**
     * Listener to detect scale gestures
     * and to handle them as explained above.
     */

    class ShapeControl_ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        private GLShapeControlListenerCV shapeControlListener;

        ShapeControl_ScaleListener(GLShapeControlListenerCV shapeControlListener) {
            this.shapeControlListener = shapeControlListener;
        }

        public boolean onScale(ScaleGestureDetector detector) {
            // scale the shape evenly in all three dimensions
            Log.v("GLDEMO","Scale");
            float adjustFactor = 1f;
            float scaleFactor = detector.getScaleFactor()*adjustFactor;
            shapeUnderControl.setScaleX(shapeUnderControl.getScaleX()*scaleFactor);
            shapeUnderControl.setScaleY(shapeUnderControl.getScaleY()*scaleFactor);
            shapeUnderControl.setScaleZ(shapeUnderControl.getScaleZ()*scaleFactor);
            marker.setScale(marker.getScaleX()*scaleFactor);
            shapeControlListener.gestureDetected = true;
            return true;
        }

    }

}


