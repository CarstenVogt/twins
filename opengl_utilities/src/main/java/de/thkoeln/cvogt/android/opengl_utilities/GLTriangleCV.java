// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 9.6.2023

package de.thkoeln.cvogt.android.opengl_utilities;

import android.graphics.Bitmap;
import android.opengl.Matrix;
import android.util.Log;

import java.io.Serializable;

/**
 * Class to specify triangles, i.e. fundamental building units of OpenGL shapes.
 * Shapes, i.e. objects of class <I>GLShapeCV</I> are defined by a set of triangles and/or lines, i.e. of objects of this class <I>GLTriangleCV</I> and/or of class <I>GLLineCV</I>.
 * <P>
 * The form of a triangle is specified by its three vertices as defined by the <I>vertices</I> attribute.
 * <P>
 * There are three ways to specify the color values of the individual pixels of a triangle:
 * <UL>
 * <P><LI>by a uniform color for the whole triangle,
 * <P><LI>by a color gradient where pixel colors are interpolated from the colors of the three vertices or
 * <P><LI>by a texture as defined by a bitmap image.
 * </UL>
 * <P>The following attributes support these options and override each others in the given order:
 * <UL>
 * <P><LI>If the attribute <I>uniformColor</I> is not null, it specifies the uniform color of the triangle.
 * <P><LI>If the attribute <I>uniformColor</I> is null and the attribute <I>vertexColors</I> is not, a color gradient is used as defined by the vertex colors.
 * <P><LI>If the attributes <I>uniformColor</I> and <I>vertexColors</I> are both null, a bitmap will be applied as a texture,
 * as defined by the attributes <I>textureBitmap</I> and <I>uvCoordinates</I>.
 * </UL>
 * Note the difference between (1) an object of this class <I>GLTriangleCV</I> and (2) an object of class <I>GLShapeCV</I> that contains a single triangle,
 * as produced by GLShapeFactory.makeTriangle():
 * (1) is used as an interior component of a <I>GLShapeCV</I> object and hence cannot be directly drawn on the display.
 * To display single triangles, use objects of type (2).
 * @see GLShapeCV
 * @see GLLineCV
 */

public class GLTriangleCV {

    /** The name / ID of the triangle. */

    private String id;

    /** The number / ID of the triangle group to which the triangle belongs, 0 being the default value.
     * Triangle groups could e.g. used in GLShapeCV objects to group their triangles. */

    private int groupId;

    /** The three vertices of the triangle:
        first index: vertex number (0,1,2), second index: 0 = x coordinate, 1 = y coordinate, 2 = z coordinate. */

    private float vertices[][];

    /** The uniform color of the triangle (four values: RGBA).
     *  If this attribute is null then the color is defined by a color gradient calculated from the three vertex colors (attribute 'vertexColors')
     *  or by a texture from a Bitmap object (attributes 'textureBitmap' and 'uvCoordinates'). */

    private float uniformColor[];

    /** The colors of the three vertices (vertices in the order as specified by 'vertices', color as RGBA).
        Only valid if 'uniformColor' is null.
        If this attribute and 'uniformColor' are null the triangle is textured as specified by 'textureBitmap' and 'uvCoordinates'. */

    private float vertexColors[][] = null;

    /** The bitmap to be used as texture. Only valid if 'uniformColor' and 'vertexColors' are null. */

    private Bitmap textureBitmap = null;

    /** The UV coordinates to map the texture bitmap onto the triangle. Only valid if 'uniformColor' and 'vertexColors' are null. */

    private float uvCoordinates[] = null;

    /**
     * The groupId is set to 0, all three vertices are set to (0.0f,0.0f,0.0f), the uniform color is set to white.
     * @param id The id of the triangle.
     */

    public GLTriangleCV(String id) {
        this(id,0);
    }
    /**
     * All three vertices are set to (0.0f,0.0f,0.0f), the uniform color is set to white.
     * @param id The id of the triangle.
     * @param groupId The groupId of the triangle.
     */

    public GLTriangleCV(String id, int groupId) {
        this(id,groupId,new float[3][3],new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
    }


    /**
     * The groupID is set to 0, the uniform color is set to white.
     * @param id The id of the triangle.
     * @param vertices The vertices of the triangle (to be given in counter-clockwise order).
     */

    public GLTriangleCV(String id, float[][] vertices) {
        this(id,0,vertices,GraphicsUtilsCV.white);
    }

    /**
     * The groupId is set to 0.
     * @param id The id of the triangle.
     * @param vertices The vertices of the triangle (to be given in counter-clockwise order).
     * @param color The uniform color of the triangle as an array of length 4 with the RGBA color values.
     */

    public GLTriangleCV(String id, float[][] vertices, float[] color) {
        this(id,0,vertices,color);
    }

    /**
     * @param id The id of the triangle.
     * @param groupId The groupId of the triangle.
     * @param vertices The vertices of the triangle (to be given in counter-clockwise order).
     * @param color The uniform color of the triangle as an array of length 4 with the RGBA color values.
     */

    public GLTriangleCV(String id, int groupId, float[][] vertices, float[] color) {
        this.id = id;
        this.groupId = groupId;
        setVertices(vertices);
        setUniformColor(color);
    }

    /**
     * The groupId is set to 0, the uniform color is set to white.
     * @param id The id of the triangle.
     * @param vertex0 The first vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param vertex1 The second vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param vertex2 The third vertex of the triangle (vertices to be given in counter-clockwise order).
     */

    public GLTriangleCV(String id, float[] vertex0, float[] vertex1, float[] vertex2) {
        this(id,0,vertex0,vertex1,vertex2,GraphicsUtilsCV.white);
    }

    /**
     * The groupId is set to 0.
     * @param id The id of the triangle.
     * @param vertex0 The first vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param vertex1 The second vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param vertex2 The third vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param color The uniform color of the triangle as an array of length 4 with the RGBA color values.
     */

    public GLTriangleCV(String id, float[] vertex0, float[] vertex1, float[] vertex2, float[] color) {
        this(id,0,vertex0,vertex1,vertex2,color);
    }

    /**
     * @param id The id of the triangle.
     * @param groupId The groupId of the triangle.
     * @param vertex0 The first vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param vertex1 The second vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param vertex2 The third vertex of the triangle (vertices to be given in counter-clockwise order).
     * @param color The uniform color of the triangle as an array of length 4 with the RGBA color values.
     */

    public GLTriangleCV(String id, int groupId, float[] vertex0, float[] vertex1, float[] vertex2, float[] color) {
        this.id = new String(id);
        this.groupId = groupId;
        setVertices(vertex0,vertex1,vertex2);
        setUniformColor(color);
    }

    /**
     * The groupId is set to 0.
     * @param id The id of the triangle.
     * @param vertices The vertices of the triangle (to be given in counter-clockwise order).
     * @param colors The vertex colors of the triangle as an array of size 3*4 with the RGBA color values of the three vertices.
     */

    public GLTriangleCV(String id, float[][] vertices, float[][] colors) {
        this(id,0,vertices,colors);
    }

    /**
     * @param id The id of the triangle.
     * @param groupId The groupId of the triangle.
     * @param vertices The vertices of the triangle (to be given in counter-clockwise order).
     * @param colors The vertex colors of the triangle as an array of size 3*4 with the RGBA color values of the three vertices.
     */

    public GLTriangleCV(String id, int groupId, float[][] vertices, float[][] colors) {
        this.id = new String(id);
        this.groupId = groupId;
        setVertices(vertices);
        setVertexColors(colors);
    }

    /**
     * The groupId is set to 0.
     * @param id The id of the triangle.
     * @param vertices The vertices of the triangle (to be given in counter-clockwise order).
     * @param textureBitmap The bitmap to be used as texture.
     * @param uvCoordinates The uv coordinates to map the bitmap onto the triangle.
     */

    public GLTriangleCV(String id, float[][] vertices, Bitmap textureBitmap, float[] uvCoordinates) {
        this(id,0,vertices,textureBitmap,uvCoordinates);
    }

    /**
     * @param id The id of the triangle.
     * @param groupId The groupId of the triangle.
     * @param vertices The vertices of the triangle (to be given in counter-clockwise order).
     * @param textureBitmap The bitmap to be used as texture.
     * @param uvCoordinates The uv coordinates to map the bitmap onto the triangle.
     */

    public GLTriangleCV(String id, int groupId, float[][] vertices, Bitmap textureBitmap, float[] uvCoordinates) {
        this.id = id;
        this.groupId = groupId;
        setVertices(vertices);
        setTexture(textureBitmap,uvCoordinates);
    }

    /** Constructs a new triangle as a copy of this triangle with in-depth copies of all attributes.
     * @return The new triangle.
     */

    protected GLTriangleCV clone() {
        GLTriangleCV newTriangle = new GLTriangleCV(id,groupId);
        newTriangle.setVertices(this.vertices);
        newTriangle.setUniformColor(this.uniformColor);
        newTriangle.setVertexColors(this.vertexColors);
        newTriangle.setTexture(this.textureBitmap,this.uvCoordinates);
        return newTriangle;
    }

    public String getId() {
        return id;
    }

    /*
    public int getIdAsInt() {
        int idAsInt = -1;
        try {
           idAsInt = Integer.parseInt(id);
        } catch (NumberFormatException exc) {}
        return idAsInt;
    } */

    public void setId(String id) {
        this.id = id;
    }

    /* public void setIdFromInt(int idAsInt) {
        this.id = idAsInt+"";
    } */

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    /**
     * Sets the values of a triangle vertex as a copy of the parameter array.
     * @param vertexNo The number of the vertex to be set (0, 1, or 2).
     * @param values The values for the vertex (x, y, and z coordinate).
     * @return false if one of the parameters is not correct.
     */

    public boolean setVertex(int vertexNo, float[] values) {
        if (vertexNo<0||vertexNo>2||values==null||values.length!=3) return false;
        long start = System.nanoTime();
        for (int i=0; i<3; i++)
            this.vertices[vertexNo][i] = values[i];
        Log.v("GLDEMO","setVertex: "+(System.nanoTime()-start));
        return true;
    }

    /**
     * Sets the vertices of the triangle as an in-depth copy of the parameter array.
     * @param vertices The vertices to be set.
     * @return false if the parameter array has not the correct size (3 in both dimensions).
     */

    public boolean setVertices(float[][] vertices) {
        if (vertices==null||vertices.length!=3) return false;
        for (int i=0; i<3; i++)
            if (vertices[i].length!=3) return false;
        if (this.vertices==null)
            this.vertices = new float[3][3];
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
                this.vertices[i][j] = vertices[i][j];
        return true;
    }

    /**
     * Sets the vertices of the triangle as copies of the parameter array.
     * @param vertex0 The first vertex to be set.
     * @param vertex1 The second vertex to be set.
     * @param vertex2 The third vertex to be set.
     * @return false if one of the parameter arrays has not the correct length 3.
     */

    public boolean setVertices(float[] vertex0, float[] vertex1, float[] vertex2) {
        return setVertices(new float[][]{vertex0,vertex1,vertex2});
    }

    /**
     * Sets the vertices of the triangle from a sequence of float values
     * in the order vertex 0: x,y,z; vertex 1: x,y,z; vertex 2: x,y,z.
     * @param vertexCoordinates The vertices to be set - must be exactly nine float values.
     * @return false if the parameter array has not the correct length 9.
     */

    public boolean setVertices(float... vertexCoordinates) {
        if (vertexCoordinates.length!=9) return false;
        return setVertices(new float[][]{{vertexCoordinates[0],vertexCoordinates[1],vertexCoordinates[2]},{vertexCoordinates[3],vertexCoordinates[4],vertexCoordinates[5]},{vertexCoordinates[6],vertexCoordinates[7],vertexCoordinates[8]}});
    }

    /**
     * Get the vertices of the triangle.
     * @return The vertices of the triangle as an in-depth copy of the 'vertices' attribute.
     */

    public float[][] getVertices() {
        float verticesToReturn[][] = new float[3][];
        for (int i=0; i<3; i++)
          verticesToReturn[i] = vertices[i].clone();
        return verticesToReturn;
    }

    /**
     * Get the vertex coordinates of the triangle.
     * @return A one-dimensional float array of length 9
     * with the vertex coordinates in the order vertex 0: x,y,z; vertex 1: x,y,z; vertex 2: x,y,z.
     */

    public float[] getVertexCoordinates() {
        float coordinatesToReturn[] = new float[9];
        for (int vNumber=0; vNumber<3;vNumber++)
            for (int vCoord=0; vCoord<3; vCoord++)
                coordinatesToReturn[vNumber*3+vCoord] = vertices[vNumber][vCoord];
        return coordinatesToReturn;
    }

    /**
     * Get the edges of the triangle.
     * @return The edges as a three-dimensional float array r in this format:
     * <UL>
     * <LI>first dimension r[0][][], r[1][][], r[2][][]: the three edges
     * <LI>second dimension r[i][0][],r[i][1][]: the first and the second vertex of the i-th edge
     * <LI>third dimension r[i][j][0],r[i][j][1],r[i][j][2]: the x, y, and z coordinate of the j-th vertex of the i-th edge
     * </UL>
     * In the second dimension, the two vertices are ordered by their x, y, and z coordinates,
     * i.e. r[i][0][0]<r[i][1][0]
     * or (if r[i][0][0]==r[i][1][0]) r[i][0][1]<r[i][1][1])
     * or (if r[i][0][0]==r[i][1][0] and r[i][0][1]==r[i][1][1])) r[i][0][2]<r[i][1][2])
     */

    public float[][][] getEdges() {
        float edgesToReturn[][][] = new float[3][][];
        float vertices[][] = getVertices();
        edgesToReturn[0]=new float[][]{vertices[0],vertices[1]};
        edgesToReturn[1]=new float[][]{vertices[0],vertices[2]};
        edgesToReturn[2]=new float[][]{vertices[1],vertices[2]};
        for (int i=0;i<3;i++)
            if (edgesToReturn[i][0][0]>edgesToReturn[i][1][0]
                  || (edgesToReturn[i][0][0]==edgesToReturn[i][1][0]&&edgesToReturn[i][0][1]>edgesToReturn[i][1][1])
                  || (edgesToReturn[i][0][0]==edgesToReturn[i][1][0]&&edgesToReturn[i][0][1]==edgesToReturn[i][1][1]&&edgesToReturn[i][0][2]>edgesToReturn[i][1][2])) {
                float[] edgeTmp = edgesToReturn[i][0];
                edgesToReturn[i][0] = edgesToReturn[i][1];
                edgesToReturn[i][1] = edgeTmp;
            }
        return edgesToReturn;
    }

    /**
     * Inverts the orientation of the triangle by swapping vertices[1] and vertices[2].
     * To be applied e.g. to invert the orientation of the surface normal returned by getNormal().
     */

    public void invertOrientation() {
        float[] tmp = vertices[1];
        vertices[1] = vertices[2];
        vertices[2] = tmp;
    }

    /**
     * Get the surface normal of the triangle, i.e. the vector that is perpendicular to the triangle surface.
     * The orientation of the normal is determined by the order of the triangle vertices as specified by the vertices[] attribute.
     * It is determined by the "right-hand rule", i.e. it is the "thumb" of the right hand
     * where the vector (vertices[0],vertices[1]) is the forefinger and (vertices[0],vertices[2]) is the middle finger of the right hand.
     * The orientation can be inverted by calling invertOrientation() before calling getNormal().
     * @return The normal as an array of length 3 with its x, y, and z coordinates.
     */

    public float[] getNormal() {
        float[] vec1 = new float[3];
        float[] vec2 = new float[3];
        for (int j=0; j<3; j++) {
            vec1[j] = vertices[1][j]-vertices[0][j];
            vec2[j] = vertices[2][j]-vertices[0][j];
        }
        float[] normal = GraphicsUtilsCV.crossProduct3D(vec1,vec2);
        GraphicsUtilsCV.normalizeVector3D(normal);
        return normal;
    }

    /**
     * Get the coloring type of the triangle.
     * @return The coloring type of the triangle:
     * <UL>
     * <LI>GLPlatformCV.COLORING_UNIFORM if the triangle is uniformly colored, i.e. the 'uniformColor' attribute is not null.
     * <LI>GLPlatformCV.COLORING_GRADIENT if the triangle is colored with a color gradient, i.e. the 'uniformColor' attribute is null and the 'vertexColors' attribute is not null.
     * <LI>GLPlatformCV.COLORING_TEXTURE if the triangle has a texture, i.e. the 'uniformColor' and 'vertexColors' attributes are null and the 'textureBitmap' and 'uvCoordinates' attributes are not null.
     * <LI>GLPlatformCV.COLORING_UNDEF otherwise.
     * </UL>
     */

    public int getColoringType()  {     // coloring types as defined in GLPlatformCV
        if (uniformColor!=null) return GLPlatformCV.COLORING_UNIFORM;
        if (vertexColors!=null) return GLPlatformCV.COLORING_VARYING;
        if (textureBitmap!=null) return GLPlatformCV.COLORING_TEXTURED;
        return GLPlatformCV.COLORING_UNDEF;
    }

    /**
     * By calling this method the triangle will become uniformly colored with the color as given by the parameter (provided that the parameter has the correct format).
     * @param uniformColor A float array of length 4 with the RGBA color values.
     * @return true if the parameter has the correct format, false otherwise.
     */

    public boolean setUniformColor(float[] uniformColor) {
        if (!GraphicsUtilsCV.isValidColorArray(uniformColor)) return false;
        this.uniformColor = uniformColor.clone();
        return true;
    }

    /**
     * Get the uniform color of the triangle.
     * @return If the triangle is uniformly colored, the color of the triangle as an in-depth copy of the 'uniformColor' attribute. Null otherwise.
     */

    public float[] getUniformColor() {
        if (getColoringType()!= GLPlatformCV.COLORING_UNIFORM) return null;
        return uniformColor.clone();
    }

    /**
     * By calling this method the triangle will become colored with a colored gradient (provided that the parameter has the correct format).
     * In particular, the 'uniformColor' attribute is set to null.
     * @param vertexColors A two-dimensional 3*4 float array with the RGBA color values of the three vertices.
     * @return true if the parameter has the correct format, false otherwise.
     */

    public boolean setVertexColors(float[][] vertexColors) {
        if (vertexColors==null||vertexColors.length!=3) return false;
        for (int i=0; i<3; i++)
            if (vertexColors[i].length!=4) return false;
        uniformColor = null;
        this.vertexColors = new float[3][4];
        for (int i=0; i<3; i++)
            for (int j=0; j<4; j++)
                this.vertexColors[i][j] = vertexColors[i][j];
        return true;
    }

    /**
     * Get the color of a vertex.
     * @param vertexNo The number of the vertex.
     * @return null if vertexNo is not 0, 1, or 2 or/and the triangle has neither a uniform or a gradient color.
     * The color of the vertex if the triangle has a gradient color; the uniform color of the triangle if the triangle is uniformly colored (both as copies of the attribute values).
     */

    public float[] getVertexColor(int vertexNo) {
        if (vertexNo<0||vertexNo>2) return null;
        if (getColoringType()!=GLPlatformCV.COLORING_UNIFORM&&getColoringType()!=GLPlatformCV.COLORING_VARYING) return null;
        if (getColoringType()==GLPlatformCV.COLORING_UNIFORM)
            return uniformColor.clone();
        return vertexColors[vertexNo].clone();
    }

    /**
     * By calling this method the triangle will become textured with a bitmap (provided that the parameters have the correct format).
     * In particular, the 'uniformColor' and 'vertexColor' attributes are set to null.
     * @param textureBitmap The bitmap to be used as texture.
     * @param uvCoordinates A float array of length 6 with the uvCoordinates to be used for the mapping (in the ordering of the corresponding vertices).
     * @return true if both parameters are not 0 and the array parameter has the correct length, false otherwise.
     */

    public boolean setTexture(Bitmap textureBitmap, float uvCoordinates[]) {
        if (textureBitmap==null||uvCoordinates==null||uvCoordinates.length!=6) return false;
        uniformColor = null;
        vertexColors = null;
        this.textureBitmap = textureBitmap;
        this.uvCoordinates = new float[6];
        for (int i=0; i<6; i++)
            this.uvCoordinates[i] = uvCoordinates[i];
        return true;
    }

    /**
     * Get the uvCoordinates of the triangle if defined.
     * @return A copy of the 'uvCoordinates' array attribute if the triangle is textured, null otherwise.
     */

    public float[] getUvCoordinates() {
        if (getColoringType()!= GLPlatformCV.COLORING_TEXTURED) return null;
        return uvCoordinates.clone();
    }

    /**
     * Get the texture bitmap of the triangle if defined.
     * @return The texture bitmap if the triangle is textured , null otherwise.
     */

    public Bitmap getTexture() {
        if (getColoringType()!= GLPlatformCV.COLORING_TEXTURED) return null;
        return textureBitmap;
    }

    /**
     * Scale the triangle.
     * <BR>
     * For a more detailed comment see method 'transform()'.
     * @param scaleX The scale factor for the x dimension.
     * @param scaleY The scale factor for the y dimension.
     * @param scaleZ The scale factor for the z dimension.
     */

    public void scale(float scaleX, float scaleY, float scaleZ) {
        transform(scaleX,scaleY,scaleZ,0,0,0,0,0,0);
    }

    /**
     * Rotate the triangle.
     * <BR>
     * For a more detailed comment see method 'transform()'.
     * @param rotAngleX The rotation angle to be applied - x axis.
     * @param rotAngleY The rotation angle to be applied - y axis.
     * @param rotAngleZ The rotation angle to be applied - z axis.
     */

    public void rotate(float rotAngleX, float rotAngleY, float rotAngleZ) {
        transform(1,1,1,rotAngleX,rotAngleY,rotAngleZ,0,0,0);
    }

    /**
     * Translate the triangle.
     * <BR>
     * For a more detailed comment see method 'transform()'.
     * @param transX The translation in the x direction.
     * @param transY The translation in the y direction.
     * @param transZ The translation in the z direction.
     */

    public void translate(float transX, float transY, float transZ) {
        if (transX!=0)
            for (int i=0;i<3;i++)
                vertices[i][0] += transX;
        if (transY!=0)
            for (int i=0;i<3;i++)
                vertices[i][1] += transY;
        if (transZ!=0)
            for (int i=0;i<3;i++)
                vertices[i][2] += transZ;
        // Alternatively: transform(1,1,1,0,0,0,transX,transY,transZ);
    }

    /**
     * Scale, rotate, and/or translate the triangle.
     * <BR>
     * N.B.: This method will affect the coordinates of the triangle in the model (!) coordinate system,
     * i.e. in the coordinate system of the shape to which the triangle belongs or is to be added.
     * By this, a triangle can be placed within this shape.
     * Similar methods of shapes (see class GLShapeCV: setTransX(), setRotAroundX(), ...)
     * do not modify the vertex coordinates of the shape's triangles and lines
     * but define a model matrix which places the whole shape into the "real world".
     * @param scaleX The scale factor for the x dimension.
     * @param scaleY The scale factor for the y dimension.
     * @param scaleZ The scale factor for the z dimension.
     * @param rotAngleX The rotation angle around the x axis.
     * @param rotAngleY The rotation angle around the y axis.
     * @param rotAngleZ The rotation angle around the z axis.
     * @param transX The translation in the x direction.
     * @param transY The translation in the y direction.
     * @param transZ The translation in the z direction.
     */

    public void transform(float scaleX, float scaleY, float scaleZ, float rotAngleX ,float rotAngleY ,float rotAngleZ, float transX, float transY, float transZ) {
        if (scaleX==1&&scaleY==1&&scaleZ==1&&rotAngleX==0&&rotAngleY==0&&rotAngleZ==0&&transX==0&&transY==0&&transZ==0) return;
        float[] transformationMatrix = new float[16];
        Matrix.setIdentityM(transformationMatrix,0);
        // scaling matrix
        float[] matrixScale = new float[16];
        Matrix.setIdentityM(matrixScale,0);
        Matrix.scaleM(matrixScale,0,scaleX, scaleY, scaleZ);
        // rotation matrix
        float[] matrixRotateX = new float[16];
        Matrix.setIdentityM(matrixRotateX,0);
        Matrix.rotateM(matrixRotateX,0,rotAngleX,1,0,0);
        float[] matrixRotateY = new float[16];
        Matrix.setIdentityM(matrixRotateY,0);
        Matrix.rotateM(matrixRotateY,0,rotAngleY,0,1,0);
        float[] matrixRotateZ = new float[16];
        Matrix.setIdentityM(matrixRotateZ,0);
        Matrix.rotateM(matrixRotateZ,0,rotAngleZ,0,0,1);
        float[] matrixRotate = new float[16];
        Matrix.setIdentityM(matrixRotate,0);
        if (rotAngleY!=0)
            Matrix.multiplyMM(matrixRotate, 0, matrixRotateY, 0, matrixRotate, 0);
        if (rotAngleZ!=0)
            Matrix.multiplyMM(matrixRotate, 0, matrixRotateZ, 0, matrixRotate, 0);
        if (rotAngleX!=0)
            Matrix.multiplyMM(matrixRotate, 0, matrixRotateX, 0, matrixRotate, 0);
        // translation matrix
        float[] matrixTranslate = new float[16];
        Matrix.setIdentityM(matrixTranslate,0);
        // Log.v("GLDEMO","Triangle Translate "+transX+" "+transY+" "+transZ);
        Matrix.translateM(matrixTranslate,0,transX,transY,transZ);
        transform(matrixScale,matrixRotate,matrixTranslate);
        /*
        // calculate the new model matrix by multiplying the scaling, rotation, and translation matrices (in this order!)
        Matrix.multiplyMM(transformationMatrix, 0, matrixScale, 0, transformationMatrix, 0);
        Matrix.multiplyMM(transformationMatrix, 0, matrixTranslate, 0, transformationMatrix, 0);
        for (int i=0; i<3; i++) {
            float[] vertex = new float[4];
            for (int j=0; j<3; j++)
                vertex[j] = this.vertices[i][j];
            vertex[3] = 1;
            Matrix.multiplyMV(vertex, 0, transformationMatrix, 0, vertex, 0);
            for (int j=0; j<3; j++)
                this.vertices[i][j] = vertex[j];
        }

         */
    }

    /**
     * Scale, rotate, and/or translate the triangle.
     * <BR>
     * N.B.: This method will affect the coordinates of the triangle in the model (!) coordinate system,
     * i.e. in the coordinate system of the shape to which the triangle belongs or is to be added.
     * By this, a triangle can be placed within this shape.
     * Similar methods of shapes (see class GLShapeCV: setTransX(), setRotAroundX(), ...)
     * do not modify the vertex coordinates of the shape's triangles and lines
     * but define a model matrix which places the whole shape into the "real world".
     * @param matrixScale The scaling matrix (a float array of length 16, as required by OpenGL).
     * @param matrixRotate The rotation matrix (a float array of length 16, as required by OpenGL).
     * @param matrixTranslate The translation matrix (a float array of length 16, as required by OpenGL).
     */

    public void transform(float[] matrixScale, float[] matrixRotate, float[] matrixTranslate) {
        if (matrixScale==null||matrixScale.length!=16||matrixRotate==null||matrixRotate.length!=16||matrixTranslate==null||matrixTranslate.length!=16) return;
        float[] transformationMatrix = new float[16];
        Matrix.setIdentityM(transformationMatrix,0);
        String logcatOutput = "";
        for (int i=0;i<16;i++)
                logcatOutput += matrixTranslate[i] + " ";
        // Log.v("GLDEMO","Translation: "+logcatOutput);
        // calculate the model matrix by multiplying the scaling, rotation, and translation matrices (in this order!)
        Matrix.multiplyMM(transformationMatrix, 0, matrixScale, 0, transformationMatrix, 0);
        Matrix.multiplyMM(transformationMatrix, 0, matrixRotate, 0, transformationMatrix, 0);
        Matrix.multiplyMM(transformationMatrix, 0, matrixTranslate, 0, transformationMatrix, 0);
        // logcatOutput = "";
        // for (int i=0;i<16;i++)
        //    logcatOutput += transformationMatrix[i] + " ";
        // Log.v("GLDEMO","Transformation: "+logcatOutput);
        for (int i=0; i<3; i++) {
            // Log.v("GLDEMO","Transform before "+this.vertices[i][0]+" "+this.vertices[i][1]+" "+this.vertices[i][2]);
            float[] vertex = new float[4];
            for (int j=0; j<3; j++)
                vertex[j] = this.vertices[i][j];
            vertex[3] = 1;
            Matrix.multiplyMV(vertex, 0, transformationMatrix, 0, vertex, 0);
            for (int j=0; j<3; j++)
                this.vertices[i][j] = vertex[j];
            // Log.v("GLDEMO","Transform after "+this.vertices[i][0]+" "+this.vertices[i][1]+" "+this.vertices[i][2]);
        }
    }

    /**
     * Flips/mirrors the triangle in the x, y, and/or z dimension
     * by changing the signs of all its corresponding coordinate values.
     * <BR>
     * N.B.: This method will affect the coordinates of the triangle in the model (!) coordinate system,
     * i.e. in the coordinate system of the shape to which the triangle belongs or is to be added.
     * Similar methods of shapes (see class GLShapeCV: setTransX(), setRotAroundX(), ...)
     * do not modify the vertex coordinates of the shape's triangles and lines
     * but define a model matrix which places the whole shape into the "real world".
     * <BR>
     * N.B.: This will change the orientation of the triangle. To set the original orientation, call changeOrientation() afterwards.
     * @param flipX Specifies if the triangle shall be flipped in the x dimension.
     * @param flipY Specifies if the triangle shall be flipped in the y dimension.
     * @param flipZ Specifies if the triangle shall be flipped in the z dimension.
     */

    synchronized public void flip(boolean flipX, boolean flipY, boolean flipZ) {
        for (int i=0; i<3; i++) {
            if (flipX)
                this.vertices[i][0] = -this.vertices[i][0];
            if (flipY)
                this.vertices[i][1] = -this.vertices[i][1];
            if (flipZ)
                this.vertices[i][2] = -this.vertices[i][2];
        }
    }

    /**
     * Changes the orientation of the triangle by swapping the second and the third vertex.
     */

    synchronized public void changeOrientation() {
        float[] temp = this.vertices[1];
        vertices[1] = vertices[2];
        vertices[2] = temp;
    }

    /**
     * Method to normalize the vertices of all triangles,
     * i.e. to lengthen or shorten all vectors leading from the origin to the vertex to length 1.
     * Note that this is a very special method that is to be applied when constructing a sphere
     * (see method makeSphere() in class GLShapeFactory).
     */

    public void normalizeVertexVectors() {
      for (int i=0; i<3; i++)
          GraphicsUtilsCV.normalizeVector3D(vertices[i]);
    }

    /* old version

    public void transform(float scaleX, float scaleY, float scaleZ, float rotAngle ,float rotX,float rotY,float rotZ, float transX, float transY, float transZ) {
        float[] transformationMatrix = new float[16];
        Matrix.setIdentityM(transformationMatrix,0);
        // scaling matrix
        float[] matrixScale = new float[16];
        Matrix.setIdentityM(matrixScale,0);
        Matrix.scaleM(matrixScale,0,scaleX, scaleY, scaleZ);
        // rotation matrix
        float[] matrixRotate = new float[16];
        Matrix.setIdentityM(matrixRotate,0);
        Matrix.rotateM(matrixRotate,0,rotAngle,rotX,rotY,rotZ);  // Rotation um die Achse (x, y, z) mit Winkel a Grad (Gradmaß).
        // translation matrix
        float[] matrixTranslate = new float[16];
        Matrix.setIdentityM(matrixTranslate,0);
        Matrix.translateM(matrixTranslate,0,transX,transY,transZ);
        // calculate the model matrix by multiplying the scaling, rotation, and translation matrices (in this order!)
        Matrix.multiplyMM(transformationMatrix, 0, matrixScale, 0, transformationMatrix, 0);
        if (rotX!=0||rotY!=0||rotZ!=0)
            Matrix.multiplyMM(transformationMatrix, 0, matrixRotate, 0, transformationMatrix, 0);
        Matrix.multiplyMM(transformationMatrix, 0, matrixTranslate, 0, transformationMatrix, 0);
        for (int i=0; i<3; i++) {
            float[] vertex = new float[4];
            for (int j=0; j<3; j++)
                vertex[j] = this.vertices[i][j];
            vertex[3] = 1;
            Matrix.multiplyMV(vertex, 0, transformationMatrix, 0, vertex, 0);
            for (int j=0; j<3; j++)
                this.vertices[i][j] = vertex[j];
        }
    }
  */

}

