// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 17.11.2022

package de.thkoeln.cvogt.android.opengl_utilities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

/**
 * EXPERIMENTAL - DOES NOT WORK TOGETHER WITH AnimatorSets:
 * playTogether() with animators of this class as parameters does not show anything
 * and crashes in the end with "Error: animation ended is not in the node map".
 * ----
 * Class for objects by which objects of class GLShapeCV can be animated.
 * <P>
 * An object of this class wraps an object of either the Android class ObjectAnimator // or the class AnimatorSet.
 * ObjectAnimators are based on the "property animation" technique by repeatedly calling setXXX() methods of the shape.
 * The animator object affects the model matrix of the shape, i.e. the placement of the shape in the world by scaling, rotating, and translating it.
 * Animators that work in the model coordinate space, i.e. modify the form of the shape ("morph" the shape) can be implemented through the control thread of the shape.
 * <P>
 * The animator will be started automatically when the shape is added to a surface view, i.e. an object of class <I>GLSurfaceViewCV</I>.
 * The animator will then be controlled by the implicit thread of the surface view.
 */

public class GLAnimatorCV extends Animator {

    private Animator animator;

    public GLAnimatorCV(Animator animator) {
        super();
        this.animator = animator;
    }

    @Override
    public void setTarget(Object shape) {
        super.setTarget(shape);
        animator.setTarget(shape);
    }

    @Override
    public void setStartDelay(long startDelay) {
        animator.setStartDelay(startDelay);
    }

    @Override
    public GLAnimatorCV setDuration(long duration) {
        animator.setDuration(duration);
        return this;
    }

    @Override
    public long getDuration() {
        return animator.getDuration();
    }

    @Override
    public void setInterpolator(TimeInterpolator value) {
        animator.setInterpolator(value);
    }

    @Override
    public boolean isRunning() {
        return animator.isRunning();
    }

    @Override
    public void start() {
        super.start();
        animator.start();
    }

    @Override
    public void end() {
        super.end();
        animator.end();
    }

    @Override
    public long getStartDelay() {
        return animator.getStartDelay();
    }

    @Override
    public void cancel() {
        super.cancel();
        animator.cancel();
    }

    @Override
    public void pause() {
        super.pause();
        animator.pause();
    }

    @Override
    public void resume() {
        super.resume();
        animator.resume();
    }

    @Override
    public boolean isPaused() {
        return animator.isPaused();
    }

    @Override
    public long getTotalDuration() {
        return animator.getTotalDuration();
    }

    @Override
    public TimeInterpolator getInterpolator() {
        return animator.getInterpolator();
    }

    @Override
    public boolean isStarted() {
        return animator.isStarted();
    }

    @Override
    public void addListener(AnimatorListener listener) {
        super.addListener(listener);
        animator.addListener(listener);
    }

    @Override
    public void removeListener(AnimatorListener listener) {
        super.removeListener(listener);
        animator.removeListener(listener);
    }

    @Override
    public ArrayList<AnimatorListener> getListeners() {
        return animator.getListeners();
    }

    @Override
    public void addPauseListener(AnimatorPauseListener listener) {
        super.addPauseListener(listener);
        animator.addPauseListener(listener);
    }

    @Override
    public void removePauseListener(AnimatorPauseListener listener) {
        super.removePauseListener(listener);
        animator.removePauseListener(listener);
    }

    @Override
    public void removeAllListeners() {
        animator.removeAllListeners();
    }

    @Override
    public void setupStartValues() {
        super.setupStartValues();
        animator.setupStartValues();
    }

    @Override
    public void setupEndValues() {
        super.setupEndValues();
        animator.setupEndValues();
    }
}

/*

   In GLShapeCV:

    private ArrayList<GLAnimatorCV> animatorsCV;

    animatorsCV = new ArrayList<GLAnimatorCV>();

    synchronized public void addAnimatorCV(GLAnimatorCV animator) {
        animator.setTarget(this);
        animatorsCV.add(animator);
    }

    synchronized public void startAnimators() {
        if (animatorsCV!=null&&!animatorsCV.isEmpty())
            for (GLAnimatorCV animator : animatorsCV)
                animator.start();
    }

    public void stopControlThreadAndAnimators() {
        if (controlThread!=null) {
            controlThread.interrupt();
        }
        if (animatorsCV!=null)
            for (GLAnimatorCV animator : animatorsCV)
                animator.end();
    }

 */


