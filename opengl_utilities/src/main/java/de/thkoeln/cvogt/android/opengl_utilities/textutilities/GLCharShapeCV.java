// This work is provided under GPLv3, the GNU General Public License 3
//   http://www.gnu.org/licenses/gpl-3.0.html

// Prof. Dr. Carsten Vogt
// Technische Hochschule Köln, Germany
// Fakultät für Informations-, Medien- und Elektrotechnik
// carsten.vogt@th-koeln.de
// 5.6.2024

package de.thkoeln.cvogt.android.opengl_utilities.textutilities;

import android.content.Context;
import android.util.Log;

import de.thkoeln.cvogt.android.opengl_utilities.GLShapeCV;
import de.thkoeln.cvogt.android.opengl_utilities.GraphicsUtilsCV;

/**
 * A subclass of class GLShapeCV to define shapes that show a single character. The character shapes
 * <UL>
 * <LI>may be two- or three-dimensional,
 * <LI>may be solid (i.e. with colored faces) or a wireframe and
 * <LI>may have lines bordering all its triangles, bordering its faces, or no lines.
 * </UL>
 * @see GLShapeCV
 * @see GLTextUtilsCV
 */

public class GLCharShapeCV extends GLShapeCV {

    /**
     * A combination of flags (see the corresponding constants defined in class GLTextUtilsCV) describing the properties of the shape.
     * @see GLTextUtilsCV#TwoD
     * @see GLTextUtilsCV#ThreeD
     * @see GLTextUtilsCV#Solid
     * @see GLTextUtilsCV#Wireframe
     * @see GLTextUtilsCV#FaceLines
     * @see GLTextUtilsCV#TriangleLines
     */

    private int propertyFlags;

    /**
     * The character displayed by the shape
     */

    private char character;

    /**
     * The face color of the shape (only valid if the shape has faces, i.e. is not a wireframe).
     */

    private float[] faceColor = null;

    /**
     * The line color of the shape (only valid if the shape has lines).
     */

    private float[] lineColor = null;

    /**
     * Constructor to create a shape in the form of a letter, digit or some special character.
     * The character shape will be two-dimensional and have a white face without lines.
     * For more information, see the comments on the constructor with seven parameters.
     * @param id The ID of the new shape.
     * @param character The character to be displayed. If not supported, a character shape for an underscore _ will be created.
     * @param context The context in which this method is called (can be null if the hash map entries in GLTextUtilsCV have already been created).
     * @see GLCharShapeCV#GLCharShapeCV(String, char, float[], float[], float, int,Context)
     **/

    public GLCharShapeCV(String id, char character, Context context) {
        this(id,character,GraphicsUtilsCV.white,context);
    }

    /**
     * Constructor to create a shape in the form of a letter, digit or some special character.
     * The character shape will be two-dimensional and have a colored face without lines.
     * For more information, see the comments on the constructor with seven parameters.
     * @param id The ID of the new shape.
     * @param character The character to be displayed. If not supported, a character shape for an underscore _ will be created.
     * @param faceColor The face color of the shape. If null or not a valid color value, a white face color will be assumed.
     * @param context The context in which this method is called (can be null if the hash map entries in GLTextUtilsCV have already been created).
     * @see GLCharShapeCV#GLCharShapeCV(String, char, float[], float[], float, int,Context)
     **/

    public GLCharShapeCV(String id, char character, float[] faceColor, Context context) {
        this(id,character,faceColor,null,0,GLTextUtilsCV.TwoD|GLTextUtilsCV.Solid,context);
    }

    /**
     * Constructor to create a shape in the form of a letter, digit or some special character.
     * The character shape will be three-dimensional, have colored faces and colored face lines.
     * For more information, see the comments on the constructor with seven parameters.
     * @param id The ID of the new shape.
     * @param character The character to be displayed. If not supported, a character shape for an underscore _ will be created.
     * @param faceColor The face color of the shape. If null or not a valid color value, a white face color will be assumed.
     * @param lineColor The line color of the shape. If null or not a valid color value, a black line color will be assumed.
     * @param lineWidth The line width of the shape.
     * @param context The context in which this method is called (can be null if the hash map entries in GLTextUtilsCV have already been created).
     * @see GLCharShapeCV#GLCharShapeCV(String, char, float[], float[], float, int,Context)
     **/

    public GLCharShapeCV(String id, char character, float[] faceColor, float[] lineColor, float lineWidth, Context context) {
        this(id,character,faceColor,lineColor,lineWidth,GLTextUtilsCV.ThreeD|GLTextUtilsCV.Solid|GLTextUtilsCV.FaceLines,context);
    }

    /**
     * Constructor to create a shape in the form of a letter, digit or some special character.
     * <P>
     * Currently these characters are supported:
     * Letters (lower or upper case, including umlaut and ß), digits,
     * or one of the special characters ( ) [ ] { } < > , ; . : ? ! @ # $ + - * / = _ " € or an empty space (a "blank")
     * If the character requested is not supported, an underscore _ will be assumed.
     * <P>
     * The size of the shape is derived from the size of the shape for the character 'X' which will have a height of 1.0, i.e. size of 1.0 in the y dimension.
     * The widths and heights of other characters will be set accordingly.
     * <P>
     * The origin of the shape (i.e. point (0,0,0) in its model space) will be set in the center of all its triangles and lines.
     * Hence when aligning multiple character shapes to form a word, the y positions of the shapes need to be adapted accordingly
     * - see utility method charShapeRelativeYPos() for this.
     * <P>
     * The looks of the shape (2D or 3D, solid or wireframe, lines at the borders of the faces or the triangles or not, colors of the faces and the lines) are controlled by parameters.
     * @param id The ID of the new shape.
     * @param character The character to be displayed
     * @param faceColor The face color of the shape. May be null if the shape is a wireframe. If needed but not provided, a white face color will be assumed.
     * @param lineColor The line color of the shape. May be null if the shape has only faces but no lines. If needed but not provided, a black line color will be assumed.
     * @param lineWidth The line width of the shape. Irrelevant if the shape has only faces but no lines.
     * @param flags Flags specifying the looks of the shape (as defined by static constants of the class GLTextUtilsCV). The flags form three groups:
     *              <UL>
     *              <LI>ThreeD, TwoD: three-dimensional or two-dimensional (i.e. flat) character shape. If both flags are set or none, ThreeD will be assumed.
     *              <LI>Solid, Wireframe: solid (i.e. with colored faces) or wireframe character shape. If both flags are set or none, Solid will be assumed.
     *              <LI>FaceLines, TriangleLines: The lines to be shown - only the border lines of the faces, the border lines of all triangles, or no lines at all.
     *                  If both flags are set, FaceLines will be assumed.
     *                  If both flags are not set, for a wireframe shape FaceLines will be assumed, and for a solid shape no lines will be drawn.
     *              </UL>
     *              Hence, the value 0 for this parameter will produce a 3D shape with solid colored faces and no lines.
     * @param context The context in which this method is called (can be null if the hash map entries in GLTextUtilsCV have already been created).
     **/

    public GLCharShapeCV(String id, char character, float[] faceColor, float[] lineColor, float lineWidth, int flags, Context context) {
        // create a temporarily empty GLShapeCV object
        super(id, null);
        // set the 'character' attribute
        this.character = character;
        // analyze the 'flags' parameter and set the 'propertyFlags' attribute
        boolean threeD = ((flags & GLTextUtilsCV.ThreeD) != 0) || ((flags & GLTextUtilsCV.ThreeD) == 0 && (flags & GLTextUtilsCV.TwoD) == 0);
        boolean solid = ((flags & GLTextUtilsCV.Solid) != 0) || ((flags & GLTextUtilsCV.Solid) == 0 && (flags & GLTextUtilsCV.Wireframe) == 0);
        boolean faceLines = ((flags & GLTextUtilsCV.FaceLines) != 0) || ((flags & GLTextUtilsCV.FaceLines) == 0 && (flags & GLTextUtilsCV.TriangleLines) == 0 && !solid);
        boolean triangleLines = !faceLines && ((flags & GLTextUtilsCV.TriangleLines) != 0);
        if (threeD)
            this.propertyFlags = GLTextUtilsCV.ThreeD;
        else
            this.propertyFlags = GLTextUtilsCV.TwoD;
        if (solid)
            this.propertyFlags |= GLTextUtilsCV.Solid;
        else
            this.propertyFlags |= GLTextUtilsCV.Wireframe;
        if (faceLines&&character!=' ') this.propertyFlags |= GLTextUtilsCV.FaceLines;
        if (triangleLines&&character!=' ') this.propertyFlags |= GLTextUtilsCV.TriangleLines;
        // set color attributes
        if (isSolid() && !GraphicsUtilsCV.isValidColorArray(faceColor))
            this.faceColor = GraphicsUtilsCV.white;
        else
            this.faceColor = faceColor.clone();
        if (hasLines())
            if (!GraphicsUtilsCV.isValidColorArray(lineColor))
                this.lineColor = GraphicsUtilsCV.black;
            else
                this.lineColor = lineColor.clone();
        if (!GLTextUtilsCV.supportedCharacters.contains(character+""))
            character = '_';
        GLTextUtilsCV.initHashMaps(context);
        // set the triangles and lines from the hash maps in GLTextUtilsCV
        if (isThreeD()) {
            if (solid)
                addTriangles(GLTextUtilsCV.chars3D_Triangles.get(character));
            if (hasFaceLines())
                addLines(GLTextUtilsCV.chars3D_FaceLines.get(character));
            if (hasTriangleLines())
                addLines(GLTextUtilsCV.chars3D_TriangleLines.get(character));
        } else {
            if (solid)
                addTriangles(GLTextUtilsCV.chars2D_Triangles.get(character));
            if (hasFaceLines())
                addLines(GLTextUtilsCV.chars2D_FaceLines.get(character));
            if (hasTriangleLines())
                addLines(GLTextUtilsCV.chars2D_TriangleLines.get(character));
        }
        if (triangles!=null)
            setTrianglesUniformColor(this.faceColor);
        if (lines!=null) {
            this.lineWidth = lineWidth;
            setLinesUniformColor(this.lineColor);
        }
    }

    /**
     * Makes a deep copy of this shape.
     * @param id The id of the new shape.
     * @param context The context from which this method is called (can be null if the hash map entries in GLTextUtilsCV have already been created).
     * @return A reference to the new shape.
     */

    synchronized public GLCharShapeCV copy(String id, Context context) {
        GLCharShapeCV newShape = new GLCharShapeCV(id,character,faceColor,lineColor,lineWidth,propertyFlags,context);
        newShape.setInfoBundle(info);
        return newShape;
    }

    /**
     * @return true if the shape is three-dimensional, false if it is two-dimensional
     */

    public boolean isThreeD() {
        return (propertyFlags&GLTextUtilsCV.ThreeD)!=0;
    }

    /**
     * @return true if the shape is solid (i.e. has colored faces, false if it is a wireframe
     */

    public boolean isSolid() {
        return (propertyFlags&GLTextUtilsCV.Solid)!=0;
    }

    /**
     * @return true if the shape has face lines, false otherwise
     */

    public boolean hasFaceLines() {
        return (propertyFlags&GLTextUtilsCV.FaceLines)!=0;
    }

    /**
     * @return true if the shape has triangle lines, false otherwise
     */

    public boolean hasTriangleLines() {
        return (propertyFlags&GLTextUtilsCV.TriangleLines)!=0;
    }

    /**
     * @return true if the shape has lines, false otherwise
     */

    public boolean hasLines() {
        return hasFaceLines()||hasTriangleLines();
    }

    /**
     * @return the value of the character attribute
     */

    public char getCharacter() {
        return this.character;
    }

    /**
     * @return the value of the propertyFlags attribute
     */

    public int getPropertyFlags() {
        return this.propertyFlags;
    }

    /**
     * @return a copy of the face color attribute (or null)
     */

    public float[] getFaceColor() {
        if (faceColor!=null)
            return faceColor.clone();
        else return null;
    }

    /**
     * @return a copy of the line color attribute (or null)
     */

    public float[] getLineColor() {
        if (lineColor!=null)
            return lineColor.clone();
        else return null;
    }

}