The Twins Game
Carsten Vogt, Technische Hochschule Köln, Germany
Release 1.0, June 2024

This gaming app is based on the OpenGL technique for displaying and animating 3D objects.
The objective of the game is to find and touch pairs of identical objects as fast as possible.