package de.thkoeln.cvogt.android.twins

/*

The Twins Game - Auxiliary class providing the app with a number of color combinations.
Carsten Vogt, Technische Hochschule Köln, Germany
Release 1.0, June 2024

This work is provided under GPLv3, the GNU General Public License 3
http://www.gnu.org/licenses/gpl-3.0.html

*/

class ColorProvider(val colors: Array<FloatArray>) {

    val randomColorIndex: Int
        get() = (Math.random() * colors.size).toInt()

    val randomColor: FloatArray
        get() = colors[randomColorIndex]

    fun getRandomColorIndices(numberOfColorIndices: Int, allDifferent: Boolean): IntArray? {
        val result = IntArray(numberOfColorIndices)
        if (allDifferent) {
            if (numberOfColorIndices > colors.size) return null
            val colorIndices = mutableListOf<Int>()
            for (i in colors.indices) colorIndices.add(i)
            colorIndices.shuffle()
            for (i in 0 until numberOfColorIndices) result[i] = colorIndices.get(i)
        } else for (i in 0 until numberOfColorIndices) result[i] = randomColorIndex
        return result
    }

    fun getRandomColors(numberOfColors: Int, allDifferent: Boolean): Array<FloatArray?>? {
        val indices = getRandomColorIndices(numberOfColors, allDifferent)
        if (indices==null) return null
        val result = arrayOfNulls<FloatArray>(numberOfColors)
        for (i in 0 until numberOfColors) result[i] = colors[indices[i]]
        return result
    }

    fun getRandomColorCombinations(
        numberOfColorCombinations: Int,
        numberOfColorsInCombination: Int,
        allDifferent: Boolean
    ): Array<Array<FloatArray?>> {
        val result = Array(numberOfColorCombinations) { arrayOfNulls<FloatArray>(numberOfColorsInCombination) }
        if (allDifferent) {
            val existingCombinations = HashSet<HashSet<Int>>()
            for (i in 0 until numberOfColorCombinations) do {
                val colorCombinationIndices =
                    getRandomColorIndices(numberOfColorsInCombination, true)
                if (!existingCombinations.contains(colorCombinationIndices?.toHashSet())) {
                    if (colorCombinationIndices != null) {
                        var j = 0;
                        for (colorIndex in colorCombinationIndices) {
                            result[i][j] = colors[colorCombinationIndices[j]]
                            j++
                        }
                        existingCombinations.add(colorCombinationIndices.toHashSet())
                    }
                    // Log.v("GLDEMO",colorCombinationIndices[0]+" "+colorCombinationIndices[1]+" "+colorCombinationIndices[2]+" "+colorCombinationIndices[3]);
                    break
                }
            } while (true)
        } else for (i in 0 until numberOfColorCombinations)
            result[i] = getRandomColors(numberOfColorsInCombination, true)!!
        return result
    }

    /*
    fun getRandomColorCombinations(
        numberOfColorCombinations: Int,
        numberOfColorsInCombination: Int,
        allDifferent: Boolean
    ): Array<Array<FloatArray?>> {
        val result = Array(numberOfColorCombinations) { arrayOfNulls<FloatArray>(numberOfColorsInCombination) }
        if (allDifferent) {
            val existingCombinationsHashes = ArrayList<Long>()
            for (i in 0 until numberOfColorCombinations) do {
                val colorCombinationIndices =
                    getRandomColorIndices(numberOfColorsInCombination, true)
                var hash: Long = 0
                for (j in colorCombinationIndices!!.indices)
                    hash += colorCombinationIndices[j] * Math.pow(colors.size.toDouble(), j.toDouble()).toLong()
                if (!existingCombinationsHashes.contains(hash)) {
                    for (j in 0 until numberOfColorsInCombination)
                        result[i][j] = colors[colorCombinationIndices[j]]
                    // Log.v("GLDEMO",colorCombinationIndices[0]+" "+colorCombinationIndices[1]+" "+colorCombinationIndices[2]+" "+colorCombinationIndices[3]);
                    existingCombinationsHashes.add(hash)
                    break
                }
            } while (true)
        } else for (i in 0 until numberOfColorCombinations)
            result[i] = getRandomColors(numberOfColorsInCombination, true)
        return result
    }
    */

}