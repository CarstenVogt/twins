package de.thkoeln.cvogt.android.twins

/*

The Twins Game - touch pairs of identical objects
Carsten Vogt, Technische Hochschule Köln, Germany
Release 1.0, June 2024

This work is provided under GPLv3, the GNU General Public License 3
http://www.gnu.org/licenses/gpl-3.0.html

*/

// GitLab integration explained at https://www.youtube.com/watch?v=_Gw9MLKjLYQ

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import de.thkoeln.cvogt.android.opengl_utilities.GLAnimatorFactoryCV
import de.thkoeln.cvogt.android.opengl_utilities.GLRendererCV
import de.thkoeln.cvogt.android.opengl_utilities.GLShapeCV
import de.thkoeln.cvogt.android.opengl_utilities.GLShapeFactoryCV
import de.thkoeln.cvogt.android.opengl_utilities.GLSurfaceViewCV
import de.thkoeln.cvogt.android.opengl_utilities.GLSurfaceViewCV.GLOnTouchListenerCV
import de.thkoeln.cvogt.android.opengl_utilities.GLTouchEventCV
import de.thkoeln.cvogt.android.opengl_utilities.GLUtilsCV
import de.thkoeln.cvogt.android.opengl_utilities.GraphicsUtilsCV
import de.thkoeln.cvogt.android.opengl_utilities.textutilities.GLStringShapeCV
import de.thkoeln.cvogt.android.opengl_utilities.textutilities.GLTextUtilsCV
import java.util.Timer
import java.util.TimerTask


class MainActivity : Activity() {

    enum class ShapeTypes { CUBE, DOUBLECONE }

    enum class PlacementTypes { TWOCOLUMNS, RANDOM }

    val durationIntro = 2000       // duration of the general introduction and the introduction of a round (ms)

    val noShapePairs: Int = 5      // initial number of shape pairs in each round

    val initPoints: Int = 10000    // initial number of points in each round

    val pointsDecrAfterTimeInterval: Int = 100  // decrement for the number of points after each time interval of length durationTimeIntervalPointsDecr

    var durationTimeIntervalPointsDecr = 1000L  // duration of the time interval after which points are decremented

    var durationRotation = 3000  // duration of a shape rotation

    val decrPointsAfterWrongTouch = 1000   // decrement for the number of points when a shape has been touched that does not belong to the current pair

    var level: Int = 1             // current level/round of the game

    var pointsCurrentRound: Int = 0   // number of points in the current round

    var pointsTotal : Int = 0      // total number of points in the current game

    val colorProvider = ColorProvider(   // provider of the color combinations to be used
        arrayOf(
            GraphicsUtilsCV.red,
            GraphicsUtilsCV.green,
            GraphicsUtilsCV.blue,
            GraphicsUtilsCV.yellow,
            GraphicsUtilsCV.orange,
            GraphicsUtilsCV.purple
        )
    )

    var counterThread: Thread? = null   // thread to count down the number of points that can be reached in the current round

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        // prepare the user interface

        val renderer = GLRendererCV(GraphicsUtilsCV.black)
        val surfaceView = GLSurfaceViewCV(this, renderer, false)
        setContentView(surfaceView)

        // prepare the characters to be displayed

        GLTextUtilsCV.initHashMaps(this);

        // play the introduction

        val titleshape = GLStringShapeCV("intro", "Twins!", GraphicsUtilsCV.red, this)
        titleshape.setTransZ(-20f)
        val animTitle = GLAnimatorFactoryCV.makeAnimTransLinearToZ(20f, durationIntro)
        titleshape.addAnimator(animTitle)
        titleshape.removeFromSurfaceViewAfterAnimations()
        surfaceView.addShape(titleshape)

        // start the game after the introduction

        Timer().schedule(
            object : TimerTask() {
                override fun run() {
                    makeAndPlaceShapePairs(surfaceView,level)
                    counterThread = initAndStartPointsCounter(surfaceView,initPoints, pointsDecrAfterTimeInterval, durationIntro, durationTimeIntervalPointsDecr)
                    showInfo(surfaceView)
                }
            },
            durationIntro.toLong() )

        // set the touch listener for the surfaceView

        surfaceView.addOnTouchListener(object : GLOnTouchListenerCV {

            var remainingPairs: Int = noShapePairs  // the number of remaining pairs

            var prevTouchedShape: GLShapeCV? = null  // the shape that has been touched immediately before

            override fun onTouch(surfaceView: GLSurfaceViewCV, event: GLTouchEventCV): Boolean {

                if (event.motionEvent.action != MotionEvent.ACTION_DOWN) return true

                // get the touched shape
                // and return immediately if it does not belong to any pair (i.e. its name does not end with 'A' or 'B') or has been touched just before

                val touchedShape = surfaceView.touchedShape(event)
                if (touchedShape==null) return true
                val idTouched = touchedShape.id
                if (!idTouched.endsWith("A") && !idTouched.endsWith("B")) return true
                if (touchedShape === prevTouchedShape) return true

                // store a reference to the touched shape if it is the first shape of a pair and return

                if (prevTouchedShape == null) {
                    prevTouchedShape = touchedShape
                    return true
                }

                // another shape has been touched before:
                // check if that shape and the shape touched now belong to the same pair

                val idPrevTouched = prevTouchedShape!!.id
                if (idPrevTouched.substring(0, idPrevTouched.length-1) != idTouched.substring(0, idTouched.length-1)) {
                    // the shapes do not belong to the same pair
                    pointsCurrentRound -= decrPointsAfterWrongTouch
                    animateShapeMismatch(surfaceView,prevTouchedShape!!,touchedShape)
                    prevTouchedShape = null
                    return true
                }

                // the shapes belong to the same pair

                animateShapeMatch(touchedShape, prevTouchedShape!!)
                prevTouchedShape = null
                remainingPairs--

                // start the next round if no pairs remain

                if (remainingPairs == 0) {
                    counterThread!!.interrupt()
                    showRoundResult(surfaceView,pointsCurrentRound.toString() + " " + getString(R.string.points))
                    pointsTotal += pointsCurrentRound
                    level++
                    // reset the number of points for the next round
                    Timer().schedule(
                        object : TimerTask() {
                            override fun run() {
                                pointsCurrentRound = initPoints
                            }
                        },
                        durationIntro.toLong() )
                    remainingPairs = noShapePairs
                    prevTouchedShape = null
                    // show a new collection of pairs depending on the current level
                    // - differing in the positions and in the types of the shapes
                    surfaceView.clearShapes()
                    makeAndPlaceShapePairs(surfaceView,level)
                    if (level>3) {
                        durationTimeIntervalPointsDecr = (0.95 * durationTimeIntervalPointsDecr).toLong()
                        durationRotation = (0.95 * durationRotation).toInt()
                    }
                    // restart the thread that counts down the points
                    counterThread = initAndStartPointsCounter(surfaceView,initPoints, pointsDecrAfterTimeInterval, durationIntro, durationTimeIntervalPointsDecr)
                    // show curent level, current points, and current highscore
                    showInfo(surfaceView)
                }
                return true
            }
        })
    }

    // create some pairs of shapes

    private fun createShapePairs(shapeType : ShapeTypes, noShapePairs: Int): Array<GLShapeCV> {
        val shapes = arrayOfNulls<GLShapeCV>(noShapePairs * 2)
        when (shapeType) {
            ShapeTypes.CUBE -> {
                val colorCombinations = colorProvider.getRandomColorCombinations(noShapePairs, 4, true)
                for (i in 0 until noShapePairs) {
                    val cubeColors = arrayOfNulls<FloatArray>(6)
                    for (j in 0..3) cubeColors[j] = colorCombinations[i][j]
                    cubeColors[5] = GraphicsUtilsCV.lightblue // lightblue on top and bottom of the cube
                    cubeColors[4] = cubeColors[5]
                    // make two identical cubes to form a pair
                    shapes[2*i] = GLShapeFactoryCV.makeCube(i.toString() + "A", cubeColors)
                    shapes[2*i]!!.setScale(0.8f)
                    shapes[2*i+1] = shapes[2*i]!!.copy(i.toString() + "B")
                    shapes[2*i+1]!!.setScale(0.8f)
                }
            }
            ShapeTypes.DOUBLECONE -> {
                val facesColors = colorProvider.getRandomColorCombinations(noShapePairs * 2, 2, true)
                for (i in 0 until noShapePairs) {
                    val cone1 = GLShapeFactoryCV.makePyramid("",10, 2f, GraphicsUtilsCV.lightblue, facesColors[2*i])
                    val cone2 = GLShapeFactoryCV.makePyramid("",10,2f, GraphicsUtilsCV.lightblue, facesColors[2*i+1])
                    shapes[2*i] = GLShapeFactoryCV.joinShapes(i.toString() + "A", cone1, cone2, 1f, 1f, 1f, 0f, 0f, 180f, 0f, 1f, 0f)
                    shapes[2*i]!!.setScale(.25f)
                    shapes[2*i+1] = shapes[2*i]!!.copy(i.toString() + "B")
                    shapes[2*i+1]!!.setScale(.25f)
                }
            }
        }
        return shapes.filterNotNull().toTypedArray()
    }

    // place shape pairs on the display

    private fun placeShapePairs(surfaceView: GLSurfaceViewCV, shapePairs: Array<GLShapeCV>, placementType: PlacementTypes, initDuration: Int, rotDuration: Int) {
        // z coordinate of the shapes
        val zTrans = 0f
        // screen geometry values
        val surfaceviewWidth = surfaceView.getWidth().toFloat()
        val surfaceviewHeight = surfaceView.getHeight().toFloat()
        val leftBorder = GLUtilsCV.minVisibleX(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,-surfaceviewWidth/surfaceviewHeight);
        val rightBorder = GLUtilsCV.maxVisibleX(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,surfaceviewWidth/surfaceviewHeight);
        val topBorder = GLUtilsCV.maxVisibleY(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,1f);
        val bottomBorder = GLUtilsCV.minVisibleY(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,-1f);
        // maximum width and height of the shapes
        var maxShapeWidth = 0f
        for (shape in shapePairs)
            if (shape.getIntrinsicSize(0)>maxShapeWidth)
                maxShapeWidth = shape.getIntrinsicSize(0)*shape.scaleX
        var maxShapeHeight = 0f
        for (shape in shapePairs)
            if (shape.getIntrinsicSize(1)>maxShapeHeight)
                maxShapeHeight = shape.getIntrinsicSize(1)*shape.scaleY
        val numberOfShapes = shapePairs.size/2
        // place the shapes
        when (placementType) {
            PlacementTypes.TWOCOLUMNS -> {
                val horizSpace = 1.3f
                val vertSpace = 0.5f
                val permut: MutableList<Int> = ArrayList()
                for (i in 0 until shapePairs.size / 2) permut.add(i)
                permut.shuffle()
                val yBase = (numberOfShapes*maxShapeHeight+(numberOfShapes-1)*vertSpace)/2
                for (i in 0 until shapePairs.size / 2) {
                    shapePairs[2 * i].setTrans(-maxShapeWidth/2-horizSpace/2, yBase - (maxShapeHeight+vertSpace) * i + maxShapeHeight/2, -30f)
                    shapePairs[2 * i].addAnimator(GLAnimatorFactoryCV.makeAnimTransLinearToZ(zTrans, initDuration))
                    shapePairs[2 * i].addAnimator(GLAnimatorFactoryCV.makeAnimRotY(360f, 3000, 1000, false))
                    shapePairs[2 * i + 1].setTrans(maxShapeWidth/2+horizSpace/2, yBase - (maxShapeHeight+vertSpace) * permut[i] + maxShapeHeight/2, -30f)
                    shapePairs[2 * i + 1].addAnimator(GLAnimatorFactoryCV.makeAnimTransLinearToZ(zTrans, initDuration))
                    shapePairs[2 * i + 1].addAnimator(GLAnimatorFactoryCV.makeAnimRotY(360f,rotDuration,1000,false))
                    surfaceView.addShape(shapePairs[2 * i])
                    surfaceView.addShape(shapePairs[2 * i + 1])
                }
            }
            PlacementTypes.RANDOM -> {
                val noOfRows = ((topBorder-bottomBorder)/(1.2f*maxShapeHeight)).toInt()
                val noOfColumns = ((rightBorder-leftBorder)/(1.3f*maxShapeWidth)).toInt()
                val colWidth = (rightBorder-leftBorder)/noOfColumns
                val rowHeight = (topBorder-bottomBorder)/noOfRows
                val positions = ArrayList<IntArray>()
                for (i in 0 until noOfColumns)
                    for (j in 3 until noOfRows-2)
                        positions.add(intArrayOf(i, j))
                for (i in shapePairs.indices) {
                    val index = (positions.size * Math.random()).toInt()
/*
                    val transX =
                        positions[index][0] * colWidth - size / 2 + (2.0 / 3.0 * (Math.random() - 0.5f) * colWidth).toFloat()
                    val transY =
                        positions[index][1] * rowHeight - size / 2 + (2.0 / 3.0 * (Math.random() - 0.5f) * rowHeight).toFloat()
*/
                    val transX = positions[index][0]*colWidth - (rightBorder-leftBorder)/2 + colWidth/2
                    val transY = positions[index][1]*rowHeight - (topBorder-bottomBorder)/2 + rowHeight/2
                    shapePairs[i].setTrans(transX, transY, -30f)
                    shapePairs[i].addAnimator(GLAnimatorFactoryCV.makeAnimTransLinearToZ(zTrans, initDuration))
                    shapePairs[i].addAnimator(GLAnimatorFactoryCV.makeAnimRotY(360f, rotDuration, 1000, false))
                    surfaceView.addShape(shapePairs[i])
                    positions.removeAt(index)
                }
            }
        }
    }

    // make shape pairs and place them depending on the game level

    private fun makeAndPlaceShapePairs(surfaceView: GLSurfaceViewCV, level: Int) {
        when (level) {
            1 -> placeShapePairs(surfaceView,createShapePairs(ShapeTypes.DOUBLECONE,noShapePairs), PlacementTypes.TWOCOLUMNS, durationIntro, durationRotation)
            2 -> placeShapePairs(surfaceView,createShapePairs(ShapeTypes.DOUBLECONE,noShapePairs), PlacementTypes.RANDOM, durationIntro, durationRotation)
            3 -> placeShapePairs(surfaceView,createShapePairs(ShapeTypes.CUBE,noShapePairs), PlacementTypes.TWOCOLUMNS, durationIntro, durationRotation)
            else -> placeShapePairs(surfaceView,createShapePairs(ShapeTypes.CUBE,noShapePairs), PlacementTypes.RANDOM, durationIntro, durationRotation)
        }
    }

    // initialize and start the thread to count down the points

    private fun initAndStartPointsCounter(
        surfaceView: GLSurfaceViewCV,
        initPoints: Int,
        decrement: Int,
        startDelay: Int,
        decrementTimeInterval : Long
    ): Thread {
        pointsCurrentRound = initPoints
        val counterThread: Thread = object : Thread() {
            override fun run() {
                super.run()
                try {
                    sleep(startDelay.toLong())
                } catch (exc: Exception) {}
                var counterShape: GLShapeCV? = null   // shape that shows the current points
                while (!isInterrupted) {
                    if (counterShape != null) surfaceView.removeShape(counterShape)
                    if (pointsCurrentRound <= 0) {
                        gameOver(surfaceView)
                        return
                    }
                    counterShape = GLTextUtilsCV.makeStringShapeForNumber(
                        "Counter",
                        pointsCurrentRound.toDouble(),
                        GraphicsUtilsCV.lightblue,
                        null,
                        0f,
                        GLTextUtilsCV.TwoD,
                        this@MainActivity
                    )
                    counterShape.setScale(.75f)
                    counterShape.setTrans(0f, -4f, -1f)
                    surfaceView.addShape(counterShape)
                    pointsCurrentRound -= decrement
                    try {
                        sleep(decrementTimeInterval)
                    } catch (exc: InterruptedException) {
                        surfaceView.removeShape(counterShape)
                        return
                    }
                }
                surfaceView.removeShape(counterShape)
                return
            }
        }
        counterThread.start()
        return counterThread
    }

    // show current level, current points, and overall highscore

    private fun showInfo(surfaceView: GLSurfaceViewCV) {
        // positioning constants
        val scale = .3f
        val zTrans = -3f
        val xPadding = .2f
        // screen geometry values
        val width = surfaceView.getWidth().toFloat()
        val height = surfaceView.getHeight().toFloat()
        val leftBorder = GLUtilsCV.minVisibleX(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,-width/height);
        val rightBorder = GLUtilsCV.maxVisibleX(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,width/height);
        val topBorder = GLUtilsCV.maxVisibleY(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,1f);
        val bottomBorder = GLUtilsCV.minVisibleY(zTrans,surfaceView.renderer.getEyePosZ(),GLRendererCV.frustumNear,-1f);
        // highscore info string
        val highscoreString = getString(R.string.current_highscore)+": "+getSharedPreferences("Highscore", 0).getInt("Highscore",0)
        val highscoreShape = GLStringShapeCV("highscoreshape", highscoreString, GraphicsUtilsCV.lightblue, this@MainActivity)
        highscoreShape.setScale(scale)
        var xPos = leftBorder+xPadding+scale*(highscoreShape.getIntrinsicSize(0)/2)
        var yPos = bottomBorder+scale*(highscoreShape.getIntrinsicSize(1)/2)+0.3f
        highscoreShape.setTrans(xPos,yPos,zTrans)
        surfaceView.addShape(highscoreShape)
        // current points info string
        val currentPointsString = getString(R.string.current_points)+": "+pointsTotal
        val currentPointsShape = GLStringShapeCV("currentpointsshape", currentPointsString, GraphicsUtilsCV.lightblue, this@MainActivity)
        currentPointsShape.setScale(scale)
        xPos = leftBorder+xPadding+scale*(currentPointsShape.getIntrinsicSize(0)/2)
        yPos = bottomBorder+scale*(highscoreShape.getIntrinsicSize(1)+(currentPointsShape.getIntrinsicSize(1)/2))+.6f
        currentPointsShape.setTrans(xPos,yPos,zTrans)
        surfaceView.addShape(currentPointsShape)
        // level info string
        val currentLevelString = getString(R.string.current_level)+": "+level
        val currentLevelShape = GLStringShapeCV("currentlevelshape", currentLevelString, GraphicsUtilsCV.lightblue, this@MainActivity)
        currentLevelShape.setScale(scale)
        xPos = leftBorder+xPadding+scale*(currentLevelShape.getIntrinsicSize(0)/2)
        yPos = topBorder-scale*(currentLevelShape.getIntrinsicSize(1)/2)-.6f
        currentLevelShape.setTrans(xPos,yPos,zTrans)
        surfaceView.addShape(currentLevelShape)
    }

    // animate a match, i.e. two shapes of the same pair have been touched

    private fun animateShapeMatch(shape1: GLShapeCV, shape2: GLShapeCV) {
        val target = GraphicsUtilsCV.midpoint3D(shape1.trans, shape2.trans)
        var animator = GLAnimatorFactoryCV.makeAnimTransLinearTo(target, 500)
        shape1.addAnimator(animator)
        shape1.removeFromSurfaceViewAfterAnimations()
        shape1.startAnimators()
        animator = GLAnimatorFactoryCV.makeAnimTransLinearTo(target, 500)
        shape2.addAnimator(animator)
        shape2.removeFromSurfaceViewAfterAnimations()
        shape2.startAnimators()
    }

    // animate a mismatch, i.e. two shapes have been touched that do not belong to the same pair

    private fun animateShapeMismatch(surfaceView: GLSurfaceViewCV, shape1: GLShapeCV, shape2: GLShapeCV) {
        shape1.addAnimator(GLAnimatorFactoryCV.makeAnimRotRoll(30f, 500, 1))
        shape1.startAnimators()
        shape2.addAnimator(GLAnimatorFactoryCV.makeAnimRotRoll(30f, 500, 1))
        shape2.startAnimators()
        if (pointsCurrentRound<decrPointsAfterWrongTouch) return
        val wrong = getString(R.string.wrong) + "!"
        val wrongShape: GLShapeCV = GLStringShapeCV("wrongShape", wrong, this@MainActivity)
        wrongShape.setTrans(0f, -20f, -20f)
        val anim = GLAnimatorFactoryCV.makeAnimTransLinearTo(floatArrayOf(0f, 0f, 4f), 2000)
        wrongShape.addAnimator(anim)
        wrongShape.removeFromSurfaceViewAfterAnimations()
        surfaceView.addShape(wrongShape)
    }

    // show the result of the current round

    private fun showRoundResult(surfaceView: GLSurfaceViewCV, text: String) {
        // Toast.makeText(MainActivity.this,text,Toast.LENGTH_LONG).show();
        // prepareThread!!.waitForPreparation()
        val resultShape: GLShapeCV = GLStringShapeCV("result", text, this@MainActivity)
        resultShape.setTransZ(-20f)
        val anim = GLAnimatorFactoryCV.makeAnimTransLinearToZ(1f, 2000)
        resultShape.addAnimator(anim)
        resultShape.removeFromSurfaceViewAfterAnimations()
        surfaceView.addShape(resultShape)
    }

    // animate the end of the game

    private fun gameOver(surfaceView: GLSurfaceViewCV) {
        counterThread!!.interrupt()
        // let the shapes currently shown disappear
        for (shape in surfaceView.shapesToRender) {
            runOnUiThread {
                shape.addAnimator(GLAnimatorFactoryCV.makeAnimTransLinearToZ(-30f,2000))
                shape.addAnimator(GLAnimatorFactoryCV.makeAnimScale(0.05f,2000))
                shape.startAnimators()
            }
        }
        // check and update the highscore
        var newHighscore = false
        val sp = getSharedPreferences("Highscore", 0)
        val highscore = sp.getInt("Highscore",0)
        if (highscore<pointsTotal) {
            newHighscore = true
            val editor = sp.edit()
            editor.putInt("Highscore", pointsTotal)
            editor.commit()
        }
        // show the result
        val gameOverShape1: GLShapeCV = GLStringShapeCV("gameOver1", getString(R.string.game_over), this@MainActivity)
        gameOverShape1.setTrans(8f,1f,-4f)
        val anim1 = GLAnimatorFactoryCV.makeAnimTransLinearToX(-10f, 5000)
        gameOverShape1.addAnimator(anim1)
        surfaceView.addShape(gameOverShape1)
        val gameOverShape2: GLShapeCV = GLStringShapeCV("gameOver2", pointsTotal.toString()+" "+getString(R.string.points), this@MainActivity)
        gameOverShape2.setTrans(8f,-1f,-3f)
        val anim2 = GLAnimatorFactoryCV.makeAnimTransLinearToX(-10f, 5000)
        gameOverShape2.addAnimator(anim2)
        surfaceView.addShape(gameOverShape2)
        if (newHighscore) {
            val gameOverShape3: GLShapeCV = GLStringShapeCV("gameOver2", getString(R.string.new_highscore), this@MainActivity)
            gameOverShape3.setTrans(8f,-3f,-3f)
            val anim3 = GLAnimatorFactoryCV.makeAnimTransLinearToX(-15f, 5000)
            gameOverShape3.addAnimator(anim3)
            surfaceView.addShape(gameOverShape3)
        }
        Timer().schedule(
            object : TimerTask() {
                override fun run() {
                    this@MainActivity.finish()
                    System.exit(0)
                }
            },
           7000 )
    }

    override fun onCreateOptionsMenu(menu : Menu) : Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==R.id.Info) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Info")
            builder.setMessage(getString(R.string.info_text))
            builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id -> dialog.dismiss() })
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
        if (item.itemId==R.id.Control) {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Info")
                builder.setMessage(getString(R.string.reset_highscore))
                builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                    val editor = getSharedPreferences("Highscore", 0).edit()
                    editor.putInt("Highscore", 0)
                    editor.commit()
                    dialog.dismiss() })
                builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                    dialog.dismiss() })
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        return super.onOptionsItemSelected(item)
    }
}